package com.zb.enums.system;

public class NavigationEnums {

    /**
     * 导航是否可用
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午10:45:03
     */
    public enum NavigationAvailable {
        
        NO(0, "禁用"),
        YES(1, "可用");

        private int type;
        private String name;

        private NavigationAvailable(int type, String name) {
            this.type = type;
            this.name = name;
        }

        public int getType() {
            return this.type;
        }

        public String getName() {
            return this.name;
        }
    }
    
    /**
     * 导航跳转的类型
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午10:46:05
     */
    public enum NavigationLinkType {
        
        VIEW(2, "视图"),
        NOTVIEW(1, "非视图");

        private int type;
        private String name;

        private NavigationLinkType(int type, String name) {
            this.type = type;
            this.name = name;
        }

        public int getType() {
            return this.type;
        }

        public String getName() {
            return this.name;
        }
    }
}
