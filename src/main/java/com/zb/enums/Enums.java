package com.zb.enums;

/**
 * 系统全局枚举类
 * 
 * 作者: zhoubang 日期：2015年4月17日 上午11:36:15
 */
public class Enums {

    /**
     * 分页枚举
     * 
     * @author zhoubang
     *
     */
    public enum Page {

        DEFAULT_PAGE_SIZE(20, "每页大小");

        private int value;
        private String name;

        private Page(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }
    
    public enum Role {
        USER("1","会员"),
        ADMIN("2","管理员");
        
        private String id;
        private String name;
        
        private Role(String id,String name) {
            this.id = id;
            this.name = name;
        }
        
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
    /**
     * 编码枚举
     * 
     * 作者: zhoubang 日期：2015年4月3日 下午4:24:29
     */
    public enum Charset {
        DEFAULT_CHARSET("UTF-8", "默认编码");

        private String value;
        private String name;

        private Charset(String value, String name) {
            this.value = value;
            this.name = name;
        }

        public String getValue() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }
    
    /**
     * 用户会员状态枚举
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午9:01:11
     */
    public enum UserStatus {
        OK(1, "正常"),
        LOCK(2, "已锁定"),
        DELETE(3, "已删除");
        
        
        private int status;
        private String name;

        private UserStatus(int status, String name) {
            this.status = status;
            this.name = name;
        }

        public int getStatus() {
            return this.status;
        }

        public String getName() {
            return this.name;
        }
        
        /**
         * 根据状态获取名称
         * 
         * 作者: zhoubang 
         * 日期：2015年4月24日 下午3:24:34
         * @param status
         * @return
         */
        public static String getUserStatusName(Integer status) {
            if (status != null) {
                for (UserStatus userStatus : UserStatus.values()) {
                    if (userStatus.getStatus() == status) {
                        return userStatus.getName();
                    }
                }
            }
            return "";
        }
    }
    
    /**
     * 角色状态枚举
     * @author zhoubang
     *
     */
    public enum RoleStatus {
        OK(1, "正常"),
        LOCK(2, "禁用"),
        DELETE(3, "已删除");
        
        
        private int status;
        private String name;

        private RoleStatus(int status, String name) {
            this.status = status;
            this.name = name;
        }

        public int getStatus() {
            return this.status;
        }

        public String getName() {
            return this.name;
        }
        
        /**
         * 根据状态获取名称
         * 
         * 作者: zhoubang 
         * 日期：2015年4月24日 下午3:24:34
         * @param status
         * @return
         */
        public static String getRoleStatusName(Integer status) {
            if (status != null) {
                for (RoleStatus roleStatus : RoleStatus.values()) {
                    if (roleStatus.getStatus() == status) {
                        return roleStatus.getName();
                    }
                }
            }
            return "";
        }
    }
    
    /**
     * 权限状态枚举
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午10:55:00
     */
    public enum PermissionStatus {
        OK(1, "正常"),
        LOCK(2, "禁用"),
        DELETE(3, "已删除");
        
        
        private int status;
        private String name;

        private PermissionStatus(int status, String name) {
            this.status = status;
            this.name = name;
        }

        public int getStatus() {
            return this.status;
        }

        public String getName() {
            return this.name;
        }
        
        /**
         * 根据状态获取名称
         * 
         * 作者: zhoubang 
         * 日期：2015年4月24日 下午3:24:34
         * @param status
         * @return
         */
        public static String getPermissionStatusName(Integer status) {
            if (status != null) {
                for (PermissionStatus permissionStatus : PermissionStatus.values()) {
                    if (permissionStatus.getStatus() == status) {
                        return permissionStatus.getName();
                    }
                }
            }
            return "";
        }
    }
    
    
    public enum MenuType {
        SYSTEM(1, "后台菜单"),
        CENTER(2, "会员菜单");
        
        
        private int type;
        private String name;

        private MenuType(int type, String name) {
            this.type = type;
            this.name = name;
        }

        public int getType() {
            return this.type;
        }

        public String getName() {
            return this.name;
        }
        
        /**
         * 根据状态获取名称
         * 
         * 作者: zhoubang 
         * 日期：2015年4月24日 下午3:24:34
         * @param status
         * @return
         */
        public static String getMenuTypeName(Integer type) {
            if (type != null) {
                for (MenuType menuType : MenuType.values()) {
                    if (menuType.getType() == type) {
                        return menuType.getName();
                    }
                }
            }
            return "";
        }
    }
    
}
