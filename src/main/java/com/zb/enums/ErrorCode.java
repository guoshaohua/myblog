package com.zb.enums;

/**
 * 错误代码
 * 
 * 作者: zhoubang 日期：2015年4月17日 上午11:35:55
 */
public enum ErrorCode {

    PARAMETER_SET_IS_INVALID(300, "参数设置是无效的"),
    PARAMETER_VALUE_IS_INVALID(301, "参数值是无效的"),
    UNKNOW_EXCEPTION(400, "未知异常"), 
    NOT_EXIST(404, "不存在");
    
    private int code;
    private String msg;

    private ErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

}
