package com.zb.orm.meta;

/**
 * 
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:50
 */
public class EntityColumnMetadata {

    private String field;// 实体类成员名
    private String name;// 表字段名
    private Class<?> fieldType;// 类型
    private int length;
    private int scale;
    private boolean require = false;
    private boolean unique = false;
    private boolean lazy = false;
    private boolean lob = false;
    private boolean primaryKey = false;

    /**
     * 是否自动创建主键
     */
    private boolean auto = false;

    /**
     * <p>
     * Getter for the field <code>field</code>.
     * </p>
     * 
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * <p>
     * Getter for the field <code>fieldType</code>.
     * </p>
     * 
     * @return the fieldType
     */
    public Class<?> getFieldType() {
        return fieldType;
    }

    /**
     * <p>
     * Getter for the field <code>length</code>.
     * </p>
     * 
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * <p>
     * Getter for the field <code>name</code>.
     * </p>
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * <p>
     * getNameWithQuote.
     * </p>
     * 
     * @return a {@link java.lang.String} object.
     */
    public String getNameWithQuote() {
        return "`" + this.name + "`";
    }

    /**
     * <p>
     * Getter for the field <code>scale</code>.
     * </p>
     * 
     * @return the scale
     */
    public int getScale() {
        return scale;
    }

    /**
     * <p>
     * isAuto.
     * </p>
     * 
     * @return a boolean.
     */
    public boolean isAuto() {
        return auto;
    }

    /**
     * <p>
     * isLazy.
     * </p>
     * 
     * @return the lazy
     */
    public boolean isLazy() {
        return lazy;
    }

    /**
     * <p>
     * isLob.
     * </p>
     * 
     * @return the lob
     */
    public boolean isLob() {
        return lob;
    }

    /**
     * <p>
     * isPrimaryKey.
     * </p>
     * 
     * @return the primaryKey
     */
    public boolean isPrimaryKey() {
        return primaryKey;
    }

    /**
     * <p>
     * isRequire.
     * </p>
     * 
     * @return the require
     */
    public boolean isRequire() {
        return require;
    }

    /**
     * <p>
     * isUnique.
     * </p>
     * 
     * @return the unique
     */
    public boolean isUnique() {
        return unique;
    }

    /**
     * <p>
     * Setter for the field <code>auto</code>.
     * </p>
     * 
     * @param auto
     *            a boolean.
     */
    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    /**
     * <p>
     * Setter for the field <code>field</code>.
     * </p>
     * 
     * @param field
     *            the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * <p>
     * Setter for the field <code>fieldType</code>.
     * </p>
     * 
     * @param fieldType
     *            the fieldType to set
     */
    public void setFieldType(Class<?> fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * <p>
     * Setter for the field <code>lazy</code>.
     * </p>
     * 
     * @param lazy
     *            the lazy to set
     */
    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }

    /**
     * <p>
     * Setter for the field <code>length</code>.
     * </p>
     * 
     * @param length
     *            the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * <p>
     * Setter for the field <code>lob</code>.
     * </p>
     * 
     * @param lob
     *            the lob to set
     */
    public void setLob(boolean lob) {
        this.lob = lob;
    }

    /**
     * <p>
     * Setter for the field <code>name</code>.
     * </p>
     * 
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>
     * Setter for the field <code>primaryKey</code>.
     * </p>
     * 
     * @param primaryKey
     *            the primaryKey to set
     */
    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    /**
     * <p>
     * Setter for the field <code>require</code>.
     * </p>
     * 
     * @param require
     *            the require to set
     */
    public void setRequire(boolean require) {
        this.require = require;
    }

    /**
     * <p>
     * Setter for the field <code>scale</code>.
     * </p>
     * 
     * @param scale
     *            the scale to set
     */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * <p>
     * Setter for the field <code>unique</code>.
     * </p>
     * 
     * @param unique
     *            the unique to set
     */
    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    @Override
    public String toString() {
        return "EntityColumnMetadata ["
                + "field=" + field 
                + ", name=" + name
                + ", fieldType=" + fieldType 
                + ", length=" + length
                + ", scale=" + scale 
                + ", require=" + require 
                + ", unique=" + unique 
                + ", lazy=" + lazy 
                + ", lob=" + lob 
                + ", primaryKey=" + primaryKey 
                + ", auto=" + auto 
                + "]";
    }

}
