package com.zb.orm.query.operator;

/**
 * 模糊匹配
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:27:32
 */
public enum LIKE implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_LIKE;
    }
}
