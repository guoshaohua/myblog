package com.zb.orm.query.operator;

/**
 * 不包含在
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:06
 */
public enum NOT_IN implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_NOT_IN;
    }

}
