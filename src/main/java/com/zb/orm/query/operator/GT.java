package com.zb.orm.query.operator;

/**
 * 大于
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:27:08
 */
public enum GT implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_GT;
    }

}
