package com.zb.orm.query.operator;

/**
 * 是否非空
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:14
 */
public enum NOT_NULL implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_NOT_NULL;
    }
}
