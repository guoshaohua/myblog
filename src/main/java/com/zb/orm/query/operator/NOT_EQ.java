package com.zb.orm.query.operator;

/**
 * 不等于
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:27:57
 */
public enum NOT_EQ implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_NOT_EQ;
    }
}
