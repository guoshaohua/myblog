package com.zb.orm.query.operator;

/**
 * 
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:40
 */
public class OperatorConsts {

    static final String STR_EQ = "=";
    static final String STR_LT = "<";
    static final String STR_GT = ">";
    static final String STR_LE = "<=";
    static final String STR_GE = ">=";
    static final String STR_NOT_EQ = "<>";
    static final String STR_LIKE = "LIKE";
    static final String STR_IN = "IN";
    static final String STR_NOT_IN = "NOT IN";
    static final String STR_BETWEEN = "BETWEEN";
    static final String STR_NOT_BETWEEN = "NOT BETWEEN";
    static final String STR_NULL = "IS NULL";
    static final String STR_NOT_NULL = "IS NOT NULL";

}
