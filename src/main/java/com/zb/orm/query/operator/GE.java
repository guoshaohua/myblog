package com.zb.orm.query.operator;

/**
 * 大于等于
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:26:39
 */
public enum GE implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_GE;
    }
}
