package com.zb.orm.query.operator;

/**
 * 介于两者之间
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:26:00
 */
public enum BETWEEN implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_BETWEEN;
    }

}
