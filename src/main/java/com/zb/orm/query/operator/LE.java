package com.zb.orm.query.operator;

/**
 * 小于等于
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:27:25
 */
public enum LE implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_LE;
    }
}
