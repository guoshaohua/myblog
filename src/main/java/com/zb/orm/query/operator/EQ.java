package com.zb.orm.query.operator;

/**
 * 
 * 等于
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:26:25
 */
public enum EQ implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_EQ;
    }

}
