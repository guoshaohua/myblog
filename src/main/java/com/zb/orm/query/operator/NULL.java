package com.zb.orm.query.operator;

/**
 * 是否空
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:26
 */
public enum NULL implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_NULL;
    }
}
