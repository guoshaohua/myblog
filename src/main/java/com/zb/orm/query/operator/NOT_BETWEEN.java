package com.zb.orm.query.operator;

/**
 * 不介于之间
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:27:47
 */
public enum NOT_BETWEEN implements Operator {
    singleton;

    @Override
    public String value() {
        return OperatorConsts.STR_NOT_BETWEEN;
    }

}
