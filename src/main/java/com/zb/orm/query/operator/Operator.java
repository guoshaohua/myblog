package com.zb.orm.query.operator;

/**
 * 运算符
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:28:33
 */
public interface Operator {
    /**
     * <p>
     * value.
     * </p>
     *
     * @return a {@link java.lang.String} object.
     */
    String value();

    /**
     * 等于 =
     */
    static final EQ eq = EQ.singleton;
    /**
     * 不等于 <>
     */
    static final NOT_EQ notEQ = NOT_EQ.singleton;
    /**
     * between
     */
    static final BETWEEN between = BETWEEN.singleton;
    /**
     * not between
     */
    static final NOT_BETWEEN notBetween = NOT_BETWEEN.singleton;
    /**
     * 大于 >
     */
    static final GT gt = GT.singleton;
    /**
     * 小于 <
     */
    static final LT lt = LT.singleton;
    /**
     * 大于等于 >=
     */
    static final GE ge = GE.singleton;
    /**
     * 小于等于 <=
     */
    static final LE le = LE.singleton;
    /**
     * like
     */
    static final LIKE like = LIKE.singleton;
    /**
     * in
     */
    static final IN in = IN.singleton;
    /**
     * not in
     */
    static final NOT_IN notIn = NOT_IN.singleton;
    /**
     * is null
     */
    static final NULL isNull = NULL.singleton;
    /**
     * is not null
     */
    static final NOT_NULL isNotNull = NOT_NULL.singleton;

}
