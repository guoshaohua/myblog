package com.zb.orm.query;

import com.zb.orm.query.operator.Operator;

/**
 * 查询上下文，用于拼接多个查询条件
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:25:11
 */
public class QueryContextPiece {

    /**
     * 
     * @param field
     * @param operator
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
    public static final <Z extends Object> QueryContextPiece and(String field,
            Operator operator, Z... value) {
        return new QueryContextPiece(LogicEnum.AND, field, operator, value);
    }

    /**
     * 
     * @param field
     * @param operator
     * @param value
     * @return
     */
    @SuppressWarnings("unchecked")
    public static final <Z extends Object> QueryContextPiece or(String field,
            Operator operator, Z... value) {
        return new QueryContextPiece(LogicEnum.OR, field, operator, value);
    }

    /**
     * 字段
     */
    private String field;
    /**
     * 运算值
     */
    private Object[] values;
    /**
     * 运算操作符
     */
    private Operator operator;
    /**
     * 拼接逻辑
     */
    private LogicEnum appendType;

    public QueryContextPiece(LogicEnum appendType, String field,
            Operator operator, Object[] values) {
        this.field = field;
        this.values = values;
        this.operator = operator;
        this.appendType = appendType;
    }

    /**
     * <p>
     * Getter for the field <code>appendType</code>.
     * </p>
     *
     * @return the appendType
     */
    public LogicEnum getAppendType() {
        return appendType;
    }

    /**
     * <p>
     * Getter for the field <code>field</code>.
     * </p>
     *
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * <p>
     * Getter for the field <code>operator</code>.
     * </p>
     *
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * <p>
     * Getter for the field <code>values</code>.
     * </p>
     *
     * @return the values
     */
    public Object[] getValues() {
        return values;
    }

}
