package com.zb.orm.query;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:25:37
 */
public final class OrderBy {

    private boolean asc;

    /**
     * @return the asc
     */
    public boolean isAsc() {
        return asc;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    private String field;

    private OrderBy(boolean isAsc, String field) {
        if (StringUtils.isBlank(field)) {
            throw new RuntimeException("create order error: field is blank.");
        }
        this.field = field;
        this.asc = isAsc;
    }

    public static final OrderBy asc(final String field) {
        return new OrderBy(true, field);
    }

    public static final OrderBy desc(final String field) {
        return new OrderBy(false, field);
    }

}
