package com.zb.orm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.zb.orm.query.OrderBy;
import com.zb.orm.query.QueryContext;
import com.zb.vo.PageVo;

/**
 * 基础DAO接口
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:24:42
 * 
 * @param <T>
 */
public interface DaoSupport<T> {

    /**
     * @return 返回实体类的类型
     */
    Class<T> getEntityClass();

    /**
     * 获取spring jdbctemplate
     * 
     * @return
     * @throws Exception
     */
    JdbcTemplate getJdbcTemplate() throws Exception;

    /**
     * 获取spring named parameter jdbctemplate
     * 
     * @return
     * @throws Exception
     */
    NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() throws Exception;

    /**
     * 返回记录总数
     * 
     * @param map
     * @return a long.
     * @throws Exception
     */
    long count(Map<String, Object> map) throws Exception;

    /**
     * 返回记录总数
     * 
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @return 记录总数
     * @throws Exception
     */
    long count(String field, Object value) throws Exception;

    /**
     * 返回记录总数
     * 
     * @param fields
     *            字段名数组
     * @param values
     *            字段值数组
     * @return 记录总数
     * @throws Exception
     */
    long count(String[] fields, Object[] values) throws Exception;

    /**
     * 返回记录总数
     * 
     * @param fields
     *            字段名数组
     * @param values
     *            字段值数组
     * @return 记录总数
     * @throws Exception
     */
    long count(QueryContext context) throws Exception;

    /**
     * 返回记录总数
     * 
     * @return 记录总数
     * @throws Exception
     */
    long countAll() throws Exception;

    /**
     * 根据主键值删除
     * 
     * @param primaryfield
     *            主键值
     * @throws Exception
     */
    void delete(Serializable primaryfield) throws Exception;

    /**
     * 根据字段值匹配删除
     * 
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @throws Exception
     */
    void delete(String field, Object value) throws Exception;

    /**
     * 根据多个字段值匹配删除
     * 
     * @param fields
     * @param values
     * @throws Exception
     */
    void delete(String[] fields, Object[] values) throws Exception;

    /**
     * 根据Map条件删除
     * 
     * @param map
     * @throws Exception
     */
    void delete(Map<String, Object> map) throws Exception;

    /**
     * 根据查询条件删除
     * 
     * @param queryContext
     * @throws YichaDaoException
     */
    void delete(QueryContext queryContext) throws Exception;

    /**
     * 删除实体类
     * 
     * @param t
     *            实体类实例
     * @throws Exception
     */
    void delete(T t) throws Exception;

    /**
     * 删除所有
     * 
     * @throws Exception
     * 
     */
    void deleteAll() throws Exception;

    /**
     * 执行sql
     * 
     * @param sql
     * @param params
     * @throws Exception
     */
    void executeSQL(String sql, Object... params) throws Exception;

    /**
     * 根据主键值查询
     * 
     * @param pkValue
     * @return 实体类
     * @throws Exception
     */
    T load(Serializable pkValue) throws Exception;

    /**
     * 根据MAP条件查询
     * 
     * @param map
     *            key:字段名 value:字段值
     * @param order
     * @return 实体类列表
     * @throws Exception
     */
    List<T> query(Map<String, Object> map, OrderBy... orderBy) throws Exception;

    /**
     * 根据字段值匹配查询
     * 
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @param orderBy
     *            排序
     * @return 实体类列表
     * @throws Exception
     */
    List<T> query(String field, Object value, OrderBy... orderBy)
            throws Exception;

    /**
     * 根据多多个字段值匹配查询
     * 
     * @param fields
     *            字段名数组
     * @param values
     *            字段值数组
     * @param order
     *            排序
     * @return 实体类列表
     * @throws Exception
     */
    List<T> query(String[] fields, Object[] values, OrderBy... orderBy)
            throws Exception;

    /**
     * 使用查询上下文匹配查询
     * 
     * @param context
     *            查询上下文
     * @param order
     *            排序，可以多个
     * @return 实体类列表
     * @throws YichaDaoException
     */
    List<T> query(QueryContext context, OrderBy... orderBy) throws Exception;

    /**
     * 查询所有
     * 
     * @param order
     *            排序
     * @return 实体类列表
     * @throws Exception
     */
    List<T> queryAll(OrderBy... orderBy) throws Exception;

    /**
     * 根据SQL查询
     * 
     * @param sql
     * @param params
     * @return 实体类列表
     * @throws Exception
     */
    List<T> queryBySQL(String sql, Object... params) throws Exception;

    /**
     * 根据SQL查询第一个
     * 
     * @param sql
     * @param params
     * @return 实体类
     * @throws Exception
     */
    T queryBySQLFirst(String sql, Object... params) throws Exception;

    /**
     * 根据MAP查询第一个
     * 
     * @param map
     * @param orderBy
     * @return 实体类
     * @throws Exception
     */
    T queryFirst(Map<String, Object> map, OrderBy... orderBy) throws Exception;

    /**
     * 根据字段值查询第一个
     * 
     * @param field
     *            字段名
     * @param orderBy
     *            字段值
     * @param order
     *            排序
     * @return 实体类
     * @throws Exception
     */
    T queryFirst(String field, Object value, OrderBy... orderBy)
            throws Exception;

    /**
     * 根据多个字段值查询第一个
     * 
     * @param fields
     *            字段名列表
     * @param values
     *            字段值列表
     * @param orderBy
     *            排序
     * @return 实体类
     * @throws Exception
     */
    T queryFirst(String[] fields, Object[] values, OrderBy... orderBy)
            throws Exception;

    /**
     * 根据上下文查询第一个
     * 
     * @param context
     * @param order
     * @return
     * @throws YichaDaoException
     */
    T queryFirst(QueryContext context, OrderBy... orderBy) throws Exception;

    /**
     * 范围查询
     * 
     * @param limitFrom
     *            从第几条记录开始
     * @param limitSize
     *            查询数量
     * @param orderBy
     *            排序
     * @return 实体类列表
     * @throws Exception
     */
    List<T> queryLimit(int limitFrom, int limitSize, OrderBy... orderBy)
            throws Exception;

    /**
     * 范围查询
     * 
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @param limitFrom
     *            从第几条记录开始
     * @param limitSize
     *            查询数量
     * @param orderBy
     *            排序
     * @return 实体类列表
     * @throws Exception
     */
    List<T> queryLimit(String field, Object value, int limitFrom,
            int limitSize, OrderBy... orderBy) throws Exception;

    /**
     * 范围查询
     * 
     * @param fields
     *            字段名数组
     * @param values
     *            字段值数组
     * @param limitFrom
     *            从第几条记录开始
     * @param limitSize
     *            查询数量
     * @param orderBy
     *            排序
     * @return 实体类结果集
     * @throws Exception
     */
    List<T> queryLimit(String[] fields, Object[] values, int limitFrom,
            int limitSize, OrderBy... orderBy) throws Exception;

    /**
     * 范围查询
     * 
     * @param map
     *            字段名数组 : 字段值数组
     * @param limitFrom
     *            从第几条记录开始
     * @param limitSize
     *            查询数量
     * @param orderBy
     *            排序
     * @return 实体类结果集
     * @throws Exception
     */
    List<T> queryLimit(Map<String, Object> map, int limitFrom, int limitSize,
            OrderBy... orderBy) throws Exception;

    /**
     * 范围查询
     * 
     * @param context
     * @param limitFrom
     * @param limitSize
     * @param order
     * @return
     * @throws YichaDaoException
     */
    List<T> queryLimit(QueryContext context, int limitFrom, int limitSize,
            OrderBy... orderBy) throws Exception;

    /**
     * 分页查询所有
     * 
     * @param pageNo
     *            起始页号，最小为第一页
     * @param pageSize
     *            每页记录数
     * @param order
     *            排序
     * @return 分页对象
     * @throws Exception
     */
    PageVo<T> queryWithPage(int pageNo, int pageSize, OrderBy... orderBy)
            throws Exception;

    /**
     * 根据字段值分页查询
     * 
     * @param pageNo
     *            起始页号，最小为第一页
     * @param pageSize
     *            每页记录数
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @param order
     *            排序
     * @return 分页对象
     * @throws Exception
     */
    PageVo<T> queryWithPage(int pageNo, int pageSize, String field,
            Object value, OrderBy... orderBy) throws Exception;

    /**
     * 根据多个字段值分页查询
     * 
     * @param pageNo
     *            起始页号，最小为第一页
     * @param pageSize
     *            每页记录数
     * @param fields
     *            多个字段名
     * @param values
     *            多个字段值
     * @param order
     *            排序
     * @return 分页对象
     * @throws Exception
     */
    PageVo<T> queryWithPage(int pageNo, int pageSize, String[] fields,
            Object[] values, OrderBy... orderBy) throws Exception;

    /**
     * 根据多个字段值分页查询
     * 
     * @param pageNo
     *            起始页号，最小为第一页
     * @param pageSize
     *            每页记录数
     * @param Map
     *            字段:字段值
     * @param order
     *            排序
     * @return 分页对象
     * @throws Exception
     */
    PageVo<T> queryWithPage(int pageNo, int pageSize,
            Map<String, Object> paramMap, OrderBy... orderBy) throws Exception;

    /**
     * 使用查询上下文分页查询
     * 
     * @param pageNo
     *            起始页号，最小为第一页
     * @param pageSize
     *            每页记录数
     * @param context
     *            查询上下文
     * @param order
     *            排序
     * @return 分页对象
     * @throws YichaDaoException
     */
    PageVo<T> queryWithPage(int pageNo, int pageSize, QueryContext context,
            OrderBy... orderBy) throws Exception;

    /**
     * 强制新增
     * 
     * @param t
     *            实体类实例
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    void save(T... t) throws Exception;

    void batchSave(List<T> t) throws Exception;

    /**
     * 强制更新
     * 
     * @param t
     *            实体类实例
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    void update(T... t) throws Exception;

    /**
     * 更新字段值
     * 
     * @param pk
     *            主键字段名
     * @param field
     *            字段名
     * @param value
     *            字段值
     * @throws Exception
     */
    void updateField(Serializable pk, String field, Object value)
            throws Exception;

    /**
     * 更新多个字段值
     * 
     * @param pk
     *            主键字段名
     * @param fields
     *            字段名
     * @param values
     *            字段值
     * @throws Exception
     */
    void updateField(Serializable pk, String[] fields, Object[] values)
            throws Exception;

}
