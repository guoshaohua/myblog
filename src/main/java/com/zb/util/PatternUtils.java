package com.zb.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;

/**
 * 正则表达式工具类
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:35:10
 */
public class PatternUtils {

    /**
     * 搜索整个字符串
     *
     * @param pattern
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @return 返回搜索结果
     */
    public static final String search(final Pattern pattern, final String str) {
        return search(pattern, str, 0);
    }

    /**
     * 搜索指定的分组
     *
     * @param pattern
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @param groupNum
     *            分组序号，0代表整个正则表达式
     * @return 返回分组搜索结果
     */
    public static final String search(final Pattern pattern, final String str,
            final int groupNum) {
        String[] arr = searchForArray(pattern, str, groupNum);
        return ArrayUtils.isEmpty(arr) ? null : arr[0];
    }

    /**
     * 搜索整个字符串
     *
     * @param patternStr
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @return 返回搜索结果
     */
    public static final String search(final String patternStr, final String str) {
        return search(patternStr, str, 0);
    }

    /**
     * 搜索指定的分组
     *
     * @param patternStr
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @param groupNum
     *            分组序号，0代表整个正则表达式
     * @return 返回分组搜索结果
     */
    public static final String search(final String patternStr,
            final String str, final int groupNum) {
        String[] arr = searchForArray(patternStr, str, groupNum);
        return ArrayUtils.isEmpty(arr) ? null : arr[0];
    }

    /**
     * 批量搜索
     *
     * @param pattern
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @param groupNum
     *            分组序号，0代表整体，其余代表分组
     * @return 按分组的次序返回搜索结果字符串数组
     */
    public static final String[] searchForArray(final Pattern pattern,
            final String str, final int... groupNum) {
        final Matcher matcher = pattern.matcher(str);
        if (groupNum.length < 1) {
            return matcher.find() ? new String[] { matcher.group() } : ArrayUtils.EMPTY_STRING_ARRAY;
        }
        final String[] result = new String[groupNum.length];
        if (matcher.find()) {
            for (int i = 0, len = result.length; i < len; i++) {
                result[i] = matcher.group(groupNum[i]);
            }
        }
        return result;
    }

    /**
     * 批量搜索
     *
     * @param patternStr
     *            正则表达式
     * @param str
     *            被搜索的字符串
     * @param groupNum
     *            分组序号，0代表整体，其余代表分组
     * @return 按分组的次序返回搜索结果字符串数组
     */
    public static final String[] searchForArray(final String patternStr,
            final String str, final int... groupNum) {
        final Pattern pattern = Pattern.compile(patternStr);
        return searchForArray(pattern, str, groupNum);
    }

}
