package com.zb.util;

import java.text.MessageFormat;

import org.apache.log4j.Logger;


/**
 * 日志格式化工具
 * 
 * 作者: zhoubang 
 * 日期：2015年4月28日 上午9:23:31
 */
public class LoggerUtil {
    private static Logger logger = Logger.getLogger(LoggerUtil.class);
    
    /**
     * 格式化日志，带有{0}这样子的消息格式<br/>
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午9:54:03
     * @param clazz
     * @param msg
     * @param arguments
     */
    public static void LoggerFormatPattern(String msg,Object... arguments){
        logger.debug(MessageFormat.format(msg, arguments));
    }
    
    /**
     * 直接输出消息
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午10:30:53
     * @param msg
     */
    public static void LoggerWrite(String msg){
        LoggerFormatPattern(msg, "");
    }
    
    
    public static void LoggerWarn(String msg,Throwable obj){
        logger.warn(msg, obj);
    }
}

