package com.zb.util.mail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 邮件内容实体类
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:30:02
 */
public class MailEntity implements Serializable {

    private static final long serialVersionUID = -5979690002116071430L;

    private String from;
    private String subject;
    private String content;
    private List<String> receivers = new ArrayList<String>();
    private List<String> inlines = new ArrayList<String>();
    private boolean html = false;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public List<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<String> receivers) {
        this.receivers = receivers;
    }

    public boolean isHtml() {
        return html;
    }

    public void setHtml(boolean html) {
        this.html = html;
    }

    public List<String> getInlines() {
        return inlines;
    }

    public void setInlines(List<String> inlines) {
        this.inlines = inlines;
    }

}
