package com.zb.util.mail;

import java.io.File;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.zb.util.ArrayUtil;

/**
 * 
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:30:10
 */
@Component("mailUtils")
public class MailUtils {

    @Autowired
    private JavaMailSender javaMailSender;

    public boolean send(MailEntity entity) {
        if (entity.isHtml()) {
            sendHtml(entity);
        } else {
            sendTxt(entity);
        }
        return true;
    }

    private void sendTxt(MailEntity entity) {
        SimpleMailMessage mail = new SimpleMailMessage();
        try {
            mail.setTo(ArrayUtil.toArray(entity.getReceivers(), String.class));// 接受者
            mail.setFrom(entity.getFrom());// 发送者,这里还可以另起Email别名，不用和xml里的username一致
            mail.setSubject(entity.getSubject());// 主题
            mail.setText(entity.getContent());// 邮件内容
            javaMailSender.send(mail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendHtml(MailEntity entity) {
        try {
            JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
            MimeMessage mailMessage = senderImpl.createMimeMessage();
            // 设置utf-8或GBK编码，否则邮件会有乱码
            MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");
            messageHelper.setTo(ArrayUtil.toArray(entity.getReceivers(), String.class));// 接受者
            messageHelper.setFrom(entity.getFrom());// 发送者,这里还可以另起Email别名，不用和xml里的username一致
            messageHelper.setSubject(entity.getSubject());// 主题
            // 邮件内容，注意加参数true
            messageHelper.setText(entity.getContent(), true);
            // 附件内容
            List<String> inlines = entity.getInlines();
            if (CollectionUtils.isNotEmpty(inlines)) {
                for (int i = 0; i < inlines.size(); i++) {
                    messageHelper.addInline(i + "", new File(inlines.get(i)));
                }
            }
            // 这里的方法调用和插入图片是不同的，使用MimeUtility.encodeWord()来解决附件名称的中文问题
            // File file=new File("E:/测试中文文件.rar");
            // messageHelper.addAttachment(MimeUtility.encodeWord(file.getName()),
            // file);
            javaMailSender.send(mailMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
