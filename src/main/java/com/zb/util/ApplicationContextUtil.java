package com.zb.util;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:32:08
 */
public class ApplicationContextUtil {

    private static ClassPathXmlApplicationContext instance;

    public static synchronized ClassPathXmlApplicationContext getInstance() {
        if (instance == null) {
            instance = new ClassPathXmlApplicationContext("classpath:/spring/applicationContext.xml");
        }
        return instance;
    }
}