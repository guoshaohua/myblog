package com.zb.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Map操作
 * 
 * 作者: zhoubang 日期：2015年4月2日 上午11:18:49
 */
public class MapUtil {

    /**
     * 获取一个Map实例
     * 
     * 作者: zhoubang 日期：2015年4月2日 上午11:21:17
     * 
     * @return
     */
    public static final Map<String, Object> newEmptyMap() {
        return new HashMap<String, Object>();
    }
}
