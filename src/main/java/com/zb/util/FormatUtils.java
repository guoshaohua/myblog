package com.zb.util;

import java.text.NumberFormat;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 格式化工具类
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:34:14
 */
public class FormatUtils {

    /**
     * 格式化URL, 去除多余的斜杠
     *
     * @param url
     *            URL
     * @return 格式化后的URL
     */
    public static final String formatURL(final String url) {
        if (StringUtils.isBlank(url)) {
            return url;
        }
        final int idx = url.indexOf("://");
        final StringBuffer sb = new StringBuffer();
        final boolean isNotNeg = idx != -1;
        if (isNotNeg) {
            sb.append(url.substring(0, idx + 3));
        }
        sb.append(url.substring(isNotNeg ? idx + 3 : 0).replaceAll("\\\\", "/").replaceAll("/{2,}", "/"));
        return sb.toString();
    }

    /**
     * 格式化文件地址
     *
     * @param path
     *            文件路径
     * @return 格式化后的文件路径
     */
    public static final String formatFilePath(final String path) {
        return path == null ? null : FilenameUtils.normalizeNoEndSeparator(path, true);
    }

    /**
     * 格式化数字, 位数不足将补足0,如: formatNumber(100,5)将返回"00100";
     *
     * @param number
     *            数字
     * @param len
     *            总长度
     * @return 格式化后的数字字符串
     */
    public static final String formatNumber(final long number, final int len) {
        final NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setMinimumIntegerDigits(len);
        numberFormat.setMaximumIntegerDigits(len);
        numberFormat.setGroupingUsed(false);
        return numberFormat.format(number);
    }

}
