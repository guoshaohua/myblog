package com.zb.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * common.settings.properties配置文件读取
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午1:32:55
 */
public class CommonSettingsUtil {

    private final static String configFile = "/properties/common.properties";

    private final static String DEFAULT_CHARSET = "default.charset";
    private final static String DEFAULT_QRCODE_SIZE = "default.qrcode.size";
    private final static String DEFAULT_QRCODE_FORMAT = "default.qrcode.format";
    private final static String DEFAULT_HTTP_TIMEOUT = "default.http.timeout";
    private final static String DEFAULT_HTTP_HOST = "default.http.proxy.host";
    private final static String DEFAULT_HTTP_PROXY_PORT = "default.http.proxy.port";
    private final static String DEFAULT_REDIS_EXPIRE = "default.redis.expire";

    private static CommonSettingsUtil instance;
    private Configuration config;

    private CommonSettingsUtil() throws ConfigurationException {
        config = new PropertiesConfiguration(configFile);
    }

    public static synchronized CommonSettingsUtil getInstance() {
        if (instance == null) {
            try {
                instance = new CommonSettingsUtil();
            } catch (ConfigurationException e) {
                throw new RuntimeException(e);
            }
        }
        return instance;
    }

    public String getDefaultCharset() {
        return config.getString(DEFAULT_CHARSET);
    }

    public String[] getDefaultQrcodeSize() {
        return config.getStringArray(DEFAULT_QRCODE_SIZE);
    }

    public String getDefaultQrcodeFormat() {
        return config.getString(DEFAULT_QRCODE_FORMAT);
    }

    public Integer getDefaultHttpTimeout() {
        return config.getInteger(DEFAULT_HTTP_TIMEOUT, 3000);
    }

    public String getDefaultHttpHost() {
        return config.getString(DEFAULT_HTTP_HOST);
    }

    public Integer getDefaultHttpProxyPort() {
        return config.getInteger(DEFAULT_HTTP_PROXY_PORT, -1);
    }

    public Integer getDefaultRedisExpire() {
        return config.getInteger(DEFAULT_REDIS_EXPIRE, 60 * 60 * 24 * 7);
    }

}
