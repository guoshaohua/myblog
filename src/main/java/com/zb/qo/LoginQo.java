package com.zb.qo;

import java.io.Serializable;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 登录参数绑定
 * 
 * 作者: zhoubang 日期：2015年4月17日 下午5:38:57
 */
public class LoginQo implements Serializable {

    private static final long serialVersionUID = -4830309263466309037L;

    @NotEmpty
    private String userName;
    @NotEmpty
    private String password;
    
    private String rememberMe;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(String rememberMe) {
        this.rememberMe = rememberMe;
    }

}
