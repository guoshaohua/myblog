package com.zb.qo;

import java.io.Serializable;
import java.util.Date;
import org.hibernate.validator.constraints.NotEmpty;
import com.zb.bean.User;
import com.zb.enums.Enums;
import com.zb.util.CryptoUtils;

/**
 * 会员注册参数映射辅助类
 * 
 * 作者: zhoubang 
 * 日期：2015年4月22日 下午5:09:33
 */
public class RegisterQo implements Serializable{

    private static final long serialVersionUID = 5868367911634110676L;
    
    @NotEmpty
    private String userName;// 账户名
    
    @NotEmpty
    private String password;// 密码
    
    @NotEmpty
    private String passwordOk;// 确认密码
    
    private String email;// 邮箱
    private String realName;// 真实姓名
    private String phone;// 手机号
    private String openId;
    
    public String getPasswordOk() {
        return passwordOk;
    }
    public void setPasswordOk(String passwordOk) {
        this.passwordOk = passwordOk;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getRealName() {
        return realName;
    }
    public void setRealName(String realName) {
        this.realName = realName;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    
    public String getOpenId() {
        return openId;
    }
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    /**
     * 转换为User对象
     * 
     * 作者: zhoubang 
     * 日期：2015年4月22日 下午5:21:39
     * @return
     */
    public User buildUserInfo(){
        User user = new User();
        user.setUserName(this.userName);
        user.setPassword(CryptoUtils.encodeMD5(this.password));
        user.setEmail(this.email);
        user.setPhone(this.phone);
        user.setStatus(Enums.UserStatus.OK.getStatus());
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setRealName(this.realName);
        user.setOpenId(this.openId);
        return user;
    }
}

