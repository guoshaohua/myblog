package com.zb.qo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class PermissionQo extends BaseQo implements Serializable{

    private static final long serialVersionUID = -3421253391668114772L;
    
    private String name;
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public static Map<String, Object> buildMap(PermissionQo permissionQo) throws UnsupportedEncodingException{
        Map<String, Object> queryMap = new HashMap<String, Object>();
        if(permissionQo != null) {
            if(StringUtils.isNotBlank(permissionQo.getName())) {
                queryMap.put("name", URLDecoder.decode(permissionQo.getName(), "UTF-8"));
            }
        }
        return queryMap;
    }
}

