package com.zb.qo;

import java.io.Serializable;

/**
 * 导航管理 - 导航查询辅助类
 * 
 * 作者: zhoubang 
 * 日期：2015年5月6日 下午2:31:04
 */
public class NavigationQo extends BaseQo implements Serializable{
    
    
    private static final long serialVersionUID = -1534735268563564668L;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

