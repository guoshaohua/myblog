package com.zb.qo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.zb.bean.Role;

public class RoleQo extends BaseQo implements Serializable{

    private static final long serialVersionUID = -766827593948633606L;
    
    /**
     * 用户角色列表查询
     */
    private String name;
    private int status;
    
    /**
     * 用于角色信息更新
     */
    private String id;//角色id
    private String description;//描述
    private String permissionIds;//赋予角色的权限id,逗号分割
    
    
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPermissionIds() {
        return permissionIds;
    }
    public void setPermissionIds(String permissionIds) {
        this.permissionIds = permissionIds;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    
    /**
     * 更新角色信息
     *      对象转换
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午5:49:31
     * @param roleQo
     * @return
     */
    public static Role buildUpdateRole(RoleQo roleQo){
        Role role = new Role();
        role.setDescription(roleQo.getDescription());
        return role;
    }
    
    public static Map<String, Object> buildMap(RoleQo roleQo) throws UnsupportedEncodingException{
        Map<String, Object> queryMap = new HashMap<String, Object>();
        if(roleQo != null) {
            if(StringUtils.isNotBlank(roleQo.getName())) {
                queryMap.put("name", URLDecoder.decode(roleQo.getName(), "UTF-8"));
            }
            if(roleQo.getStatus() > 0) {
                queryMap.put("status", roleQo.getStatus());
            }
        }
        return queryMap;
    }
}
