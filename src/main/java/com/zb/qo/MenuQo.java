package com.zb.qo;

import java.io.Serializable;

public class MenuQo extends BaseQo implements Serializable {

    private static final long serialVersionUID = -2218656600153992039L;
    
    private String id;
    private String name;
    private String pid;
    private String link;
    private int available;
    private int model;
    
    private int type;
    
    
    
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public int getAvailable() {
        return available;
    }
    public void setAvailable(int available) {
        this.available = available;
    }
    public int getModel() {
        return model;
    }
    public void setModel(int model) {
        this.model = model;
    }
    
    
}

