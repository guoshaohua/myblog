package com.zb.qo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.zb.service.ArticleService;
import com.zb.util.ApplicationContextUtil;

/**
 * 文章查询辅助对象
 * 
 * @author zhoubang
 *
 */
public class ArticleQo extends BaseQo implements Serializable {

    private static final long serialVersionUID = -6202540094547365750L;

    /**
     * 模块类型id
     */
    private String navigationId;

    public String getNavigationId() {
        return navigationId;
    }

    public void setNavigationId(String navigationId) {
        this.navigationId = navigationId;
    }

    /**
     * 获取该模块的父级模块名称集合，“-”分割 调用方法： 在页面上使用当前文章对象.parentNames的方式获取结果 例如：
     * ${articleObj.parentNames}
     * 
     * 作者: zhoubang 日期：2015年4月16日 下午2:16:10
     * 
     * @return
     * @throws Exception
     */
    public List<String> getParentNames() throws Exception {
        ArticleService articleService = (ArticleService) ApplicationContextUtil.getInstance().getBean("articleServiceImpl");
        List<String> ls = null;
        if (navigationId != null && !"".equals(navigationId.trim())) {
            String result = articleService.getParentNameList(navigationId);
            if (result.split("-").length > 0) {
                ls = new ArrayList<String>();
                for (int i = 0; i < result.split("-").length; i++) {
                    ls.add(result.split("-")[i]);
                }
            }
        }
        return ls;
    }

    @Override
    public String toString() {
        return "ArticleQo [navigationId=" + navigationId + "]";
    }
}
