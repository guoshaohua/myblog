package com.zb.qo;

import java.io.Serializable;

import com.zb.enums.Enums;

/**
 * 公共的查询辅助对象
 * 
 * 作者: zhoubang 日期：2015年4月3日 下午4:23:13
 */
public class BaseQo implements Serializable {

    private static final long serialVersionUID = 4833495824786598344L;

    /**
     * 标识是否是ajax请求
     */
    private boolean ajaxRequest;

    /**
     * 默认查询第一页
     */
    private Integer pageNo = 1;

    /**
     * 每页大小
     */
    private Integer pageSize = Enums.Page.DEFAULT_PAGE_SIZE.getValue();

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public boolean getAjaxRequest() {
        return ajaxRequest;
    }

    public void setAjaxRequest(boolean ajaxRequest) {
        this.ajaxRequest = ajaxRequest;
    }
}
