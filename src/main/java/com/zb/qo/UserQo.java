package com.zb.qo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.zb.bean.User;

public class UserQo extends BaseQo implements Serializable{

    private static final long serialVersionUID = -3522708708256606970L;
    
    
    private String userName;
    private String phone;
    private int status;
    
    /**
     * 更新会员信息之用
     */
    private String id;
    private String email;
    private String roleIds;//为会员赋予的角色id列表
    
    
    public String getRoleIds() {
        return roleIds;
    }
    public void setRoleIds(String roleIds) {
        this.roleIds = roleIds;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * 转换成map
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:19:56
     * @return
     */
    public static Map<String, Object> buildMap(UserQo userQo){
        Map<String, Object> queryMap = new HashMap<String, Object>();
        if(userQo != null) {
            if(StringUtils.isNotBlank(userQo.getUserName())) {
                queryMap.put("userName", userQo.getUserName());
            }
            if(userQo.getStatus() > 0) {
                queryMap.put("status", userQo.getStatus());
            }
        }
        return queryMap;
    }
    
    /**
     * 更新会员信息，将参数转换为user对象
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午11:02:59
     * @param userQo
     * @return
     */
    public static User buildUser(UserQo userQo){
        User user = new User();
        user.setId(userQo.getId());
        user.setUserName(userQo.getUserName());
        user.setPhone(userQo.getPhone());
        user.setEmail(userQo.getEmail());
        user.setUpdateTime(new Date());
        return user;
    }
}

