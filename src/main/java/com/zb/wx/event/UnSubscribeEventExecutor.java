package com.zb.wx.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wxinf.rece.pojo.request.event.BaseEventRequest;
import com.wxinf.rece.pojo.request.event.UnSubscribeEventRequest;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.press.IWXEventExecutor;

public class UnSubscribeEventExecutor implements IWXEventExecutor {

	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	public BaseMessageResponse executor(BaseEventRequest baseEventRequest) {
		System.out.println("[获取取消关注事件：]" + baseEventRequest);
		// 获取取消关注事件请求类
		UnSubscribeEventRequest unSubscribeEventRequest = (UnSubscribeEventRequest) baseEventRequest;
		String msgType = unSubscribeEventRequest.getMsgType();
		String event = unSubscribeEventRequest.getEvent();
		String fromUserName = unSubscribeEventRequest.getFromUserName();
		String toUserName = unSubscribeEventRequest.getToUserName();
		Long createTime = unSubscribeEventRequest.getCreateTime();
		
		
		// 删除关注用户信息
		// ApplicationContext context = ApplicationContextUtil.getInstance();
		// IUserService userService =
		// (IUserService)context.getBean("userService");
		// userService.delUser(fromUserName);
		return null;
	}
}
