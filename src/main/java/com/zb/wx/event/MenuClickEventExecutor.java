package com.zb.wx.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxinf.rece.pojo.request.event.BaseEventRequest;
import com.wxinf.rece.pojo.request.event.MenuEventRequest;
import com.wxinf.rece.pojo.response.Article;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.pojo.response.message.NewsMessageResponse;
import com.wxinf.rece.pojo.response.message.TextMessageResponse;
import com.wxinf.rece.press.IWXEventExecutor;
import com.wxinf.utils.WXConstants;

public class MenuClickEventExecutor implements IWXEventExecutor {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Override
    public BaseMessageResponse executor(BaseEventRequest baseEventRequest) {
        System.out.println("[获取用户点击菜单请求事件：]" + baseEventRequest);
        // 获取用户点击菜单请求事件类
        MenuEventRequest menuEventRequest = (MenuEventRequest) baseEventRequest;
        String fromUserName = menuEventRequest.getFromUserName();
        String toUserName = menuEventRequest.getToUserName();
        String eventKey = menuEventRequest.getEventKey();

        if (eventKey.equals("about_us")) {
            NewsMessageResponse newsMessage = new NewsMessageResponse();
            newsMessage.setToUserName(fromUserName);
            newsMessage.setFromUserName(toUserName);
            newsMessage.setCreateTime(new Date().getTime());
            newsMessage.setMsgType(WXConstants.RSP_MESSAGE_TYPE_NEWS);
            // newsMessage.setFuncFlag(0);

            Article article1 = new Article();
            article1.setTitle("［车到巴士］关于我们");
            article1.setPicUrl("https://mmbiz.qlogo.cn/mmbiz/MQfvBqD9sYXSE37eia8qL6zf2E1UHR2lGDfwJ599WoWL3KvS18QDkVpvOu87Eerficc0iaOI0j5XYM0a0libsVBQyQ/0?wx_fmt=jpeg");
            article1.setUrl("http://mp.weixin.qq.com/s?__biz=MzAwNzcwMjA0OA==&mid=216821633&idx=1&sn=11b1c56f5fd952ed9b8616238b63bc8e#rd");

            Article article2 = new Article();
            article2.setTitle("［车到巴士］预定问题");
            article2.setPicUrl("https://mmbiz.qlogo.cn/mmbiz/MQfvBqD9sYXSE37eia8qL6zf2E1UHR2lGQlt75YDVAhhXlDTvgNvcrPyhT5ia4Cxic6ibHZhZTVUJxa0jFCpT77WCA/0?wx_fmt=jpeg");
            article2.setUrl("http://mp.weixin.qq.com/s?__biz=MzAwNzcwMjA0OA==&mid=216821633&idx=2&sn=e7dea83344527c0611edf23f38313b84#rd");

            Article article3 = new Article();
            article3.setTitle("［车到巴士］退款问题");
            article3.setPicUrl("https://mmbiz.qlogo.cn/mmbiz/MQfvBqD9sYXSE37eia8qL6zf2E1UHR2lGiba8hwXGqcSDpiaHjAVm10QxAN6TyLy2EN4zWrqJCibiaEicGogFmVqshRg/0?wx_fmt=jpeg");
            article3.setUrl("http://mp.weixin.qq.com/s?__biz=MzAwNzcwMjA0OA==&mid=216821633&idx=3&sn=756a443967f67d2820a3c76051a9b066#rd");

            Article article4 = new Article();
            article4.setTitle("［车到巴士］常见问题");
            article4.setPicUrl("https://mmbiz.qlogo.cn/mmbiz/MQfvBqD9sYXSE37eia8qL6zf2E1UHR2lGDhglUWkDtDucneykt8Lah4jQ91hJv6TB828lY7wS0N69YMu3ebkAKg/0?wx_fmt=jpeg");
            article4.setUrl("http://mp.weixin.qq.com/s?__biz=MzAwNzcwMjA0OA==&mid=216821633&idx=4&sn=191a3261ef7e0e83ba5dde6ca9daa350#rd");

            Article article5 = new Article();
            article5.setTitle("［车到巴士］使用协议");
            article5.setPicUrl("https://mmbiz.qlogo.cn/mmbiz/MQfvBqD9sYXSE37eia8qL6zf2E1UHR2lG9DOCeF19Rr1NBwHdnk4suTiabicoujZibIHsEKJv7ibeFEvdFibCElyThBw/0?wx_fmt=jpeg");
            article5.setUrl("http://mp.weixin.qq.com/s?__biz=MzAwNzcwMjA0OA==&mid=216821633&idx=5&sn=130fbcaf4e4fbaadfa89cc49a5478242#rd");

            List<Article> articleList = new ArrayList<Article>();
            articleList.add(article1);
            articleList.add(article2);
            articleList.add(article3);
            articleList.add(article4);
            articleList.add(article5);
            newsMessage.setArticleCount(articleList.size());
            newsMessage.setArticles(articleList);
            return newsMessage;
        }
        // 返回欢迎文本消息
        TextMessageResponse textMessageResponse = new TextMessageResponse();
        textMessageResponse.setFromUserName(toUserName);
        textMessageResponse.setToUserName(fromUserName);
        textMessageResponse.setMsgType(WXConstants.RSP_MESSAGE_TYPE_TEXT);
        textMessageResponse.setCreateTime(System.currentTimeMillis());
        textMessageResponse.setContent("服务暂未开放，敬请期待。");
        return textMessageResponse;
    }
}
