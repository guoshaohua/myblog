package com.zb.wx.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxinf.rece.pojo.request.event.BaseEventRequest;
import com.wxinf.rece.pojo.request.event.NewsFinishEventRequest;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.pojo.response.message.TextMessageResponse;
import com.wxinf.rece.press.IWXEventExecutor;
import com.wxinf.utils.WXConstants;

public class NewsFinishEventExecutor implements IWXEventExecutor{
    
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    
    @Override
    public BaseMessageResponse executor(BaseEventRequest baseEventRequest) {
        System.out.println("[获取图文消息发送完成请求事件：]" + baseEventRequest);
        // 获取图文消息发送完成请求事件类
        NewsFinishEventRequest newsFinishEventRequest = (NewsFinishEventRequest)baseEventRequest;
        String msgType = newsFinishEventRequest.getMsgType();
        String event = newsFinishEventRequest.getEvent();
        String fromUserName = newsFinishEventRequest.getFromUserName();
        String toUserName = newsFinishEventRequest.getToUserName();
        Long createTime = newsFinishEventRequest.getCreateTime();
        
        // TODO Auto-generated method stub
        
        // 返回欢迎文本消息
        TextMessageResponse textMessageResponse = new TextMessageResponse();
        textMessageResponse.setFromUserName(toUserName);
        textMessageResponse.setToUserName(fromUserName);
        textMessageResponse.setMsgType(WXConstants.RSP_MESSAGE_TYPE_TEXT);
        textMessageResponse.setCreateTime(System.currentTimeMillis());
        textMessageResponse.setContent("图文消息发送完成服务未完成！");
        return textMessageResponse;
    }
}
