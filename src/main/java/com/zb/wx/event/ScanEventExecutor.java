package com.zb.wx.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxinf.rece.pojo.request.event.BaseEventRequest;
import com.wxinf.rece.pojo.request.event.QRCodeEventRequest;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.pojo.response.message.TextMessageResponse;
import com.wxinf.rece.press.IWXEventExecutor;
import com.wxinf.utils.WXConstants;

public class ScanEventExecutor implements IWXEventExecutor {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    
    @Override
    public BaseMessageResponse executor(BaseEventRequest baseEventRequest) {
        System.out.println("[获取扫描二维码请求事件：]" + baseEventRequest);
        // 获取扫描二维码请求事件类
        QRCodeEventRequest qrCodeEventRequest = (QRCodeEventRequest)baseEventRequest;
        String msgType = qrCodeEventRequest.getMsgType();
        String event = qrCodeEventRequest.getEvent();
        String fromUserName = qrCodeEventRequest.getFromUserName();
        String toUserName = qrCodeEventRequest.getToUserName();
        Long createTime = qrCodeEventRequest.getCreateTime();
        
        // TODO Auto-generated method stub
        
        // 返回欢迎文本消息
        TextMessageResponse textMessageResponse = new TextMessageResponse();
        textMessageResponse.setFromUserName(toUserName);
        textMessageResponse.setToUserName(fromUserName);
        textMessageResponse.setMsgType(WXConstants.RSP_MESSAGE_TYPE_TEXT);
        textMessageResponse.setCreateTime(System.currentTimeMillis());
        textMessageResponse.setContent("扫描二维码服务未完成！");
        return textMessageResponse;
    }
}
