package com.zb.wx.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxinf.rece.pojo.request.event.BaseEventRequest;
import com.wxinf.rece.pojo.request.event.LocationEventRequest;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.pojo.response.message.TextMessageResponse;
import com.wxinf.rece.press.IWXEventExecutor;
import com.wxinf.utils.WXConstants;

public class LocationEventExecutor implements IWXEventExecutor {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    
    @Override
    public BaseMessageResponse executor(BaseEventRequest baseEventRequest) {
        System.out.println("[获取用户定位服务请求事件：]" + baseEventRequest);
        // 获取用户定位服务请求事件类
        LocationEventRequest locationEventRequest = (LocationEventRequest)baseEventRequest;
        String msgType = locationEventRequest.getMsgType();
        String event = locationEventRequest.getEvent();
        String fromUserName = locationEventRequest.getFromUserName();
        String toUserName = locationEventRequest.getToUserName();
        Long createTime = locationEventRequest.getCreateTime();
        
        // TODO Auto-generated method stub
        
        // 返回欢迎文本消息
        TextMessageResponse textMessageResponse = new TextMessageResponse();
        textMessageResponse.setFromUserName(toUserName);
        textMessageResponse.setToUserName(fromUserName);
        textMessageResponse.setMsgType(WXConstants.RSP_MESSAGE_TYPE_TEXT);
        textMessageResponse.setCreateTime(System.currentTimeMillis());
        textMessageResponse.setContent("用户定位服务未完成！");
        return textMessageResponse;
    }
}
