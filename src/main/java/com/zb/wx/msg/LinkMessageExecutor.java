package com.zb.wx.msg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wxinf.rece.pojo.request.message.BaseMessageRequest;
import com.wxinf.rece.pojo.request.message.LinkMessageRequest;
import com.wxinf.rece.pojo.response.Article;
import com.wxinf.rece.pojo.response.message.BaseMessageResponse;
import com.wxinf.rece.pojo.response.message.NewsMessageResponse;
import com.wxinf.rece.pojo.response.message.TextMessageResponse;
import com.wxinf.rece.press.IWXMessageExecutor;
import com.wxinf.utils.WXConstants;

public class LinkMessageExecutor implements IWXMessageExecutor {
    
    private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    
    @Override
    public BaseMessageResponse executor(BaseMessageRequest baseMessageRequest) {
        System.out.println("[获取链接消息请求事件：]" + baseMessageRequest);
        // 获取链接消息请求事件类
        LinkMessageRequest linkMessageRequest = (LinkMessageRequest)baseMessageRequest;
        Long msgId = linkMessageRequest.getMsgId();
        String msgType = linkMessageRequest.getMsgType();
        String fromUserName = linkMessageRequest.getFromUserName();
        String toUserName = linkMessageRequest.getToUserName();
        Long createTime = linkMessageRequest.getCreateTime();
        
        // TODO Auto-generated method stub
        
        // 返回欢迎文本消息
        /*TextMessageResponse textMessageResponse = new TextMessageResponse();
        textMessageResponse.setFromUserName(toUserName);
        textMessageResponse.setToUserName(fromUserName);
        textMessageResponse.setMsgType(WXConstants.RSP_MESSAGE_TYPE_TEXT);
        textMessageResponse.setCreateTime(System.currentTimeMillis());
        textMessageResponse.setContent("链接消息服务未完成！");
        return textMessageResponse;*/
        
     // 创建图文消息
        NewsMessageResponse newsMessage = new NewsMessageResponse();
        newsMessage.setToUserName(fromUserName);
        newsMessage.setFromUserName(toUserName);
        newsMessage.setCreateTime(new Date().getTime());
        newsMessage.setMsgType(WXConstants.RSP_MESSAGE_TYPE_NEWS);
        //newsMessage.setFuncFlag(0);
        
        Article article1 = new Article();
        article1.setTitle("[JAVA技术分享] 欢迎关注我的个人公众号^_^");
        article1.setPicUrl("https://mp.weixin.qq.com/misc/getheadimg?token=1129909443&fakeid=3088733098&r=973383");
        article1.setUrl("http://www.2b2b92b.com");

        Article article2 = new Article();
        article2.setTitle("[博客网址] www.2b2b92b.com");
        article2.setPicUrl("https://mp.weixin.qq.com/misc/getheadimg?token=1129909443&fakeid=3088733098&r=973383");
        article2.setUrl("http://www.2b2b92b.com");

        Article article3 = new Article();
        article3.setTitle("［联系邮箱］842324724@qq.com");
        article3.setPicUrl("https://mp.weixin.qq.com/misc/getheadimg?token=1129909443&fakeid=3088733098&r=973383");
        article3.setUrl("http://www.2b2b92b.com");
        
        Article article4 = new Article();
        article4.setTitle("提倡开源，分享快乐。");
        article4.setPicUrl("https://mp.weixin.qq.com/misc/getheadimg?token=1129909443&fakeid=3088733098&r=973383");
        article4.setUrl("http://www.2b2b92b.com");
        
        Article article5 = new Article();
        article5.setTitle("共同进步，共同学习。");
        article5.setPicUrl("https://mp.weixin.qq.com/misc/getheadimg?token=1129909443&fakeid=3088733098&r=973383");
        article5.setUrl("http://www.2b2b92b.com");
        
        
        List<Article> articleList = new ArrayList<Article>();
        articleList.add(article1);
        articleList.add(article2);
        articleList.add(article3);
        articleList.add(article4);
        articleList.add(article5);
        newsMessage.setArticleCount(articleList.size());
        newsMessage.setArticles(articleList);
        return newsMessage;
    }
}
