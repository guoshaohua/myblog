package com.zb.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 导航 实体bean
 * 
 * 作者: zhoubang 日期：2015年3月31日 下午1:39:41
 */
@Entity
@Table(name = "blog_navigation", catalog = "myblog")
public class Navigation implements java.io.Serializable {

    private static final long serialVersionUID = 3952055965613035451L;

    private String id;
    private String name;// 名称
    private String pid;// 父id
    private int indexNo;// 排序序号
    private int available;// 是否可用
    private String link;
    private int linkType;
    
    private String icon;
    
    private String parentName;//冗余字段，方便映射
    public Navigation() {
    }

    public Navigation(String id, String name, String pid, int indexNo,
            int available, String link, int linkType,String parentName) {
        super();
        this.id = id;
        this.name = name;
        this.pid = pid;
        this.indexNo = indexNo;
        this.available = available;
        this.link = link;
        this.linkType = linkType;
        this.parentName = parentName;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @Column(name = "icon", nullable = false)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "pid", nullable = false)
    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    @Column(name = "indexNo", nullable = false)
    public int getIndexNo() {
        return indexNo;
    }

    public void setIndexNo(int indexNo) {
        this.indexNo = indexNo;
    }

    @Column(name = "available", nullable = false)
    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    @Column(name = "link", nullable = true)
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Column(name = "linkType", nullable = false)
    public int getLinkType() {
        return linkType;
    }

    public void setLinkType(int linkType) {
        this.linkType = linkType;
    }
    
    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return "Navigation [id=" + id + ", name=" + name + ", pid=" + pid
                + ", indexNo=" + indexNo + ", available=" + available
                + ", link=" + link + ", linkType=" + linkType + ", parentName="
                + parentName + "]";
    }
}
