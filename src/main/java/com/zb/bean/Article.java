package com.zb.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 文章实体bean
 * 
 * 作者: zhoubang 日期：2015年4月2日 下午2:10:13
 */
@Entity
@Table(name = "blog_article", catalog = "myblog")
public class Article implements Serializable {

    private static final long serialVersionUID = 2316925504666443798L;

    private String id;
    private String title;
    private String content;
    private Date createTime;
    private Date updateTime;
    private String navigationId;
    private String userId;
    private String introduction;
    private int available;
    
    private String viewLink;
    private int isViewOpen;
    
    
    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "title", nullable = false)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "content", nullable = false)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Column(name = "update_time", nullable = false)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Column(name = "navigation_id", nullable = false)
    public String getNavigationId() {
        return navigationId;
    }

    public void setNavigationId(String navigationId) {
        this.navigationId = navigationId;
    }

    @Column(name = "user_id", nullable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "introduction", nullable = true)
    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    
    @Column(name = "available", nullable = false)
    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }
    
    
    @Column(name = "view_link", nullable = true)
    public String getViewLink() {
        return viewLink;
    }

    public void setViewLink(String viewLink) {
        this.viewLink = viewLink;
    }

    @Column(name = "is_view_open", nullable = false)
    public int getIsViewOpen() {
        return isViewOpen;
    }

    public void setIsViewOpen(int isViewOpen) {
        this.isViewOpen = isViewOpen;
    }

    public Article() {
    }

    public Article(String id, String title, String content, Date createTime,
            Date updateTime, String navigationId, String userId,
            String introduction,String viewLink,int isViewOpen) {
        super();
        this.id = id;
        this.title = title;
        this.content = content;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.navigationId = navigationId;
        this.userId = userId;
        this.introduction = introduction;
        this.viewLink = viewLink;
        this.isViewOpen = isViewOpen;
        
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", title=" + title + ", content="
                + content + ", createTime=" + createTime + ", updateTime="
                + updateTime + ", navigationId=" + navigationId + ", userId="
                + userId + ", introduction=" + introduction + ", available="
                + available + ", viewLink=" + viewLink + ", isViewOpen="
                + isViewOpen + "]";
    }

    
}
