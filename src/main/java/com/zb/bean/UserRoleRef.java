package com.zb.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户与角色 实体bean
 * 
 * 作者: zhoubang 日期：2015年3月31日 下午1:44:44
 */
@Entity
@Table(name = "blog_user_role", catalog = "myblog")
public class UserRoleRef implements java.io.Serializable {

    private static final long serialVersionUID = -457529927562556381L;

    private String id;
    private String userId;// 用户id
    private String roleId;// 角色id

    public UserRoleRef() {
    }

    public UserRoleRef(String id, String userId, String roleId) {
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "userId", nullable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "roleId", nullable = false)
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}
