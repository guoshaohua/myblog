package com.zb.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 菜单menu
 *      个人中心、后台管理
 * 
 * 作者: zhoubang 
 * 日期：2015年4月24日 下午5:10:29
 */
@Entity
@Table(name = "blog_menu", catalog = "myblog")
public class Menu implements Serializable{

    private static final long serialVersionUID = -4215056601623352072L;
    
    private String id;
    private String name;
    private String pid;
    private String link;
    private int available = 1;
    private int model;
    
    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name = "pid", nullable = false, length = 24)
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    
    @Column(name = "link", length = 100)
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    
    @Column(name = "available", nullable = false, length = 1)
    public int getAvailable() {
        return available;
    }
    public void setAvailable(int available) {
        this.available = available;
    }
    
    @Column(name = "model", nullable = false, length = 1)
    public int getModel() {
        return model;
    }
    public void setModel(int model) {
        this.model = model;
    }
    
}

