package com.zb.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 角色与权限 实体bean
 * 
 * 作者: zhoubang 日期：2015年3月31日 下午1:42:44
 */
@Entity
@Table(name = "blog_role_permission", catalog = "myblog")
public class RolePermissionRef implements java.io.Serializable {

    private static final long serialVersionUID = 6585552119085351995L;

    private String id;
    private String roleId;// 角色id
    private String permissionId;// 权限id

    public RolePermissionRef() {
    }

    public RolePermissionRef(String id, String roleId, String permissionId) {
        this.id = id;
        this.roleId = roleId;
        this.permissionId = permissionId;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "roleId", nullable = false)
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Column(name = "permissionId", nullable = false)
    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

}
