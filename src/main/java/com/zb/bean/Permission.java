package com.zb.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 权限 实体bean
 * 
 * 作者: zhoubang 日期：2015年3月31日 下午1:40:31
 */
@Entity
@Table(name = "blog_permission", catalog = "myblog")
public class Permission implements java.io.Serializable {

    private static final long serialVersionUID = 5310880992340095131L;

    private String id;
    private String name;// 名称
    private String code;// 权限代码
    private String description;// 描述
    private String parentId;//

    public Permission() {
    }

    public Permission(String id, String name, String code, String description,
            String parentId) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.description = description;
        this.parentId = parentId;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "code", unique = true, nullable = false)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "description", nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "parentId", nullable = true)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
