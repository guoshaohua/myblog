package com.zb.bean;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 角色 实体bean
 * 
 * 作者: zhoubang 日期：2015年3月31日 下午1:42:08
 */
@Entity
@Table(name = "blog_role", catalog = "myblog")
public class Role implements java.io.Serializable {

    private static final long serialVersionUID = -4697973230492786177L;

    private String id;
    private String name;// 名称
    private String description;// 描述
    private int status;// 状态
    private Date updateTime;// 最后更新时间
    private Date createTime;// 用户创建时间

    public Role() {
    }

    public Role(String id, String name, String description, int status,
            Date updateTime, Date createTime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.status = status;
        this.updateTime = updateTime;
        this.createTime = createTime;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false, length = 24)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "status", nullable = false)
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "updateTime", nullable = false)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Temporal(TemporalType.TIME)
    @Column(name = "createTime", nullable = false)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
