package com.zb.dao;

import java.util.List;

import com.zb.bean.Role;
import com.zb.orm.DaoSupport;

/**
 * 角色处理接口
 * 
 * 作者: zhoubang 日期：2015年3月26日 上午11:29:52
 */
public interface RoleDao extends DaoSupport<Role> {

    /**
     * 根据用户名获取角色列表
     * 
     * 作者: zhoubang 日期：2015年3月26日 下午1:52:32
     * 
     * @param userName
     * @return
     */
    public List<Role> getUserRoles(String userName) throws Exception;
}
