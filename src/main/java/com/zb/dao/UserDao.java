package com.zb.dao;

import com.zb.bean.User;
import com.zb.orm.DaoSupport;

/**
 * 用户处理接口
 * 
 * 作者：zhoubang 日期：2015年3月14日 上午1:37:58
 */
public interface UserDao extends DaoSupport<User> {

}
