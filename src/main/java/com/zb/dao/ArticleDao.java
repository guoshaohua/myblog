package com.zb.dao;

import java.util.List;
import java.util.Map;

import com.zb.bean.Article;
import com.zb.orm.DaoSupport;
import com.zb.qo.ArticleQo;
import com.zb.vo.PageVo;

public interface ArticleDao extends DaoSupport<Article> {

    /**
     * 获取文章列表
     * 
     * 作者: zhoubang 日期：2015年4月2日 下午3:02:02
     * 
     * @return
     * @throws Exception
     */
    public PageVo<Article> getArticleList(ArticleQo articleQo) throws Exception;
    
    /**
     * 根据id获取该菜单父级菜单的名称集合. 前端页面显示 —— 当前位置
     * 
     * 作者: zhoubang 日期：2015年4月16日 下午3:46:39
     * 
     * @param id
     * @return
     * @throws Exception
     */
    public String getParentNameList(String id) throws Exception;
    
    /**
     * 后台管理-文章列表查询-
     *      文章所属模块列表
     * 
     * 作者: zhoubang 
     * 日期：2015年5月8日 上午9:20:38
     * @return
     * @throws Exception
     */
    public List<Map<String, Object>> getArticleModels() throws Exception;
}
