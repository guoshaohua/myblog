package com.zb.dao;

import com.zb.bean.Menu;
import com.zb.orm.DaoSupport;

/**
 * 菜单接口
 * 
 * 作者: zhoubang 
 * 日期：2015年4月24日 下午5:20:08
 */
public interface MenuDao extends DaoSupport<Menu> {
    
}

