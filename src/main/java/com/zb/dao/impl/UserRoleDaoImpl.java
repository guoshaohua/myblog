package com.zb.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.UserRoleRef;
import com.zb.dao.UserRoleDao;
import com.zb.orm.AbstractDaoSupportImpl;


@Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
@Repository(value = "userRoleDaoImpl")
public class UserRoleDaoImpl extends AbstractDaoSupportImpl<UserRoleRef> implements UserRoleDao {

}

