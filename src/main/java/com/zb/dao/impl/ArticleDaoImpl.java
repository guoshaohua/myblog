package com.zb.dao.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.zb.bean.Article;
import com.zb.dao.ArticleDao;
import com.zb.orm.AbstractDaoSupportImpl;
import com.zb.qo.ArticleQo;
import com.zb.vo.PageVo;

/**
 * 文章数据访问层处理类
 * 
 * @author zhoubang
 *
 */
@Repository(value = "articleDaoImpl")
public class ArticleDaoImpl extends AbstractDaoSupportImpl<Article> implements ArticleDao {

    private static final String BLOG_ARTICLE = "blog_article";
    
    @Inject
    protected JdbcTemplate jdbcTemplate;
    

    @Override
    public PageVo<Article> getArticleList(ArticleQo articleQo) throws Exception {
        // 构建查询sql
        StringBuffer querySql = new StringBuffer();
        querySql.append("SELECT `user_id` userId,`navigation_id` navigationId,`content`,`update_time` updateTime,`id`,`title`,`create_time` createTime,introduction,is_view_open isViewOpen,view_link viewLink FROM "
                + BLOG_ARTICLE);
        querySql.append(" WHERE `available`=1 ");

        // 构建总记录数sql
        StringBuffer countSql = new StringBuffer();
        countSql.append("SELECT count(id) FROM " + BLOG_ARTICLE + " WHERE `available`=1");

        // 查询条件
        if (articleQo != null) {
            if (null != articleQo.getNavigationId() && !"".equals(articleQo.getNavigationId())) {
                querySql.append(" AND `navigation_id`=:navigationId");
                countSql.append(" AND `navigation_id`=:navigationId");
            }
        }
        // 排序
        querySql.append(" ORDER BY `create_time`  DESC ");

        // limit分页
        querySql.append(" limit " + (articleQo.getPageNo() - 1) * articleQo.getPageSize() + "," + articleQo.getPageSize());

        SqlParameterSource paramSource = new BeanPropertySqlParameterSource(articleQo);
        RowMapper<Article> romMapper = new BeanPropertyRowMapper<Article>(Article.class);

        // 查询
        List<Article> scoreVos = this.namedParameterJdbcTemplate.query(querySql.toString(), paramSource, romMapper);

        // 总记录数
        long totalSize = this.namedParameterJdbcTemplate.queryForObject(countSql.toString(), paramSource, Long.class);

        return new PageVo<Article>(scoreVos, totalSize, articleQo.getPageNo(),articleQo.getPageSize());
    }

    @Override
    public List<Map<String, Object>> getArticleModels() throws Exception {
        String sql = "SELECT na.`id`,na.`name` FROM `blog_navigation` na WHERE na.`id` IN (SELECT n.id FROM `blog_navigation` n WHERE n.linkType = 1)";
        List<Map<String, Object>> objs = jdbcTemplate.queryForList(sql);
        return objs;
    }

    @Override
    public String getParentNameList(String id) throws Exception {
        /**
         * 自定义mysql函数getParentNameList
         */
        String sql = "SELECT getParentNameList('" + id + "')";
        return jdbcTemplate.queryForObject(sql, String.class);
    }
}
