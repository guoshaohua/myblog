package com.zb.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Role;
import com.zb.dao.RoleDao;
import com.zb.orm.AbstractDaoSupportImpl;

/**
 * 角色 Dao处理类
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午2:07:59
 */
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Repository(value = "roleDaoImpl")
public class RoleDaoImpl extends AbstractDaoSupportImpl<Role> implements RoleDao {

    @Override
    public List<Role> getUserRoles(String userName) throws Exception {
        StringBuffer builder = new StringBuffer();

        builder.append("SELECT r.`id`,r.`name`,r.`status`,r.`createTime`,r.`description`,r.`updateTime` FROM `blog_user_role` ur,`blog_user` u,`blog_role` r ");
        builder.append("WHERE u.`id` = ur.`userId` AND r.`id` = ur.`roleId` AND u.`userName` = ?");

        final String sql = builder.toString();
        return this.queryBySQL(sql, userName);
    }

}
