package com.zb.dao.impl;

import java.net.URLDecoder;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Permission;
import com.zb.dao.PermissionDao;
import com.zb.orm.AbstractDaoSupportImpl;
import com.zb.qo.PermissionQo;
import com.zb.vo.PageVo;
import com.zb.vo.PermissionVo;

/**
 * 权限 Dao操作实现类
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午4:55:04
 */
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Repository(value = "permissionDaoImpl")
public class PermissionDaoImpl extends AbstractDaoSupportImpl<Permission> implements PermissionDao {

    @Override
    public List<Permission> getUserPermissions(String userName)
            throws Exception {
        StringBuffer builder = new StringBuffer();

        builder.append("SELECT p.`id`,p.`name`,p.`parentId`,p.`code`,p.`description` ");
        builder.append("FROM `blog_user` u,`blog_role` r,blog_user_role ur,`blog_permission` p,`blog_role_permission` rp ");
        builder.append("WHERE u.`id`=ur.`userId` AND ur.`roleId` = r.`id` AND rp.`roleId` = r.`id` AND rp.`permissionId` = p.`id` AND u.`userName` = ?");

        final String sql = builder.toString();
        return this.queryBySQL(sql, userName);
    }

    @Override
    public PageVo<PermissionVo> getPermissionList(PermissionQo permissionQo) throws Exception {
        //构建查询sql
        StringBuffer querySql = new StringBuffer();
        querySql.append("SELECT p.`id`,p.`name`,p.`code`,p.`description`,p.`parentId`,(SELECT p2.name FROM `blog_permission` p2 WHERE p2.id = p.`parentId` ) parentName FROM `blog_permission` p WHERE 1=1");
        
        //构建总计路数sql
        StringBuffer countSql = new StringBuffer();
        countSql.append("SELECT count(id) FROM blog_permission AS p WHERE 1=1");
        
        //查询条件
        if(permissionQo != null){
            if(StringUtils.isNotBlank(permissionQo.getName())) {
                querySql.append(" AND p.`name` = '" + URLDecoder.decode(permissionQo.getName(),"UTF-8")).append("'");
            }
        }
        
        //limit分页
        querySql.append(" limit "+(permissionQo.getPageNo() - 1) * permissionQo.getPageSize()+"," + permissionQo.getPageSize());
        System.out.println("获取权限列表querySql:" + querySql.toString());
        
        SqlParameterSource paramSource = new BeanPropertySqlParameterSource(permissionQo);
        RowMapper<PermissionVo> romMapper = new BeanPropertyRowMapper<PermissionVo>(PermissionVo.class);
        // 查询
        List<PermissionVo> permissionList = this.namedParameterJdbcTemplate.query(querySql.toString(),paramSource,romMapper);
        // 总记录数
        long totalSize= this.namedParameterJdbcTemplate.queryForObject(countSql.toString(), paramSource, Long.class);
        
        return new PageVo<PermissionVo>(permissionList, totalSize, permissionQo.getPageNo(), permissionQo.getPageSize());
    }

}
