package com.zb.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Navigation;
import com.zb.dao.NavigationDao;
import com.zb.orm.AbstractDaoSupportImpl;

@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Repository(value = "navigationDaoImpl")
public class NavigationDaoImpl extends AbstractDaoSupportImpl<Navigation> implements NavigationDao {

}
