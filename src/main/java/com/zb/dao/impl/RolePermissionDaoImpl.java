package com.zb.dao.impl;


import org.springframework.stereotype.Repository;

import com.zb.bean.RolePermissionRef;
import com.zb.dao.RolePermissionDao;
import com.zb.orm.AbstractDaoSupportImpl;

@Repository(value = "rolePermissionDaoImpl")
public class RolePermissionDaoImpl extends AbstractDaoSupportImpl<RolePermissionRef> implements RolePermissionDao {

}

