package com.zb.dao.impl;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.zb.bean.User;
import com.zb.dao.UserDao;
import com.zb.orm.AbstractDaoSupportImpl;

/**
 * 用户处理
 * 
 * 作者：zhoubang 日期：2015年3月14日 上午1:39:54
 */
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Repository(value = "userDaoImpl")
public class UserDaoImpl extends AbstractDaoSupportImpl<User> implements UserDao {

}
