package com.zb.dao.impl;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Menu;
import com.zb.dao.MenuDao;
import com.zb.orm.AbstractDaoSupportImpl;


@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@Repository(value = "menuDaoImpl")
public class MenuDaoImpl extends AbstractDaoSupportImpl<Menu> implements MenuDao {
    
}

