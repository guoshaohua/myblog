package com.zb.dao;

import com.zb.bean.Navigation;
import com.zb.orm.DaoSupport;

public interface NavigationDao extends DaoSupport<Navigation> {

}
