package com.zb.dao;

import java.util.List;

import com.zb.bean.Permission;
import com.zb.orm.DaoSupport;
import com.zb.qo.PermissionQo;
import com.zb.vo.PageVo;
import com.zb.vo.PermissionVo;

/**
 * 权限 Dao操作接口
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午2:15:26
 */
public interface PermissionDao extends DaoSupport<Permission> {

    /**
     * 获取用户权限列表
     * 
     * 作者: zhoubang 日期：2015年3月26日 下午4:53:40
     * 
     * @param userName
     * @return
     * @throws Exception
     */
    public List<Permission> getUserPermissions(String userName) throws Exception;
    
    /**
     * 获取权限列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 下午1:11:44
     * @param permissionQo
     * @return
     * @throws Exception
     */
    public PageVo<PermissionVo> getPermissionList(PermissionQo permissionQo) throws Exception;
}
