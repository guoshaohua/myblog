package com.zb.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 * 请求地址拦截，登录成功后进入用户之前请求的地址 (目前取消该过滤器在项目中的使用)
 * 
 * 作者: zhoubang 日期：2015年4月2日 上午10:38:19
 */
public class ReqUrlFilter implements Filter {
    private final static Logger LOGGER = Logger.getLogger(ReqUrlFilter.class);

    public ReqUrlFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        // 拦截的请求url地址,如：/zb_blog/index/mailList.shtml
        String reqUrl = req.getRequestURI();
        LOGGER.debug("reqUrl：" + reqUrl);
        // 如：/zb_blog
        String path = req.getContextPath();
        LOGGER.debug("上下文路径:" + path);

        if ("".equals(reqUrl) || null == reqUrl || "/".equals(reqUrl)) {

        } else {
            String url = reqUrl.substring(8);
            if (!"/index".equals(url)) {
                // 如果不是登录url，将请求的url存放在上下文中，登录成功后进行转发
                if (!"/toLogin".equals(url) && !"/login".equals(url)) {
                    ServletContext application = req.getServletContext();
                    application.setAttribute("reqUrl", url);
                }

            }
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

}
