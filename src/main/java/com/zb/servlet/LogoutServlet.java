package com.zb.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import com.zb.util.LoggerUtil;

public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public LogoutServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            LoggerUtil.LoggerFormatPattern("用户 {0} 退出登录", subject.getPrincipal());
            subject.logout();
        }
        
        //移除友言评论的插件cookie
        Cookie cookie = new Cookie("syncuyan",null);
        cookie.setPath("/");
        cookie.setMaxAge(0);
        cookie.setDomain("www.2b2b92b.com");
        response.addCookie(cookie);
        
        response.sendRedirect("/toLogin");
    }

}
