package com.zb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.zb.bean.Menu;
import com.zb.controller.BaseController;
import com.zb.enums.Enums.MenuType;
import com.zb.qo.MenuQo;
import com.zb.service.MenuService;
import com.zb.util.JsonUtils;
import com.zb.vo.AjaxResult;
import com.zb.vo.MenuVo;
import com.zb.vo.PageResult;
import com.zb.vo.PageVo;
import com.zb.vo.ZtreeVo;


@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController {
    
    @Resource(name="menuServiceImpl")
    private MenuService menuService;
    
    /**
     * 获取菜单列表
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 上午10:25:03
     * @param req
     * @param res
     * @param model 标识菜单的类型：
     *                           1：后台系统的菜单    2：会员个人中心的菜单
     * @return
     * @throws IOException
     */
    @RequiresPermissions(value="menu:list")
    @RequestMapping("/getMenuList")
    @ResponseBody
    public PageResult<MenuVo> getMenuList(HttpServletRequest req, HttpServletResponse res,MenuQo menuQo) throws IOException {
        PageVo<Menu> menuList = null;
        try {
            if(menuQo.getType() == 1){//首次进入后台 or 缓存清空后，查询菜单导航数据
                menuQo.setPageSize(100);
            }
            menuList = menuService.getMenuList(menuQo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new PageResult<MenuVo>(menuList.getTotalSize(),MenuVo.buildMenu(menuList.getResult()));
    }
    
    /**
     * 进入菜单列表view
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午2:00:32
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="menu:list")
    @RequestMapping("/toMenuList")
    public ModelAndView toMenuList(HttpServletRequest req, HttpServletResponse res){
        ModelAndView mav = modelAndView("/admin/menu/list");
        mav.addObject("menuType", MenuType.values());
        return mav;
    }
    
    /**
     * 进入编辑菜单view
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 上午10:54:24
     * @param req
     * @param res
     * @param type  标识是系统菜单还是会员菜单
     *              1：后台系统的菜单    2：会员个人中心的菜单
     * @return
     */
    @RequiresPermissions(value="menu:list")
    @RequestMapping("/toMenuManage")
    public ModelAndView toMenuManage(HttpServletRequest req, HttpServletResponse res,int type){
        ModelAndView mav = modelAndView("/admin/menu/menu");
        try {
            List<ZtreeVo> results = new ArrayList<ZtreeVo>();
            ZtreeVo result = new ZtreeVo();
            result.setId("-1");
            result.setpId("0");
            result.setName("无上级菜单");
            results.add(result);
            
            MenuQo menuQo = new MenuQo();
            menuQo.setModel(type);
            menuQo.setPageSize(100);
            PageVo<Menu> sysMenu = menuService.getMenuList(menuQo);
            
            List<Menu> parentMenuIds = new ArrayList<Menu>();
            if (CollectionUtils.isNotEmpty(sysMenu.getResult())) {
                for (Menu menu : sysMenu.getResult()) {
                    ZtreeVo foo = new ZtreeVo();
                    foo.setId(menu.getId());
                    foo.setpId(menu.getPid());
                    foo.setName(menu.getName());
                    results.add(foo);
                    
                    //前端用于添加菜单的时候，校验选择的菜单是否可以添加子菜单,pid不等于-1的不能添加子菜单.
                    if(menu.getPid().equals("-1")){
                        parentMenuIds.add(menu);
                    }
                }
            }
            mav.addObject("parentMenus", JsonUtils.toJson(parentMenuIds));
            mav.addObject("selParent", parentMenuIds);
            mav.addObject("menus", JsonUtils.toJson(results));
            mav.addObject("model", type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * 添加菜单
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午1:17:16
     * @param req
     * @param res
     * @param menu
     * @return
     * @throws IOException
     */
    @RequiresPermissions(value="menu:add")
    @RequestMapping("/addMenu")
    @ResponseBody
    public AjaxResult<List<Menu>> addMenu(HttpServletRequest req, HttpServletResponse res,Menu menu) throws IOException {
        AjaxResult<List<Menu>> result = new AjaxResult<List<Menu>>();
        try {
            menuService.addMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(500);
        }
        return result;
    }
    
    /**
     * 进入编辑菜单view
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午3:33:30
     * @param req
     * @param res
     * @param id
     * @return
     * @throws IOException
     */
    @RequiresPermissions(value="menu:edit")
    @RequestMapping("/editMenuView")
    @ResponseBody
    public ModelAndView editMenuView(HttpServletRequest req, HttpServletResponse res,String id) throws IOException {
        ModelAndView mav = modelAndView("/admin/menu/view");
        try {
            mav.addObject("menu", menuService.getMenuById(id));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * 更新菜单信息
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午3:24:47
     * @param req
     * @param res
     * @param menu
     * @return
     * @throws IOException
     */
    @RequiresPermissions(value="menu:edit")
    @RequestMapping("/editMenu")
    @ResponseBody
    public AjaxResult<String> editMenu(HttpServletRequest req, HttpServletResponse res,Menu menu) throws IOException {
        AjaxResult<String> result = new AjaxResult<String>();
        try {
            menuService.updateMenu(menu);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(500);
        }
        return result;
    }
    
}

