package com.zb.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zb.bean.Navigation;
import com.zb.enums.system.NavigationEnums;
import com.zb.qo.NavigationQo;
import com.zb.service.NavigationService;
import com.zb.vo.AjaxResult;
import com.zb.vo.PageResult;
import com.zb.vo.PageVo;

/**
 * 导航控制器
 * 
 * 作者: zhoubang 日期：2015年4月1日 下午5:29:50
 */
@Controller
@RequestMapping("/navigation")
public class NavigationController extends BaseController{

    @Autowired
    private NavigationService navigationService;

    /**
     * 获取所有导航数据
     * 
     * @param req
     * @param res
     * @throws Exception
     */
    @RequestMapping(value = "getNavigationList", method = RequestMethod.GET)
    @ResponseBody
    public List<Object> getNavigationList(HttpServletRequest req,
            HttpServletResponse res) throws Exception {
        List<Object> navigationList = navigationService.getNavigationList();
        return navigationList;
    }

    /**
     * 获取所有子模块分类
     * 
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "getModuleSub", method = RequestMethod.GET)
    @ResponseBody
    public List<Navigation> getModuleSub(HttpServletRequest req,
            HttpServletResponse res) throws Exception {
        List<Navigation> navigationList = navigationService.getModuleSub();
        return navigationList;
    }

    
    /**
     * 后台 进入导航列表view
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午9:18:18
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="navigation:list")
    @RequestMapping(value = "toNavigationList", method = RequestMethod.GET)
    public ModelAndView toNavigationList(HttpServletRequest req, HttpServletResponse res) throws Exception {
        ModelAndView mav = modelAndView("/admin/navigation/list");
        return mav;
    }
    
    /**
     * 后台 获取导航数据
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午9:44:52
     * @param req
     * @param res
     * @param navigationQo
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="navigation:list")
    @RequestMapping(value = "navigationList", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<Navigation> navigationList(HttpServletRequest req, HttpServletResponse res,NavigationQo navigationQo) throws Exception {
        PageVo<Navigation> result = navigationService.getNavigationListFromSystem(navigationQo);
        return new PageResult<Navigation>(result.getTotalSize(), result.getResult());
    }
    
    /**
     * 进入更新导航view
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午9:56:59
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="navigation:edit")
    @RequestMapping(value = "editNavigationView", method = RequestMethod.GET)
    public ModelAndView editNavigationView(HttpServletRequest req, HttpServletResponse res,@RequestParam(required = true) String id) throws Exception {
        ModelAndView mav = modelAndView("/admin/navigation/view");
        mav.addObject("navigationInfo", navigationService.getNavigationById(id));
        mav.addObject("availableStatus", NavigationEnums.NavigationAvailable.values());
        mav.addObject("linkTypes", NavigationEnums.NavigationLinkType.values());
        mav.addObject("parentList", navigationService.getModuleParent());
        return mav;
    }
    
    /**
     * 添加导航
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 下午1:10:09
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="navigation:add")
    @RequestMapping(value = "addNavigationView", method = RequestMethod.GET)
    public ModelAndView addNavigationView(HttpServletRequest req, HttpServletResponse res) throws Exception {
        ModelAndView mav = modelAndView("/admin/navigation/add");
        mav.addObject("linkTypes", NavigationEnums.NavigationLinkType.values());
        mav.addObject("parentList", navigationService.getModuleParent());
        return mav;
    }
    
    /**
     * 新增导航
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 下午1:13:07
     * @param req
     * @param res
     * @param navigation
     * @return
     */
    @RequiresPermissions(value="navigation:add")
    @RequestMapping(value = "addNavigation", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> addNavigation(HttpServletRequest req, HttpServletResponse res,Navigation navigation){
        AjaxResult<String> result = new AjaxResult<String>();
        try {
            navigationService.addNavigation(navigation);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(500);
        }
        return result;
    }
    
    
    
    /**
     * 更新导航数据
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午10:08:10
     * @param req
     * @param res
     * @param navigation
     * @return
     */
    @RequiresPermissions(value="navigation:edit")
    @RequestMapping(value = "editNavigation", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> editNavigation(HttpServletRequest req, HttpServletResponse res,Navigation navigation){
        AjaxResult<String> result = new AjaxResult<String>();
        try {
            navigationService.updateNavigation(navigation);
        } catch (Exception e) {
            e.printStackTrace();
            result.setCode(500);
        }
        return result;
    }
    
}
