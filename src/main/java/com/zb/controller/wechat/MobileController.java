package com.zb.controller.wechat;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zb.controller.BaseController;
/**
 * 手机端控制器
 * 
 * 作者: zhoubang 
 * 日期：2015年5月8日 下午3:21:29
 */
@Controller
@RequestMapping(value="/mobile")
public class MobileController extends BaseController {
    
    /**
     * 进入手机端首页
     * 
     * 作者: zhoubang 
     * 日期：2015年5月8日 下午3:20:57
     * @return
     */
    @RequestMapping(value="index")
    public ModelAndView mobileIndexView(){
        return modelAndView("/mobile/index");
    }
    
    /**
     * 进入注册页面
     * @return
     */
    @RequestMapping(value="register")
    public ModelAndView register(){
        return modelAndView("/mobile/register");
    }
    
    /**
     * 进入登录页面
     * @return
     */
    @RequestMapping(value="toLogin")
    public ModelAndView toLogin(){
        return modelAndView("/mobile/login");
    }
}

