package com.zb.controller.wechat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.zb.bean.Navigation;
import com.zb.controller.BaseController;
import com.zb.vo.PageResult;

/**
 * 微信消息推送
 * 
 * 作者: zhoubang 
 * 日期：2015年5月6日 下午3:43:24
 */
@Controller
@RequestMapping(value="/push")
public class PushController extends BaseController {
    
    /**
     * 进入推送列表视图
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 下午3:49:13
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="wechat:push:list")
    @RequestMapping(value="pushListView")
    public ModelAndView pushListView(HttpServletRequest req, HttpServletResponse res){
        ModelAndView mav = modelAndView("/admin/wechat/pushList");
        return mav;
    }
    
    /**
     * 推送 列表
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 下午3:49:02
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="wechat:push:list")
    @RequestMapping(value = "pushList", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<Navigation> pushList(HttpServletRequest req, HttpServletResponse res) throws Exception {
        return null;
    }
}

