package com.zb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 视图转发控制器
 * 
 * 作者: zhoubang 日期：2015年3月13日 下午6:05:04
 */
@Controller
public class ViewController extends BaseController {
    /**
     * 登录
     * 
     * 作者：zhoubang 日期：2015年3月17日 下午10:48:46
     * 
     * @param req
     * @param res
     * @return
     */
    @RequestMapping(value = "/toLogin", method = RequestMethod.GET)
    public ModelAndView toLoginPage(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/login");
    }

    
    /**
     * 进入注册页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月22日 下午5:04:13
     * @param req
     * @param res
     * @return
     */
    @RequestMapping(value = "/toRegister", method = RequestMethod.GET)
    public ModelAndView toRegisterPage(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/register");
    }
    
    /**
     * 进入后台系统管理
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 下午2:14:30
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions("system:mgt")
    @RequestMapping(value = "/system", method = RequestMethod.GET)
    public ModelAndView toSystemPage(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/admin/system");
    }
    
    /**
     * 进入会员个人中心
     * 
     * 作者: zhoubang 
     * 日期：2015年4月30日 下午2:01:05
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions("center:mgt")
    @RequestMapping(value = "/center", method = RequestMethod.GET)
    public ModelAndView toCenterPage(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/user/center");
    }
    
    /**
     * 功能暂未开发完成-提示
     * 
     * 作者: zhoubang 
     * 日期：2015年5月21日 上午10:01:40
     * @param req
     * @param res
     * @return
     */
    @RequestMapping(value = "/notice", method = RequestMethod.GET)
    public ModelAndView toNoticePage(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/notice");
    }
    
    /**
     * 网站收藏
     * 
     * 作者: zhoubang 
     * 日期：2015年9月22日 下午4:36:57
     * @param req
     * @param res
     * @return
     */
    @RequestMapping(value = "/favorite", method = RequestMethod.GET)
    public ModelAndView favorite(HttpServletRequest req,
            HttpServletResponse res) {
        return this.modelAndView("/onlinetools/favorite");
    }
}
