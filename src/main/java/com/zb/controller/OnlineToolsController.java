package com.zb.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 在线工具控制器
 * 
 * 作者: zhoubang 
 * 日期：2015年9月17日 下午4:21:51
 */
@Controller
@RequestMapping(value="/online")
public class OnlineToolsController extends BaseController {
    
    /**
     * 进入在线工具视图页面
     * 
     * 作者: zhoubang 
     * 日期：2015年9月21日 下午2:54:02
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(HttpServletRequest req,
            HttpServletResponse res) throws Exception {
        ModelAndView mav = new ModelAndView("/onlinetools/online_home");
        mav.addObject("address",getParentNames("55fa57caea01f20c425bb66c"));
        return mav;
    }
    
    /**
     * 进入图标iconGlyphs视图页面
     * 
     * 作者: zhoubang 
     * 日期：2015年9月21日 下午2:54:13
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/iconGlyphs", method = RequestMethod.GET)
    public ModelAndView iconGlyphs(HttpServletRequest req,
            HttpServletResponse res) throws Exception {
        ModelAndView mav = new ModelAndView("/onlinetools/iconGlyphs");
        return mav;
    }
    
}
