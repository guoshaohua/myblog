package com.zb.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zb.bean.Article;
import com.zb.qo.ArticleQo;
import com.zb.service.ArticleService;
import com.zb.vo.PageVo;

@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {
    
    @Autowired
    private ArticleService articleService;

    /**
     * 首页
     * 
     * 作者：zhoubang 日期：2015年3月17日 下午10:48:40
     * 
     * @param req
     * @param res
     * @return
     * @throws Exception 
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView toIndexPage(HttpServletRequest req,
            HttpServletResponse res, ArticleQo articleQo) throws Exception {
        
        ModelAndView mav = modelAndView("/index");
        PageVo<Article> articleList = null;
        try {
            articleList = articleService.getArticleList(articleQo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mav.addObject(articleList);
        mav.addObject("address",getParentNames(""));
        return mav;
    }

}
