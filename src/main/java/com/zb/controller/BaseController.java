package com.zb.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import com.zb.qo.BaseQo;
import com.zb.service.ArticleService;
import com.zb.util.ApplicationContextUtil;
import com.zb.util.JsonUtils;

/**
 * 通用ModelAndView操作类
 * 
 * 作者: zhoubang 日期：2015年3月11日 下午1:09:55
 */
public class BaseController{

    /**
     * 空的ModelAndView对象
     * 
     * 作者: zhoubang 日期：2015年4月1日 下午5:14:05
     * 
     * @return
     */
    protected ModelAndView emptyModelAndView() {
        return new ModelAndView();
    }

    /**
     * 转发至view，无数据传递
     * 
     * 作者: zhoubang 日期：2015年3月11日 下午1:10:48
     * 
     * @param sourceName
     * @return
     */
    protected ModelAndView modelAndView(String sourceName) {
        return new ModelAndView(sourceName);
    }

    /**
     * 转发到相应view，带有数据传递，String类型
     * 
     * 作者: zhoubang 日期：2015年3月11日 下午1:11:40
     * 
     * @param sourceName
     * @param data
     * @return
     */
    protected ModelAndView modelAndView(String sourceName,
            Map<String, Object> dataMap) {
        ModelAndView modelAndView = modelAndView(sourceName);
        for (Entry<String, Object> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            modelAndView.addObject(key, value);
        }
        return modelAndView;
    }

    /**
     * 转发到相应view，带有一个数据传递
     * 
     * 作者: zhoubang 日期：2015年4月3日 下午5:12:04
     * 
     * @param sourceName
     * @param key
     * @param value
     * @return
     */
    protected ModelAndView modelAndView(String sourceName, String key, String value) {
        ModelAndView modelAndView = modelAndView(sourceName);
        if (value != null && !"".equals(value.trim())) {
            modelAndView.addObject(key, value);
        }
        return modelAndView;
    }

    /**
     * 重定向到指定view 或 请求地址
     * 
     * 作者: zhoubang 日期：2015年4月2日 上午10:45:42
     * 
     * @param path
     * @return
     */
    protected String redirect(String path) {
        return "redirect:" + path;
    }

    /**
     * 重定向
     * 
     * 作者: zhoubang 日期：2015年4月3日 下午5:15:05
     * 
     * @param path
     * @return
     */
    protected ModelAndView redirectModelAndView(String path) {
        return modelAndView("redirect:" + path);
    }

    /**
     * 转发到指定请求地址
     * 
     * 作者: zhoubang 日期：2015年4月2日 上午10:49:59
     * 
     * @param path
     * @return
     */
    protected ModelAndView forwardView(String path) {
        return modelAndView("forward:" + path);
    }

    /**
     * 将数据转发到指定请求地址对应的view中
     * 
     * 作者: zhoubang 日期：2015年4月2日 上午11:00:19
     * 
     * @param path
     * @param dataMap
     * @return
     */
    protected ModelAndView forwardView(String path, Map<String, Object> dataMap) {
        return modelAndView("forward:" + path, dataMap);
    }

    /**
     * 将结果写到前端
     * 
     * 作者: zhoubang 日期：2015年4月2日 上午10:43:24
     * 
     * @param response
     * @param data
     * @throws IOException
     */
    protected void write(HttpServletResponse response, String data)
            throws IOException {
        response.setCharacterEncoding("utf-8");
        response.getWriter().print(data);
    }

    /**
     * 处理返回ajax数据 或者 返回数据到modelview
     * 
     * 作者: zhoubang 日期：2015年4月16日 上午10:28:06
     * 
     * @param response
     * @param obj
     *            结果对象
     * @return
     * @throws IOException
     */
    protected Object returnData(HttpServletResponse response, Object obj,
            String viewPath, BaseQo baseQo) throws IOException {
        if (baseQo.getAjaxRequest()) {
            write(response, JsonUtils.toJson(obj));
        } else {
            ModelAndView mav = modelAndView(viewPath);
            mav.addObject(obj);
            mav.addObject(baseQo);
            return mav;
        }
        return null;
    }

    /**
     * 获取菜单的父级名称集合
     * 
     * 作者: zhoubang 日期：2015年4月16日 下午5:56:13
     * 
     * @param navigationId
     * @return
     * @throws Exception
     */
    public List<String> getParentNames(String navigationId) throws Exception {
        ArticleService articleService = (ArticleService) ApplicationContextUtil.getInstance().getBean("articleServiceImpl");
        List<String> ls = null;
        if (navigationId != null && !"".equals(navigationId.trim())) {
            String result = articleService.getParentNameList(navigationId);
            if (result.split("-").length > 0) {
                ls = new ArrayList<String>();
                for (int i = 0; i < result.split("-").length; i++) {
                    ls.add(result.split("-")[i]);
                }
            }
        }
        return ls;
    }
}