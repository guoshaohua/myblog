package com.zb.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wxinf.send.pojo.token.Oauth2Token;
import com.wxinf.send.press.TokenUtil;

/**
 * 微信控制器
 * 
 * 作者: zhoubang 
 * 日期：2015年6月29日 下午5:29:27
 */
@Controller
@RequestMapping("/weixin")
public class WeixinController extends BaseController{
    
    /**
     * 微信网页授权回调
     * 
     * 作者: zhoubang 
     * 日期：2015年6月30日 上午10:19:22
     * @param req
     * @param res
     * @throws Exception 
     * @throws EcarpoolException
     */
    @RequestMapping(value = "/authRedirect",method = RequestMethod.GET)
    @ResponseBody
    public String wxBind(HttpServletRequest req,HttpServletResponse res) throws Exception{
        System.out.println("授权返回的数据:" + req.getQueryString());
        String code = req.getParameter("code");
        System.out.println("code:" + code);
        //根据code获取网页授权的access_token
        Oauth2Token token = TokenUtil.getOauth2AccessToken(code);
        //获取openId
        String openId = token.getOpenId();
        return openId + "@" + code + "@" + req.getQueryString();
    }
}

