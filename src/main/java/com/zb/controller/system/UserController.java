package com.zb.controller.system;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zb.bean.User;
import com.zb.bean.UserRoleRef;
import com.zb.controller.BaseController;
import com.zb.enums.Enums;
import com.zb.qo.UserQo;
import com.zb.service.RoleService;
import com.zb.service.UserRoleService;
import com.zb.service.UserService;
import com.zb.util.JsonUtils;
import com.zb.vo.AjaxResult;
import com.zb.vo.PageResult;
import com.zb.vo.PageVo;
import com.zb.vo.UserVo;
import com.zb.vo.ZtreeVo;

@Controller
@RequestMapping(value="/user")
public class UserController extends BaseController{

    @Resource(name="userServiceImpl")
    private UserService userService;
    
    @Resource(name="roleServiceImpl")
    private RoleService roleService;
    
    @Resource(name="userRoleServiceImpl")
    private UserRoleService userRoleService;
    /**
     * 进入用户列表页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 下午2:15:30
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="user:list")
    @RequestMapping(value = "/toUserList", method = RequestMethod.GET)
    public ModelAndView toUserListPage(HttpServletRequest req,
            HttpServletResponse res) {
        ModelAndView mav = modelAndView("/admin/user/userList");
        mav.addObject("status",Enums.UserStatus.values());
        return mav;
    }
    
    /**
     * 获取用户列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:49:21
     * @param req
     * @param res
     * @param userQo
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="user:list")
    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<UserVo> getUserList(HttpServletRequest req,HttpServletResponse res,
            UserQo userQo) throws Exception {
        // 查询列表
        PageVo<User> userList = userService.getUserList(userQo);
        // 返回结果
        return new PageResult<UserVo>(userList.getTotalSize(),UserVo.buildList(userList.getResult()));
    }
    
    /**
     * 进入编辑会员页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:43:24
     * @param req
     * @param res
     * @param user
     * @return
     */
    @RequiresPermissions(value="user:edit")
    @RequestMapping(value = "/editUserView", method = RequestMethod.GET)
    public ModelAndView editUserView(HttpServletRequest req, HttpServletResponse res,User user) {
        ModelAndView mav = modelAndView("/admin/user/editUser");
        try {
            //会员信息
            User userInfo = userService.getUserById(user.getId());
            
            //获取所有角色列表
            List<ZtreeVo> allRole = roleService.queryAllFormatWithZtree(true);
            //获取当前编辑的会员对应的角色列表
            List<UserRoleRef> userRoles = userRoleService.getRolesByUserId(user.getId());
            
            //角色对应的权限.json字符串包含权限的复选框选中属性设置以及展开属性设置.
            mav.addObject("roles", JsonUtils.toJson(setPZtreeCheck(allRole, userRoles)));
            mav.addObject("userInfo", userInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * 对用户的角色tree进行操作
     *      对用户拥有的角色进行复选框选中的属性设置,并且展开对应的权限tree。
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午9:52:16
     * @param list
     * @param rplist
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list ,List<UserRoleRef> urlist){
        for (ZtreeVo ztreeVo : list) {
            for (UserRoleRef urr : urlist) {
                if(StringUtils.equals(urr.getRoleId(), ztreeVo.getId())){
                    ztreeVo.setChecked(true);
                }
            }
            ztreeVo.setOpen(true);
        }
        return list;
    }
    
    /**
     * 更新会员信息
     *      为会员赋予角色
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:42:30
     * @param req
     * @param res
     * @param userQo
     * @return
     */
    @RequiresPermissions(value="user:edit")
    @RequestMapping(value = "/updateUser", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> updateUser(HttpServletRequest req, HttpServletResponse res,UserQo userQo) {
        AjaxResult<String> ajaxResult = new AjaxResult<String>();
        try {
            //更新会员信息
            userService.updateUser(UserQo.buildUser(userQo));
            
            //删除该会员全部角色
            userRoleService.delRolesByUserid(userQo.getId());
            //重新赋予角色
            String[] roleIds = userQo.getRoleIds().split(",");
            for (int i = 0; i < roleIds.length; i++) {
                UserRoleRef userRole = new UserRoleRef();
                userRole.setRoleId(roleIds[i]);
                userRole.setUserId(userQo.getId());
                userRoleService.addUserRole(userRole);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setCode(500);
        }
        return ajaxResult;
    }
    
    /**
     * 更新用户状态
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 下午1:43:50
     * @param req
     * @param res
     * @param userQo
     * @return
     */
    @RequiresPermissions(value="user:edit")
    @RequestMapping(value = "/updateUserStatus", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> updateUserStatus(HttpServletRequest req, HttpServletResponse res,UserQo userQo) {
        AjaxResult<String> ajaxResult = new AjaxResult<String>();
        try {
            userService.updateUserStatus(userQo.getId(), userQo.getStatus());
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setCode(500);
        }
        return ajaxResult;
    }
    
}

