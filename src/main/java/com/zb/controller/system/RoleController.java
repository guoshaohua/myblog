package com.zb.controller.system;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zb.bean.Role;
import com.zb.bean.RolePermissionRef;
import com.zb.controller.BaseController;
import com.zb.enums.Enums;
import com.zb.qo.RoleQo;
import com.zb.service.PermissionService;
import com.zb.service.RolePermissionService;
import com.zb.service.RoleService;
import com.zb.util.JsonUtils;
import com.zb.vo.AjaxResult;
import com.zb.vo.PageResult;
import com.zb.vo.PageVo;
import com.zb.vo.RoleVo;
import com.zb.vo.ZtreeVo;

@Controller
@RequestMapping("/role")
public class RoleController extends BaseController{

    @Resource(name="roleServiceImpl")
    private RoleService roleService;
    
    @Resource(name="permissionServiceImpl")
    private PermissionService permissionService;
    
    @Resource(name="rolePermissionServiceImpl")
    private RolePermissionService rolePermissionService;
    /**
     * 进入角色列表页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午10:52:51
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="role:list")
    @RequestMapping(value = "/toRoleList", method = RequestMethod.GET)
    public ModelAndView toRoleListPage(HttpServletRequest req,
            HttpServletResponse res) {
        ModelAndView mav = modelAndView("/admin/role/roleList");
        mav.addObject("status",Enums.RoleStatus.values());
        return mav;
    }
    
    /**
     * 获取角色列表数据
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午10:53:02
     * @param req
     * @param res
     * @param roleQo
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="role:list")
    @RequestMapping(value = "/roleList", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<RoleVo> getRoleList(HttpServletRequest req,HttpServletResponse res,
            RoleQo roleQo) throws Exception {
        // 查询列表
        PageVo<Role> roleList = roleService.getRoleList(roleQo);
        // 返回结果
        return new PageResult<RoleVo>(roleList.getTotalSize(),RoleVo.buildList(roleList.getResult()));
    }
    
    /**
     * 进入角色编辑页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午4:57:23
     * @param req
     * @param res
     * @param role
     * @return
     */
    @RequiresPermissions(value="role:edit")
    @RequestMapping(value = "/editRoleView", method = RequestMethod.GET)
    public ModelAndView editRoleView(HttpServletRequest req, HttpServletResponse res,Role role) {
        ModelAndView mav = modelAndView("/admin/role/editRole");
        try {
            //角色信息
            Role roleInfo = roleService.getRoleById(role);
            mav.addObject("roleInfo", roleInfo);
            
            //获取所有权限列表
            List<ZtreeVo> allPermission = permissionService.queryAllFormatWithZtree(true);
            //获取当前编辑的角色对应的权限列表
            List<RolePermissionRef> rolePermissions = rolePermissionService.getPermissionByRoleId(role.getId());
            
            //角色对应的权限.json字符串包含权限的复选框选中属性设置以及展开属性设置.
            mav.addObject("permissions", JsonUtils.toJson(setPZtreeCheck(allPermission, rolePermissions)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * 对角色的权限tree进行操作
     *      对角色拥有的权限进行复选框选中的属性设置,并且展开对应的权限tree。
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午9:52:16
     * @param list
     * @param rplist
     * @return
     */
    public static List<ZtreeVo> setPZtreeCheck(List<ZtreeVo> list ,List<RolePermissionRef> rplist){
        for (ZtreeVo ztreeVo : list) {
            for (RolePermissionRef rpr : rplist) {
                if(StringUtils.equals(rpr.getPermissionId(), ztreeVo.getId())){
                    ztreeVo.setChecked(true);
                }
            }
            ztreeVo.setOpen(true);
        }
        return list;
    }
    
    
    /**
     * 更新角色信息
     *      为角色赋予权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午5:45:52
     * @param req
     * @param res
     * @param roleQo
     * @return
     */
    @RequiresPermissions(value="role:edit")
    @RequestMapping(value = "/updateRole", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> updateRole(HttpServletRequest req, HttpServletResponse res,RoleQo roleQo) {
        AjaxResult<String> ajaxResult = new AjaxResult<String>();
        try {
            roleService.updateRole(RoleQo.buildUpdateRole(roleQo));
            
            //更新角色权限
            //删除该角色全部权限
            rolePermissionService.delPermissionByRole(roleQo.getId());
            //重新赋予权限
            String[] permissionIds = roleQo.getPermissionIds().split(",");
            for (int i = 0; i < permissionIds.length; i++) {
                RolePermissionRef rolePermission = new RolePermissionRef();
                rolePermission.setRoleId(roleQo.getId());
                rolePermission.setPermissionId(permissionIds[i]);
                rolePermissionService.addPermission(rolePermission);
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setCode(500);
        }
        return ajaxResult;
    }
    
}
