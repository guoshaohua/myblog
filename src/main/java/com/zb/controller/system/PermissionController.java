package com.zb.controller.system;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zb.bean.Permission;
import com.zb.bean.RolePermissionRef;
import com.zb.controller.BaseController;
import com.zb.qo.PermissionQo;
import com.zb.service.PermissionService;
import com.zb.service.RolePermissionService;
import com.zb.util.JsonUtils;
import com.zb.vo.AjaxResult;
import com.zb.vo.PageResult;
import com.zb.vo.PageVo;
import com.zb.vo.PermissionVo;

@Controller
@RequestMapping(value="/permission")
public class PermissionController extends BaseController{

    @Resource(name="permissionServiceImpl")
    private PermissionService permissionService;
    
    @Resource(name="rolePermissionServiceImpl")
    private RolePermissionService rolePermissionService;
    
    /**
     * 进入权限列表页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午10:54:01
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="permission:list")
    @RequestMapping(value = "/toPermissionList", method = RequestMethod.GET)
    public ModelAndView toPermissionListPage(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mav = modelAndView("/admin/permission/permissionList");
        return mav;
    }
    
    /**
     * 获取权限列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午11:07:44
     * @param req
     * @param res
     * @param permissionQo
     * @return
     * @throws Exception
     */
    @RequiresPermissions(value="permission:list")
    @RequestMapping(value = "/permissionList", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<PermissionVo> getRoleList(HttpServletRequest req,HttpServletResponse res, PermissionQo permissionQo) throws Exception {
        // 查询列表
        PageVo<PermissionVo> permissionList = permissionService.getPermissionList(permissionQo);
        // 返回结果
        return new PageResult<PermissionVo>(permissionList.getTotalSize(),PermissionVo.buildList(permissionList.getResult()));
    }
    
    /**
     * 进入新增权限页面
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午1:14:50
     * @param req
     * @param res
     * @return
     */
    @RequiresPermissions(value="permission:add")
    @RequestMapping(value = "/addPermissionView", method = RequestMethod.GET)
    public ModelAndView addPermissionView(HttpServletRequest req, HttpServletResponse res) {
        ModelAndView mav = modelAndView("/admin/permission/addPermission");
        try {
            mav.addObject("permissions", JsonUtils.toJson(permissionService.queryAllFormatWithZtree(false)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * 检查code是否已经存在
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午4:09:28
     * @param req
     * @param res
     * @param permission
     * @return
     * @throws Exception 
     */
    @RequiresPermissions(value="permission:list")
    @RequestMapping(value = "/checkCodeIsExists", method = RequestMethod.POST)
    @ResponseBody
    public boolean checkCodeIsExists(HttpServletRequest req, HttpServletResponse res,Permission permission) throws Exception {
        return permissionService.checkCodeIsExists(permission);
    }
    
    
    /**
     * 新增权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午2:32:52
     * @param req
     * @param res
     * @param permissionQo
     * @return
     */
    @RequiresPermissions(value="permission:add")
    @RequestMapping(value = "/addPermission", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> addPermission(HttpServletRequest req, HttpServletResponse res,Permission permission) {
        AjaxResult<String> ajaxResult = new AjaxResult<String>();
        //检查code是否重复
        try {
            if(permissionService.checkCodeIsExists(permission)){
                ajaxResult.setCode(201);
                return ajaxResult;
            }
            permissionService.addPermission(permission);
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setCode(500);
        }
        return ajaxResult;
    }
    
    /**
     * 删除权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午3:05:12
     * @param req
     * @param res
     * @param permission
     * @return
     */
    @RequiresPermissions(value="permission:edit")
    @RequestMapping(value = "/delPermission", method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult<String> delPermission(HttpServletRequest req, HttpServletResponse res,Permission permission) {
        AjaxResult<String> ajaxResult = new AjaxResult<String>();
        try {
            //检查该权限节点下是否有子节点
            List<Permission> childrenList = permissionService.getPermissionByPid(permission.getId());
            if(childrenList != null && childrenList.size() > 0){
                ajaxResult.setCode(201);
                return ajaxResult;
            }
            //检查该权限是否已分配角色
            List<RolePermissionRef> roleList = rolePermissionService.getRoleIdsByPermission(permission.getId());
            if(roleList != null && roleList.size() > 0){
                ajaxResult.setCode(202);
                return ajaxResult;
            }
            permissionService.delPermission(permission);
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.setCode(500);
        }
        return ajaxResult;
    }
}

