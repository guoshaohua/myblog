package com.zb.vo;

public class EditorImgVo {

    private String mtime;
    private String url;
    public String getMtime() {
        return mtime;
    }
    public void setMtime(String mtime) {
        this.mtime = mtime;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
