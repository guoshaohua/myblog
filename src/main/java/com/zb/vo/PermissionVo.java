package com.zb.vo;

import java.util.ArrayList;
import java.util.List;
import com.zb.bean.Permission;

public class PermissionVo {

    private String id;
    private String name;// 名称
    private String code;// 权限代码
    private String description;// 描述
    private String parentId;//
    private String parentName;//权限父级名称
    
    
    public String getParentName() {
        return parentName;
    }
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    
    public PermissionVo() {
    }
    public PermissionVo(Permission permission) {
        this.id = permission.getId();
        this.code = permission.getCode();
        this.description = permission.getDescription();
        this.name = permission.getName();
        this.parentId = permission.getParentId();
        //this.parentName = permission
    }
    
    public static List<PermissionVo> buildList(List<PermissionVo> result) {
        List<PermissionVo> permissionVos = new ArrayList<PermissionVo>();
        for(PermissionVo permission : result) {
            permissionVos.add(permission);
        }
        return permissionVos;
    }
    
    
}

