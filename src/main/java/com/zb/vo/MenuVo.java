package com.zb.vo;

import java.util.ArrayList;
import java.util.List;

import com.zb.bean.Menu;

public class MenuVo {
    
    private String id;
    private String name;
    private String pid;
    private String link;
    private int available;
    private int model;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPid() {
        return pid;
    }
    public void setPid(String pid) {
        this.pid = pid;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public int getAvailable() {
        return available;
    }
    public void setAvailable(int available) {
        this.available = available;
    }
    public int getModel() {
        return model;
    }
    public void setModel(int model) {
        this.model = model;
    }
    
    public MenuVo() {
    }
    
    public MenuVo(Menu menu){
        this.id = menu.getId();
        this.available = menu.getAvailable();
        this.link = menu.getLink();
        this.model = menu.getModel();
        this.name = menu.getName();
        this.pid = menu.getPid();
    }
    public static List<MenuVo> buildMenu(List<Menu> menuList){
        List<MenuVo> menuVos = new ArrayList<MenuVo>();
        for(Menu menu : menuList) {
            menuVos.add(new MenuVo(menu));
        }
        return menuVos;
    }
}

