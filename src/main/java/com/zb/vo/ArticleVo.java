package com.zb.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zb.bean.Article;

public class ArticleVo {

    private String title;
    private Date createTime;
    private Date updateTime;
    private String navigationId;
    private String userId;
    private String id;
    
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getNavigationId() {
        return navigationId;
    }
    public void setNavigationId(String navigationId) {
        this.navigationId = navigationId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public ArticleVo(Article article){
        this.title = article.getTitle();
        this.createTime = article.getCreateTime();
        this.updateTime = article.getUpdateTime();
        this.userId = article.getUserId();
        this.navigationId = article.getNavigationId();
        this.id = article.getId();
    }
    
    public static List<ArticleVo> buildArticleList(List<Article> articleList){
        List<ArticleVo> ls = new ArrayList<ArticleVo>();
        for (Article article : articleList) {
            ls.add(new ArticleVo(article));
        }
        return ls;
    }
}

