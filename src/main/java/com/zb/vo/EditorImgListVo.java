package com.zb.vo;

import java.util.List;

public class EditorImgListVo {

    private String state;
    private int start;
    private int total;
    private List<EditorImgVo> list;
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public int getStart() {
        return start;
    }
    public void setStart(int start) {
        this.start = start;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    public List<EditorImgVo> getList() {
        return list;
    }
    public void setList(List<EditorImgVo> list) {
        this.list = list;
    }
    
}
