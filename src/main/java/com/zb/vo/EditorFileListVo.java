package com.zb.vo;

import java.util.List;

public class EditorFileListVo {

    private String state;
    private int start;
    private int total;
    private List<EditorFileVo> list;
    
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public int getStart() {
        return start;
    }
    public void setStart(int start) {
        this.start = start;
    }
    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    public List<EditorFileVo> getList() {
        return list;
    }
    public void setList(List<EditorFileVo> list) {
        this.list = list;
    }
    
}
