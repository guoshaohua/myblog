package com.zb.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zb.bean.User;
import com.zb.enums.Enums;
import com.zb.util.DateUtil;

/**
 * 用户信息
 *      数据绑定辅助类
 * 
 * 作者: zhoubang 
 * 日期：2015年4月24日 下午2:34:39
 */
public class UserVo{

    private String id;
    private String userName;
    private String email;
    private String realName;
    private String createTime;
    private String updateTime;
    private String phone;
    private int status;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    /**
     * 用户状态
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 下午3:17:52
     * @return
     */
    public String getStatusName() {
        return Enums.UserStatus.getUserStatusName(this.status);
    }

    /**
     * 转换成map
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:19:56
     * @return
     */
    public static Map<String, Object> buildMap(UserVo userQo){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userName", userQo.userName);
        map.put("phone", userQo.phone);
        map.put("status", userQo.status);
        return map;
    }
    
    public UserVo() {
    }
    
    
    public UserVo(User user){
        this.createTime = DateUtil.getDateTimeFormat(new Date(user.getCreateTime().getTime()));
        this.email = user.getEmail();
        this.id = user.getId();
        this.phone = user.getPhone();
        this.realName = user.getRealName();
        this.status = user.getStatus();
        this.updateTime = DateUtil.getDateTimeFormat(new Date(user.getUpdateTime().getTime()));
        this.userName = user.getUserName();
    }
    
    
    public static List<UserVo> buildList(List<User> users){
        List<UserVo> userVos = new ArrayList<UserVo>();
        for(User user : users) {
            userVos.add(new UserVo(user));
        }
        return userVos;
    }
}

