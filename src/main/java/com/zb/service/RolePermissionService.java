package com.zb.service;

import java.util.List;
import com.zb.bean.RolePermissionRef;

public interface RolePermissionService {
    /**
     * 检查该权限是否已经分配角色
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午3:20:30
     * @return
     * @throws Exception
     */
    public List<RolePermissionRef> getRoleIdsByPermission(String id) throws Exception;
    
    /**
     * 删除角色所有权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午5:55:16
     * @param role
     * @throws Exception
     */
    public void delPermissionByRole(String roleId) throws Exception;
    
    /**
     * 添加权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午5:57:24
     * @param permission
     * @throws Exception
     */
    public void addPermission(RolePermissionRef rolePermission) throws Exception;
    
    /**
     * 获取角色对应的所有权限信息
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午9:49:05
     * @return
     * @throws Exception
     */
    public List<RolePermissionRef> getPermissionByRoleId(String roleId) throws Exception;
}

