package com.zb.service;

import com.zb.bean.User;
import com.zb.qo.UserQo;
import com.zb.vo.PageVo;

/**
 * 用户处理接口
 * 
 * 作者：zhoubang 日期：2015年3月14日 上午1:36:39
 */
public interface UserService {

    /**
     * 根据名称获取该用户信息
     * 
     * 作者：zhoubang 日期：2015年3月14日 上午1:36:54
     * 
     * @param name
     * @return
     */
    public User getUserByName(String userName) throws Exception;
    
    /**
     * 会员注册
     * 
     * 作者: zhoubang 
     * 日期：2015年4月22日 下午5:19:28
     * @param user
     * @throws Exception
     */
    public void register(User user) throws Exception;
    
    /**
     * 查询会员列表
     *      条件查询
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:13:05
     * @param user
     * @return
     */
    public PageVo<User> getUserList(UserQo userQo) throws Exception;
    
    /**
     * 获取会员信息
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:09:17
     * @param id
     * @return
     * @throws Exception
     */
    public User getUserById(String id) throws Exception;
    
    /**
     * 更新会员信息
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:31:02
     * @param user
     * @throws Exception
     */
    public void updateUser(User user) throws Exception;
    
    /**
     * 更新用户状态
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 下午1:45:19
     * @param id
     * @param status
     * @throws Exception
     */
    public void updateUserStatus(String id,Integer status) throws Exception;
    
    /**
     * 根据openid获取用户
     * 
     * 作者: zhoubang 
     * 日期：2015年5月7日 下午4:13:34
     * @param openId
     * @return
     * @throws Exception
     */
    public User getUserByOpenId(String openId) throws Exception;
}
