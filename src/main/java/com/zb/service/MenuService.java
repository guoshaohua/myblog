package com.zb.service;

import com.zb.bean.Menu;
import com.zb.qo.MenuQo;
import com.zb.vo.PageVo;

/**
 * 菜单接口
 * 
 * 作者: zhoubang 
 * 日期：2015年4月24日 下午5:20:26
 */
public interface MenuService {

    /**
     * 获取菜单列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 下午5:16:07
     * @return
     */
    public PageVo<Menu> getMenuList(MenuQo menuQo) throws Exception;
    
    /**
     * 添加菜单
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午1:16:26
     * @param menu
     * @throws Exception
     */
    public void addMenu(Menu menu) throws Exception;
    
    /**
     * 更新菜单信息
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午3:23:55
     * @param menu
     * @throws Exception
     */
    public void updateMenu(Menu menu) throws Exception;
    
    /**
     * 获取菜单详细
     * 
     * 作者: zhoubang 
     * 日期：2015年5月4日 下午3:27:19
     * @param id
     * @throws Exception
     */
    public Menu getMenuById(String id) throws Exception;
}


