package com.zb.service;

import java.util.List;
import com.zb.bean.UserRoleRef;

public interface UserRoleService {

    /**
     * 获取会员的角色列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:36:14
     * @param userId
     * @return
     * @throws Exception
     */
    public List<UserRoleRef> getRolesByUserId(String userId) throws Exception;
    
    /**
     * 删除会员角色
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:36:05
     * @param userId
     * @throws Exception
     */
    public void delRolesByUserid(String userId) throws Exception;
    
    /**
     * 新增会员角色
     * 
     * 作者: zhoubang 
     * 日期：2015年4月29日 上午10:41:56
     * @param userRoleRef
     * @throws Exception
     */
    public void addUserRole(UserRoleRef userRoleRef) throws Exception;
}

