package com.zb.service;

import java.util.List;

import com.zb.bean.Role;
import com.zb.qo.RoleQo;
import com.zb.vo.PageVo;
import com.zb.vo.ZtreeVo;

/**
 * 角色接口
 * 
 * 作者: zhoubang 日期：2015年3月26日 上午11:26:40
 */
public interface RoleService {

    /**
     * 获取用户的角色列表
     * 
     * 作者: zhoubang 日期：2015年3月26日 上午11:27:11
     * 
     * @param userName
     * @return
     */
    public List<Role> getUserRoles(String userName) throws Exception;
    
    
    /**
     * 获取所有角色列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:45:06
     * @return
     */
    public PageVo<Role> getRoleList(RoleQo roleQo) throws Exception;
    
    /**
     * 根据id获取角色
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午4:59:08
     * @param role
     * @return
     * @throws Exception
     */
    public Role getRoleById(Role role) throws Exception;
    
    /**
     * 更新角色信息
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午5:47:13
     * @param role
     * @throws Exception
     */
    public void updateRole(Role role) throws Exception;
    
    
    /**
     * 查询所有的角色格式化成ztree需要的数据
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午1:31:43
     * @param isModify
     * @return
     * @throws Exception
     */
    public List<ZtreeVo> queryAllFormatWithZtree(boolean isModify) throws Exception;
    
}
