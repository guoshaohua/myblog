package com.zb.service.impl;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Navigation;
import com.zb.dao.NavigationDao;
import com.zb.enums.system.NavigationEnums;
import com.zb.qo.NavigationQo;
import com.zb.service.NavigationService;
import com.zb.vo.PageVo;

@Service(value = "navigationServiceImpl")
@Transactional
public class NavigationServiceImpl implements NavigationService {

    @Resource(name = "navigationDaoImpl")
    private NavigationDao navigationDao;

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getNavigationList() throws Exception {
        List<Navigation> navigationList = navigationDao.queryBySQL("SELECT * FROM `blog_navigation` nav WHERE nav.`available` = 1 ORDER BY nav.`indexNo` ASC");

        Map<String, Map<String, Object>> dataMaps = new LinkedHashMap<String, Map<String, Object>>();
        // 临时存放子导航数据
        List<Navigation> subNavigation = new ArrayList<Navigation>();
        for (int i = 0; i < navigationList.size(); i++) {
            Navigation navigation = navigationList.get(i);
            // 父
            if (navigation.getPid().equals("0")) {
                Map<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("parent", navigation);
                dataMap.put("sub", new ArrayList<Object>());
                dataMaps.put(String.valueOf(navigation.getId()), dataMap);
            } else {
                // 子
                subNavigation.add(navigation);
            }
        }
        // 遍历子导航数据集合，加入到父导航容器中
        for (int i = 0; i < subNavigation.size(); i++) {
            Navigation navigation = subNavigation.get(i);
            if (!navigation.getPid().equals("0")) {
                List<Object> list = (List<Object>) dataMaps.get(String.valueOf(navigation.getPid())).get("sub");
                list.add(navigation);
            }
        }
        List<Object> data = new ArrayList<Object>();
        data.add(dataMaps.values());
        return data;
    }

    @Override
    public List<Navigation> getModuleSub() throws Exception {
        return navigationDao.queryBySQL("SELECT na.`id`,na.`name`,na.pid,na2.name parentName FROM `blog_navigation` na LEFT JOIN blog_navigation na2 ON na2.id = na.pid  WHERE na.`id` IN (SELECT n.id FROM `blog_navigation` n WHERE n.linkType = 1)");
    }

    @Override
    public PageVo<Navigation> getNavigationListFromSystem(
            NavigationQo navigationQo) throws Exception {
        Map<String, Object> queryMap = new HashMap<String, Object>();
        if(StringUtils.isNotEmpty(navigationQo.getName())){
            queryMap.put("name", navigationQo.getName());
        }
        return navigationDao.queryWithPage(navigationQo.getPageNo(), navigationQo.getPageSize(),queryMap);
    }

    @Override
    public Navigation getNavigationById(String id) throws Exception {
        return navigationDao.load(id);
    }

    @Override
    public void updateNavigation(Navigation navigation) throws Exception {
        navigation.setName(URLDecoder.decode(navigation.getName(), "UTF-8"));
        navigation.setLink("".equals(navigation.getLink()) ? null : navigation.getLink());
        navigationDao.update(navigation);
    }

    @Override
    public List<Navigation> getModuleParent() throws Exception {
        return navigationDao.queryBySQL("SELECT n.`id`,n.`name`,n.`pid` FROM `blog_navigation` n WHERE n.`pid` = 0 AND n.`available` = 1");
    }

    @Override
    public void addNavigation(Navigation navigation) throws Exception {
        navigation.setName(URLDecoder.decode(navigation.getName(), "UTF-8"));
        if(!"0".equals(navigation.getPid()) && navigation.getLinkType() == NavigationEnums.NavigationLinkType.VIEW.getType()){
            navigation.setLink(URLDecoder.decode(navigation.getLink(), "UTF-8"));
        }
        navigationDao.save(navigation);
    }

}
