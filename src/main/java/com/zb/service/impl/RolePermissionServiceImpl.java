package com.zb.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.RolePermissionRef;
import com.zb.dao.RolePermissionDao;
import com.zb.service.RolePermissionService;

@Service(value = "rolePermissionServiceImpl")
@Transactional
public class RolePermissionServiceImpl implements RolePermissionService {

    @Resource(name="rolePermissionDaoImpl")
    private RolePermissionDao rolePermissionDao;
    
    @Override
    public List<RolePermissionRef> getRoleIdsByPermission(String id) throws Exception {
        return rolePermissionDao.query("permissionId", id);
    }

    @Override
    public void delPermissionByRole(String roleId) throws Exception {
        rolePermissionDao.delete("roleId", roleId);
    }

    @Override
    public void addPermission(RolePermissionRef rolePermission) throws Exception {
        rolePermissionDao.save(rolePermission);
    }

    @Override
    public List<RolePermissionRef> getPermissionByRoleId(String roleId) throws Exception {
        return rolePermissionDao.query("roleId", roleId);
    }

}

