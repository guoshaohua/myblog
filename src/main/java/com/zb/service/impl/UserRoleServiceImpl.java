package com.zb.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.UserRoleRef;
import com.zb.dao.UserRoleDao;
import com.zb.service.UserRoleService;

@Service(value = "userRoleServiceImpl")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    @Resource(name="userRoleDaoImpl")
    private UserRoleDao userRoleDao;
    
    @Override
    public List<UserRoleRef> getRolesByUserId(String userId) throws Exception {
        return userRoleDao.query("userId", userId);
    }

    @Override
    public void delRolesByUserid(String userId) throws Exception {
        userRoleDao.delete("userId", userId);
    }

    @Override
    public void addUserRole(UserRoleRef userRoleRef) throws Exception {
        userRoleDao.save(userRoleRef);
    }

}

