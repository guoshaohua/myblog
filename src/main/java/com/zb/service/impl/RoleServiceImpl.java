package com.zb.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.zb.bean.Role;
import com.zb.dao.RoleDao;
import com.zb.orm.query.OrderBy;
import com.zb.qo.RoleQo;
import com.zb.service.RoleService;
import com.zb.vo.PageVo;
import com.zb.vo.ZtreeVo;

/**
 * 角色 操作Service
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午2:07:19
 */
@Service(value = "roleServiceImpl")
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public List<Role> getUserRoles(String userName) throws Exception {
        return roleDao.getUserRoles(userName);
    }

    @Override
    public PageVo<Role> getRoleList(RoleQo roleQo) throws Exception {
        return roleDao.queryWithPage(roleQo.getPageNo(), roleQo.getPageSize(), RoleQo.buildMap(roleQo), OrderBy.asc("id"));
    }

    @Override
    public Role getRoleById(Role role) throws Exception {
        return roleDao.load(role.getId());
    }

    @Override
    public void updateRole(Role role) throws Exception {
        roleDao.update(role);
    }

    @Override
    public List<ZtreeVo> queryAllFormatWithZtree(boolean isModify)
            throws Exception {
        List<ZtreeVo> results = new ArrayList<ZtreeVo>();
        if(!isModify){
            ZtreeVo result = new ZtreeVo();
            result.setId("-1");
            result.setpId("0");
            result.setName("无上级权限");
            results.add(result);
        }
        List<Role> roles = roleDao.queryAll();
        if (CollectionUtils.isNotEmpty(roles)) {
            for (Role role : roles) {
                ZtreeVo foo = new ZtreeVo();
                foo.setId(role.getId());
                foo.setName(role.getName());
                results.add(foo);
            }
        }
        return results;
    }
}
