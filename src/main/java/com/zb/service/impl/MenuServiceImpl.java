package com.zb.service.impl;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Menu;
import com.zb.dao.MenuDao;
import com.zb.qo.MenuQo;
import com.zb.service.MenuService;
import com.zb.vo.PageVo;


@Service(value = "menuServiceImpl")
@Transactional
public class MenuServiceImpl implements MenuService {

    @Resource(name="menuDaoImpl")
    private MenuDao menuDao;
    
    @Override
    public PageVo<Menu> getMenuList(MenuQo menuQo) throws Exception {
        Map<String, Object> parmas = new HashMap<String, Object>();
        if(StringUtils.isNotEmpty(menuQo.getName())){
            parmas.put("name", URLDecoder.decode(menuQo.getName(),"UTF-8"));
        }
        if(menuQo.getModel() > 0){
            parmas.put("model", menuQo.getModel());
        }
        return menuDao.queryWithPage(menuQo.getPageNo(), menuQo.getPageSize(), parmas);
    }

    @Override
    public void addMenu(Menu menu) throws Exception {
        menu.setName(URLDecoder.decode(menu.getName(),"UTF-8"));
        menuDao.save(menu);
    }

    @Override
    public void updateMenu(Menu menu) throws Exception {
        menu.setName(URLDecoder.decode(menu.getName(),"UTF-8"));
        if(StringUtils.isNotEmpty(menu.getLink())){
            menu.setLink(URLDecoder.decode(menu.getLink(),"UTF-8"));
        }
        menuDao.update(menu);
    }

    @Override
    public Menu getMenuById(String id) throws Exception {
        return menuDao.load(id);
    }

}

