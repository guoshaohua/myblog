package com.zb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.User;
import com.zb.dao.RoleDao;
import com.zb.dao.UserDao;
import com.zb.orm.query.OrderBy;
import com.zb.qo.UserQo;
import com.zb.service.UserService;
import com.zb.vo.PageVo;

@Service(value = "userServiceImpl")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Override
    public User getUserByName(String userName) throws Exception {
        return userDao.queryFirst("userName", userName);
    }

    @Override
    public void register(User user) throws Exception {
        userDao.save(user);
    }

    @Override
    public PageVo<User> getUserList(UserQo userQo) throws Exception {
        return userDao.queryWithPage(userQo.getPageNo(), userQo.getPageSize(), UserQo.buildMap(userQo), OrderBy.desc("createTime"));
    }

    @Override
    public User getUserById(String id) throws Exception {
        return userDao.load(id);
    }

    @Override
    public void updateUser(User user) throws Exception {
        String fields[] = {"userName","phone","email","updateTime"};
        Object fieldValues[] = {user.getUserName(),user.getPhone(),user.getEmail(),user.getUpdateTime()};
        userDao.updateField(user.getId(), fields, fieldValues);
    }

    @Override
    public void updateUserStatus(String id, Integer status) throws Exception {
        userDao.updateField(id, "status", status);
    }

    @Override
    public User getUserByOpenId(String openId) throws Exception {
        return userDao.queryFirst("openId", openId);
    }

}
