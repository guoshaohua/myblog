package com.zb.service.impl;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.bean.Article;
import com.zb.bean.Navigation;
import com.zb.dao.ArticleDao;
import com.zb.orm.query.OrderBy;
import com.zb.qo.ArticleQo;
import com.zb.service.ArticleService;
import com.zb.service.UserService;
import com.zb.vo.PageVo;

/**
 * 文章业务处理类
 * 
 * @author zhoubang
 *
 */
@Service(value = "articleServiceImpl")
@Transactional
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao articleDao;
    
    @Autowired
    private UserService userService;
    
    @Override
    public PageVo<Article> getArticleList(ArticleQo articleQo) throws Exception {
        return articleDao.getArticleList(articleQo);
    }

    @Override
    public void saveArticle(Article article) throws Exception {
        articleDao.save(article);
    }

    @Override
    public PageVo<Article> getArticleListByNavigationId(ArticleQo articleQo)
            throws Exception {
        return articleDao.queryWithPage(articleQo.getPageNo(),
                articleQo.getPageSize(), "navigationId",
                articleQo.getNavigationId(), OrderBy.desc("createTime"));
    }

    @Override
    public Article loadArticleContent(String id) throws Exception {
        return articleDao.load(id);
    }

    @Override
    public String getParentNameList(String id) throws Exception {
        return articleDao.getParentNameList(id);
    }

    @Override
    public List<Navigation> getArticleModels() throws Exception {
        List<Navigation> navigations = new ArrayList<Navigation>();
        
        List<Map<String, Object>> ls = articleDao.getArticleModels();
        for (Map<String, Object> map : ls) {
            Navigation navigation = new Navigation();
            navigation.setId(map.get("id").toString());
            navigation.setName(map.get("name").toString());
            navigations.add(navigation);
        }
        return navigations;
    }

    @Override
    public void updateArticle(Article article) throws Exception {
        Subject subject = SecurityUtils.getSubject();
        article.setUserId(userService.getUserByName(subject.getPrincipal().toString()).getId());
        
        String fileds[] = {"title","navigationId","available","content"};
        String values[] = {URLDecoder.decode(article.getTitle(),"UTF-8"),article.getNavigationId(),article.getAvailable() + "",article.getContent()};
        articleDao.updateField(article.getId(), fileds, values);
    }

    @Override
    public Article load(String id) throws Exception {
        return articleDao.load(id);
    }
}
