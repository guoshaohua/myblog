package com.zb.service;

import java.util.List;

import com.zb.bean.Permission;
import com.zb.qo.PermissionQo;
import com.zb.vo.PageVo;
import com.zb.vo.PermissionVo;
import com.zb.vo.ZtreeVo;

/**
 * 权限操作
 * 
 * 作者: zhoubang 日期：2015年3月26日 下午2:10:45
 */
public interface PermissionService {

    /**
     * 获取用户权限列表
     * 
     * 作者: zhoubang 日期：2015年3月26日 下午2:10:51
     * 
     * @return
     */
    public List<Permission> getUserPermissions(String userName) throws Exception;
    
    /**
     * 获取权限列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 上午11:02:04
     * @param permissionQo
     * @return
     * @throws Exception
     */
    public PageVo<PermissionVo> getPermissionList(PermissionQo permissionQo) throws Exception;
    
    
    /**
     * 查询所有的权限格式化成ztree需要的数据
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午1:31:43
     * @param isModify
     * @return
     * @throws Exception
     */
    public List<ZtreeVo> queryAllFormatWithZtree(boolean isModify) throws Exception;
    
    /**
     * 新增权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午2:33:39
     * @param permission
     */
    public void addPermission(Permission permission) throws Exception;
    
    /**
     * 删除权限
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午3:04:02
     * @param permission
     * @throws Exception
     */
    public void delPermission(Permission permission) throws Exception;
    
    /**
     * 检查该权限节点是否存在子节点
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午3:16:29
     * @param pid
     * @return
     */
    public List<Permission> getPermissionByPid(String id) throws Exception;
    
    /**
     * 检查code是否已经存在
     * 
     * 作者: zhoubang 
     * 日期：2015年4月28日 下午4:14:07
     * @param permission
     * @return   1:名称重复   2:code重复   0:可以新增
     * @throws Exception
     */
    public boolean checkCodeIsExists(Permission permission) throws Exception;
    
}
