package com.zb.service;

import java.util.List;
import com.zb.bean.Article;
import com.zb.bean.Navigation;
import com.zb.qo.ArticleQo;
import com.zb.vo.PageVo;

public interface ArticleService {

    /**
     * 获取文章列表
     * 
     * 作者: zhoubang 日期：2015年4月2日 下午3:02:02
     * 
     * @return
     * @throws Exception
     */
    public PageVo<Article> getArticleList(ArticleQo articleQo) throws Exception;

    /**
     * 发布文章
     * 
     * @param article
     * @return
     * @throws Exception
     */
    public void saveArticle(Article article) throws Exception;

    /**
     * 根据模块id获取下面的所有文章
     * 
     * @param article
     * @return
     * @throws Exception
     */
    public PageVo<Article> getArticleListByNavigationId(ArticleQo articleQo)
            throws Exception;

    /**
     * 获取文章内容
     * 
     * @param id
     * @return
     * @throws Exception
     */
    public Article loadArticleContent(String id) throws Exception;

    /**
     * 根据id获取该菜单父级菜单的名称集合. 前端页面显示 —— 当前位置
     * 
     * 作者: zhoubang 日期：2015年4月16日 下午3:46:39
     * 
     * @param id
     * @return
     * @throws Exception
     */
    public String getParentNameList(String id) throws Exception;
    
    /**
     * 后台管理-文章列表查询-
     *      文章所属模块列表
     * 
     * 作者: zhoubang 
     * 日期：2015年5月8日 上午9:20:38
     * @return
     * @throws Exception
     */
    public List<Navigation> getArticleModels() throws Exception;
    
    /**
     * 更新文章
     * 
     * 作者: zhoubang 
     * 日期：2015年5月8日 上午10:51:08
     * @param article
     * @throws Exception
     */
    public void updateArticle(Article article) throws Exception;
    
    /**
     * 获取文章对象
     * 
     * 作者: zhoubang 
     * 日期：2015年5月21日 上午9:28:02
     * @param id
     * @return
     * @throws Exception
     */
    public Article load(String id)  throws Exception;
}
