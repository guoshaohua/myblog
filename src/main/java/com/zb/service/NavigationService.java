package com.zb.service;

import java.util.List;

import com.zb.bean.Navigation;
import com.zb.qo.NavigationQo;
import com.zb.vo.PageVo;

public interface NavigationService {

    /**
     * 获取导航数据(非后台获取)
     * 
     * @return
     */
    public List<Object> getNavigationList() throws Exception;

    /**
     * 获取所有子分类菜单导航
     * 
     * @return
     */
    public List<Navigation> getModuleSub() throws Exception;
    
    /**
     * 后台获取导航数据
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午9:39:47
     * @param navigationQo
     * @return
     * @throws Exception
     */
    public PageVo<Navigation> getNavigationListFromSystem(NavigationQo navigationQo) throws Exception;
    
    /**
     * 获取导航数据
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午9:58:43
     * @param id
     * @return
     * @throws Exception
     */
    public Navigation getNavigationById(String id) throws Exception;
    
    /**
     * 更新导航
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 上午10:06:28
     * @param navigation
     * @throws Exception
     */
    public void updateNavigation(Navigation navigation) throws Exception;
    
    /**
     * 新增导航
     * 
     * 作者: zhoubang 
     * 日期：2015年5月6日 下午1:12:11
     * @param navigation
     * @throws Exception
     */
    public void addNavigation(Navigation navigation) throws Exception;
    
    /**
     * 获取所有父分类菜单导航
     * 
     * @return
     */
    public List<Navigation> getModuleParent() throws Exception;
}
