<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/menu/menuList.js'/>"></script>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">菜单管理</a></li>
                    <li class="active">菜单列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <!-- <div class="col-xs-3 text-left">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus"></span>新增优惠券
                </button>
            </div> -->
            <div class="col-xs-12 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/menu/getMenuList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">菜单类型</div>
                            <select name="model" class="form-control">
                                <option value="-1">全部</option>
                                <c:forEach items="${menuType}" var="t">
                                    <option value="${t.type}">${t.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">菜单名称</div>
                            <input class="form-control" type="text" name="name" placeholder="菜单名称" value="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="menuTable"></table>
            </div>
        </div>
    </div>
    
    <!-- 菜单列表请求地址 -->
    <input type="hidden" name="menuList" value="<c:url value="/menu/getMenuList"/>"/>
    
    <!-- 更新菜单URL -->
    <input type="hidden" name="editMenuUrl" value="<c:url value="/menu/editMenuView"/>"/>
    
</body>
</html>