<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/js/admin/menu/view.js"/>" type="text/javascript"></script>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">菜单管理</a></li>
                    <li class="active">编辑菜单</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form id="permissionForm" class="form-horizontal paddingLeft24" role="form" method="post" action="javascript:editMenu()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">菜单名称：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="name" name="name" value="${menu.name}" required>
                    </div>
                </div>
                <c:if test="${menu.pid ne '-1'}">
                    <div class="form-group-split"></div>
                    <div class="form-group">
                        <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">菜单链接：</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" id="link" name="link" value="${menu.link}">
                        </div>
                    </div>
                </c:if>
                <div class="col-xs-12">&nbsp;</div>
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-11">
                        <button type="submit" class="btn btn-primary" data-loading-text="保存中...">保存</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" onclick="javascript:history.back(-1);">返回</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <input type="hidden" id="menuId" name="menuId" value="${menu.id}"/>
    <input type="hidden" id="menuPid" name="menuId" value="${menu.pid}"/>
    <input type="hidden" id="model" name="model" value="${menu.model}"/>
    <!-- 更新菜单信息URL -->
    <input type="hidden" name="editMenuUrl" value="<c:url value="/menu/editMenu"/>"/>
</body>
</html>