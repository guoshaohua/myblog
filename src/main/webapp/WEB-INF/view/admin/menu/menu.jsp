<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle-custom.css"/>" rel="stylesheet">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/ztree/js/jquery.ztree.core-3.5.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/utils/jquery.validate.min.js"/>" type="text/javascript" ></script>
    <script src="<c:url value="/resources/js/admin/menu/menu.js"/>" type="text/javascript"></script>
    <style type="text/css">
        .error {
            color: red;
            padding-top: 8px;
        }
    </style>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">菜单管理</a></li>
                    <li class="active">编辑菜单</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form id="permissionForm" class="form-horizontal paddingLeft24" role="form" method="post" action="javascript:addMenu()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">菜单名称</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="name" name="name" required autofocus>
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">是否父级</label>
                    <div class="col-xs-8">
                        <select name="menuType" class="form-control" onchange="changeParent()">
                            <option value="1" selected="selected">是</option>
                            <option value="2">否</option>
                        </select>
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group" id="parentMenuDiv" style="display: none;">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">父级菜单</label>
                    <div class="col-xs-8">
                        <select id="pid" name="pid" class="form-control">
                            <c:forEach items="${selParent}" var="vo" varStatus="i">
                                <option value="${vo.id}">${vo.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group" id="linkDiv" style="display: none;">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">菜单链接</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="link" name="link">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="parentId" class="col-xs-2 control-label" style="font-weight:normal;">菜单预览</label>
                    <div class="col-xs-8">
                        <div style="height: 350px;width: 100%;border: solid 1px #ccc;overflow: scroll;overflow-x: hidden;background-color: #f3f3f4;">
                            <div class="left">
                                <!-- 菜单tree  start -->
                                <ul id="menuTree" class="ztree"></ul>
                                <!-- 菜单tree  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-11">
                        <button type="submit" class="btn btn-primary" data-loading-text="添加中...">添加</button>
                        <!-- <button type="button" class="btn btn-danger" data-loading-text="删除中..." onclick="javascript:delSysMenu()">删除</button> -->
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" onclick="javascript:history.back(-1);">返回</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <!-- 菜单json数据 -->
    <input type="hidden" name="menus" value='${menus}'/>
    <!-- 选中的节点对应的菜单id -->
    <input type="hidden" name="menuId" value="-1"/>
    <!-- 所有父菜单列表 -->
    <input type="hidden" name="parentMenus" value='${parentMenus}'/>
    <!-- 添加菜单URL -->
    <input type="hidden" name="addMenuUrl" value="<c:url value="/menu/addMenu"/>"/>
    <!-- 标识是系统菜单还是会员菜单 -->
    <input type="hidden" name="model" value='${model}'/>
    
</body>
</html>