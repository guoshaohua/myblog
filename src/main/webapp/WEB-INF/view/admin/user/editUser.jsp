<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle-custom.css"/>" rel="stylesheet">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/ztree/js/jquery.ztree.core-3.5.js"/>" type="text/javascript"></script>
    <script type="text/javascript" src="<c:url value='/resources/ztree/js/jquery.ztree.excheck-3.5.js'/>"></script>
    <script src="<c:url value="/resources/js/utils/jquery.validate.min.js"/>" type="text/javascript" ></script>
    <script src="<c:url value="/resources/js/admin/user/editUser.js"/>" type="text/javascript"></script>
    <style type="text/css">
        .error {
            color: red;
            padding-top: 8px;
        }
    </style>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">会员管理</a></li>
                    <li class="active">编辑会员</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form id="permissionForm" class="form-horizontal paddingLeft24" role="form" method="post" action="javascript:updateUser()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">会员名称：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="userName" name="userName" value="${userInfo.userName}">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">手机号：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="phone" name="phone" value="${userInfo.phone}">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">邮箱：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="email" name="email" value="${userInfo.email}">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="parentId" class="col-xs-2 control-label" style="font-weight:normal;">角色：</label>
                    <div class="col-xs-8">
                        <div style="height: 200px;width: 100%;border: solid 1px #ccc;overflow: scroll;overflow-x: hidden;background-color: #f3f3f4;">
                            <div class="left">
                                <!-- 角色tree  start -->
                                <ul id="roleTree" class="ztree"></ul>
                                <!-- 角色tree  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-11">
                        <button type="submit" class="btn btn-primary" data-loading-text="保存中...">保存</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" onclick="javascript:history.back(-1);">返回</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <input type="hidden" name="userId" value="${userInfo.id}"/>
    <!-- 更新会员信息URL -->
    <input type="hidden" name="updateUserUrl" value="<c:url value="/user/updateUser"/>"/>
    <!-- 存放选中的角色id -->
    <input type="hidden" id="roleIds" name="roleIds" value=""/>
    <!-- 角色列表json字符串，这里value必须是单引号，避免与roles值中的双引号冲突，出现问题 -->
    <input type="hidden" name="roles" value='${roles}'/>
</body>
</html>