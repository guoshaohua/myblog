<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/user/userList.js'/>"></script>
        
        <!-- 表格行背景色 -->
        <style type="text/css">
            .lock{background-color: #faebcc;}
            .del{background-color: #ebccd1;}
        </style>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">会员管理</a></li>
                    <li class="active">会员列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <!-- <div class="col-xs-3 text-left">
                <button type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus"></span>新增优惠券
                </button>
            </div> -->
            <div class="col-xs-12 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/user/userList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">用户状态</div>
                            <select name="status" class="form-control">
                                <option value="-1">全部</option>
                                <c:forEach items="${status}" var="cts">
                                    <option value="${cts.status}">${cts.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">用户名称</div>
                            <input class="form-control" type="text" name="userName" placeholder="用户名称" value="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="userTable" data-row-style="rowStyle"></table>
            </div>
        </div>
    </div>
    
    <!-- 用户列表请求地址 -->
    <input type="hidden" name="userList" value="<c:url value="/user/userList"/>"/>
    <!-- 进入编辑会员页面 -->
    <input type="hidden" name="editUserUrl" value="<c:url value="/user/editUserView"/>"/>
    <!-- 更新用户状态URL -->
    <input type="hidden" name="updateUserStatusUrl" value="<c:url value="/user/updateUserStatus"/>"/>
</body>
</html>