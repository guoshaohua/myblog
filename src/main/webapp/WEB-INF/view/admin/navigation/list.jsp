<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/navigation/list.js'/>"></script>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">导航管理</a></li>
                    <li class="active">导航列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <div class="col-xs-3 text-left">
                <button type="button" class="btn btn-default" onclick="javascript:location.href='<c:url value="/navigation/addNavigationView"/>'">
                    <span class="glyphicon glyphicon-plus"></span>新增导航
                </button>
            </div>
            <div class="col-xs-9 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/navigation/navigationList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">导航名称</div>
                            <input class="form-control" type="text" name="name" placeholder="导航名称" value="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="navigationTable"></table>
            </div>
        </div>
    </div>
    
    <!-- 导航列表请求地址 -->
    <input type="hidden" name="navigationList" value="<c:url value="/navigation/navigationList"/>"/>
    <!-- 进入编辑导航页面 -->
    <input type="hidden" name="editNavigationUrl" value="<c:url value="/navigation/editNavigationView"/>"/>
</body>
</html>