<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/js/admin/navigation/view.js"/>" type="text/javascript"></script>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">导航管理</a></li>
                    <li class="active">编辑导航</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form id="permissionForm" class="form-horizontal paddingLeft24" role="form" method="post" action="javascript:editNavigation()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">导航名称：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="name" name="name" value="${navigationInfo.name}" required>
                    </div>
                </div>
                
                <c:if test="${navigationInfo.pid != 0}">
                    <div class="form-group-split"></div>
                    <div class="form-group" id="pidDiv">
                        <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">所属父级：</label>
                        <div class="col-xs-8">
                            <select id="pid" name="pid" class="form-control">
                                <c:forEach items="${parentList}" var="vo" varStatus="i">
                                    <option value="${vo.id}" <c:if test="${vo.id == navigationInfo.pid}">selected="selected"</c:if>>${vo.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </c:if>
                
                <c:if test="${navigationInfo.pid != 0}">
                    <c:if test="${navigationInfo.linkType == 2}">
                        <div class="form-group-split"></div>
                        <div class="form-group" id="linkDiv" <c:if test="${navigationInfo.linkType == 2}">style="display:none;"</c:if>>
                            <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">导航链接：</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" id="link" name="link" value="${navigationInfo.link}" <c:if test="${navigationInfo.pid != 0}">required</c:if> >
                            </div>
                        </div>
                    </c:if>
                </c:if>
                
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">是否可用：</label>
                    <div class="col-xs-8">
                        <select id="available" name="available" class="form-control">
                            <c:forEach items="${availableStatus}" var="vo" varStatus="i">
                                <option value="${vo.type}" <c:if test="${vo.type == navigationInfo.available}">selected="selected"</c:if>>${vo.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                
                <div class="col-xs-12">&nbsp;</div>
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-11">
                        <button type="submit" class="btn btn-primary" data-loading-text="保存中...">保存</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" onclick="javascript:history.back(-1);">返回</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <input type="hidden" id="navigationId" name="navigationId" value="${navigationInfo.id}"/>
    <input type="hidden" id="linkType" name="linkType" value="${navigationInfo.linkType}"/>
    <!-- 更新导航信息URL -->
    <input type="hidden" name="editNavigationUrl" value="<c:url value="/navigation/editNavigation"/>"/>
</body>
</html>