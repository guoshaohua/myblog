<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>发布文章</title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script type="text/javascript" src="<c:url value="/resources/js/admin/article/add.js"/>"></script>
        
        <!-- 设置编辑器的高度和距离 -->
        <style type="text/css">
            #container{
                height: 600px;
                margin-left: 0px;
            }
        </style>
    </head>
    <body>
        <!-- 系统消息提示   start -->
        <div class="alert alert-warning" role="alert" style="height: 25px; vertical-align: middle; width: 100%; padding: 0px;margin-top: -50px;">
            <i class="fa fa-volume-up"></i> 
            <span style="margin-left: 10px; position: absolute;">若需要代码高亮显示，请手动在代码前后加入< pre>< /pre>标签</span>
        </div>
        <!-- 系统消息提示   end -->
        
        <table style="width: 1000px; border: 0px;">
            <tr>
                <td>
                    <div class="form-group">
                        <div class="input-group col-xs-10">
                            <div class="input-group-addon">文章标题</div>
                            <input class="form-control" id="title" type="text" name="title" placeholder="文章标题" value="">
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class="input-group col-xs-9">
                            <div class="input-group-addon">所属模块</div>
                            <select id="moduleSubId" class="form-control">
                                <c:forEach items="${modelSubList}" var="vo">
                                    <option value="${vo.id}">${vo.parentName == null ? "主页" : vo.parentName} - ${vo.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        <div class="input-group col-xs-10">
                            <div class="input-group-addon">是否视图</div>
                            <select id="isViewOpen" class="form-control" onchange="selViewOpen()">
                                <option value="1">否</option>
                                <option value="2">是</option>
                            </select>
                        </div>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr >
                <td>
                    <div class="form-group" id="viewLinkDiv" style="display: none;">
                        <div class="input-group col-xs-10">
                            <div class="input-group-addon">视图链接</div>
                            <input id="viewLink" value="" class="form-control"/>
                        </div>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="font-size: 12px;">文章简介：</td>
            </tr>
            <tr>
                <td colspan="4"><textarea class="form-control" id="introduction" style="width: 680px; height: auto;"></textarea></td>
            </tr>
            
        </table>
        <br />
        <div id="contentTitle" style="font-size: 12px;">文章内容:</div>
        
        
        <!-- 加载编辑器的容器 -->
        <script id="container" name="content" type="text/plain"></script>
        
        <!-- 配置文件 -->
        <script src="<c:url value="/resources/ueditor/ueditor.config.js"/>" type="text/javascript" ></script>
        <!-- 编辑器源码文件 -->
        <script src="<c:url value="/resources/ueditor/ueditor.all.js"/>" type="text/javascript" ></script>
        
        <!-- 实例化编辑器 -->
        <script type="text/javascript">
            UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
            UE.Editor.prototype.getActionUrl = function(action) {
                //进入查看在线图片列表.
                if (action == 'listimage') {
                    return "<c:url value="/article/imageListManage"/>";
                } else if (action == 'listfile') {//指定目录下文件列表
                    return "<c:url value="/article/fileListManage"/>";
                } else {
                    return this._bkGetActionUrl.call(this, action);
                }
            }
            
            var ue = UE.getEditor('container', {
                autoHeight: false
            });
        </script>
        
        <input type="button" class="btn btn-warning" name="button" id="subBtn" value="预览" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" class="btn btn-success" name="button" id="subBtnSave" value="保存" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="button" class="btn btn-default" name="button" value="返回" onclick="javascript:history.back(-1);"/>
        <br />
        <br />
        <fieldset style="width: 1200px;" id="filedSetDiv">
            <legend style="font-size: 14px; color: red;">预览区域(以此为准，此处才是最终的网页展示效果):</legend>
            <div id="view" style="width: 1175px;"></div>
        </fieldset>
    </body>
    
    
    <input type="hidden" name="getModuleSubUrl" value="<c:url value="/navigation/getModuleSub"/>"/>
    <input type="hidden" name="saveArticleUrl" value="<c:url value="/article/saveArticle"/>"/>
</html>