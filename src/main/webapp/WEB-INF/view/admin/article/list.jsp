<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/article/list.js'/>"></script>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">文章管理</a></li>
                    <li class="active">文章列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <div class="col-xs-3 text-left">
                <button type="button" class="btn btn-default" onclick="javascript:location.href='<c:url value="/article/toAddArticle"/>'">
                    <span class="glyphicon glyphicon-plus"></span>新增文章
                </button>
            </div>
            <div class="col-xs-9 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/article/articleList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">所属模块</div>
                            <select name="navigationId" class="form-control">
                                <option value="">请选择</option>
                                <c:forEach var="vo" items="${models}">
                                    <option value="${vo.id}">${vo.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="articleTable" data-row-style="rowStyle"></table>
            </div>
        </div>
    </div>
    
    <!-- 文章列表请求地址 -->
    <input type="hidden" name="articleList" value="<c:url value="/article/articleList"/>"/>
    <!-- 进入编辑文章页面 -->
    <input type="hidden" name="editArticleUrl" value="<c:url value="/article/editArticleView"/>"/>
    <input type="hidden" name="delArticleUrl" value="<c:url value="/article/delArticle"/>"/>
    <input type="hidden" name="showArticleUrl" value="<c:url value="/article/articleDetail"/>"/>
    
</body>
</html>