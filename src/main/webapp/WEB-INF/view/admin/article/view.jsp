<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/js/admin/article/view.js"/>" type="text/javascript"></script>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">文章管理</a></li>
                    <li class="active">编辑文章</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form class="form-horizontal paddingLeft24" role="form" action="javascript:updateArticle()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">文章标题：</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="title" name="title" value="${article.title}" required>
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">所属模块：</label>
                    <div class="col-xs-8">
                        <select class="form-control" name="navigationId">
                            <c:forEach items="${models}" var="vo">
                                <option value="${vo.id}" <c:if test="${vo.id == article.navigationId}">selected="selected"</c:if> >${vo.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group-split"></div>
                <br />
                
                <div style="font-size: 12px;">文章内容:</div>
                <!-- 加载编辑器的容器 -->
                <script id="container" name="content" type="text/plain">${article.content}</script>
                
                <!-- 配置文件 -->
                <script src="<c:url value="/resources/ueditor/ueditor.config.js"/>" type="text/javascript" ></script>
                <!-- 编辑器源码文件 -->
                <script src="<c:url value="/resources/ueditor/ueditor.all.js"/>" type="text/javascript" ></script>
                
                <!-- 实例化编辑器 -->
                <script type="text/javascript">
                    UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
                    UE.Editor.prototype.getActionUrl = function(action) {
                      //进入查看在线图片列表.
                        if (action == 'listimage') {
                            return "<c:url value="/article/imageListManage"/>";
                        } else if (action == 'listfile') {//指定目录下文件列表
                            return "<c:url value="/article/fileListManage"/>";
                        } else {
                            return this._bkGetActionUrl.call(this, action);
                        }
                    }
                    
                    var ue = UE.getEditor('container', {
                        autoHeight: false
                    });
                </script>
                
                <input type="button" class="btn btn-warning" name="button" id="subBtn" value="预览" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="submit" class="btn btn-primary" name="button" id="subBtnSave" value="保存" data-loading-text="保存中..."/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" class="btn btn-default" name="button" value="返回" onclick="javascript:history.back(-1);"/>
                <br />
                <br />
                <fieldset style="width: 1200px;">
                    <legend style="font-size: 14px; color: red;">预览区域(以此为准，此处才是最终的网页展示效果):</legend>
                    <div id="view" style="width: 1175px;"></div>
                </fieldset>
            </form>
        </div>
    </div>
    
    <input type="hidden" name="articleId" value="${article.id}"/>
    <!-- 更新文章信息URL -->
    <input type="hidden" name="updateArticleUrl" value="<c:url value="/article/updateArticle"/>"/>
</body>
</html>