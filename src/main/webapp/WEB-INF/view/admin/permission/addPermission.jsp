<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/ztree/css/zTreeStyle/zTreeStyle-custom.css"/>" rel="stylesheet">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value="/resources/ztree/js/jquery.ztree.core-3.5.js"/>" type="text/javascript"></script>
    <script src="<c:url value="/resources/js/utils/jquery.validate.min.js"/>" type="text/javascript" ></script>
    <script src="<c:url value="/resources/js/admin/permission/addPermission.js"/>" type="text/javascript"></script>
    <style type="text/css">
        .error {
            color: red;
            padding-top: 8px;
        }
    </style>
</head>
<body style="padding-top: 10px;width: 90%;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">权限管理</a></li>
                    <li class="active">编辑权限</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <form id="permissionForm" class="form-horizontal paddingLeft24" role="form" method="post" action="javascript:addPermission()">
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">权限名称</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="name" class="col-xs-2 control-label" style="font-weight:normal;">权限代码</label>
                    <div class="col-xs-8">
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
                <div class="form-group-split"></div>
                <div class="form-group">
                    <label for="parentId" class="col-xs-2 control-label" style="font-weight:normal;">权限列表</label>
                    <div class="col-xs-8">
                        <div style="height: 400px;width: 100%;border: solid 1px #ccc;overflow: scroll;overflow-x: hidden;background-color: #f3f3f4;">
                            <div class="left">
                                <!-- 权限tree  start -->
                                <ul id="permissionTree" class="ztree"></ul>
                                <!-- 权限tree  end -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">&nbsp;</div>
                <div class="form-group">
                    <div class="col-xs-offset-2 col-xs-11">
                        <button type="submit" class="btn btn-primary" data-loading-text="添加中...">添加</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-danger" data-loading-text="删除中..." onclick="javascript:delPermission()">删除</button>
                        &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-success" onclick="javascript:history.back(-1);">返回</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <input type="hidden" id="parentId" name="parentId" value="0">
    <!-- 检查code是否已经存在url -->
    <input type="hidden" name="checkCodeIsExists" value="<c:url value="/permission/checkCodeIsExists"/>">
    <!-- 添加权限URL -->
    <input type="hidden" name="addPermissionUrl" value="<c:url value="/permission/addPermission"/>">
    <!-- 删除权限URL -->
    <input type="hidden" name="delPermissionUrl" value="<c:url value="/permission/delPermission"/>">
    
    <!-- 权限列表json字符串，这里value必须是单引号，避免与permissions值中的双引号冲突，出现问题 -->
    <input type="hidden" name="permissions" value='${permissions}'/>
</body>
</html>