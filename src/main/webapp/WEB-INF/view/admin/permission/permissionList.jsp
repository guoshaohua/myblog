<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/permission/permissionList.js'/>"></script>
        
        <!-- 表格行背景色 -->
        <style type="text/css">
            .lock{background-color: #faebcc;}
            .del{background-color: #ebccd1;}
        </style>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">权限管理</a></li>
                    <li class="active">权限列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <div class="col-xs-3 text-left">
                <button type="button" class="btn btn-default" onclick="javascript:location.href='<c:url value="/permission/addPermissionView"/>'">
                    <span class="glyphicon glyphicon-plus"></span>新增权限
                </button>
            </div>
            <div class="col-xs-9 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/permission/permissionList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">权限名称</div>
                            <input class="form-control" type="text" name="name" placeholder="权限名称" value="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="permissionTable"></table>
            </div>
        </div>
    </div>
    
    <!-- 角色列表请求地址 -->
    <input type="hidden" name="permissionList" value="<c:url value="/permission/permissionList"/>"/>
</body>
</html>