<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>发布文章</title>
        <link rel="stylesheet" type="text/css" media="all" href="<c:url value="/resources/kindeditor-4.1.10/themes/default/default.css"/>">
        <link rel="stylesheet" type="text/css" media="all" href="<c:url value="/resources/kindeditor-4.1.10/plugins/code/prettify.css"/>">
        <script type="text/javascript" src="<c:url value="/resources/js/utils/jquery.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/kindeditor-4.1.10/kindeditor.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/kindeditor-4.1.10/lang/zh_CN.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/kindeditor-4.1.10/plugins/code/prettify.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/admin/release.js"/>"></script>
    </head>
    <body>
        <input type="hidden" name="fileManagerUrl" value="<c:url value="/FileManagerJsonServlet"/>" />
        <input type="hidden" name="fileUploadUrl" value="<c:url value="/UploadJsonServlet"/>" />
        <input type="hidden" name="prettifyUrl" value="<c:url value="/resources/kindeditor-4.1.10/plugins/code/prettify.css"/>" />
    
        <table style="width: 1000px; border: 0px;">
            <tr>
                <td>文章标题：</td>
                <td><input id="title" /></td>
                <td>所属模块：</td>
                <td><select id="moduleSubId">
                </select></td>
            </tr>
            <tr>
                <td colspan="4">文章简介：<input type="button" value="简介预览" id="jianjieBtn" /></td>
            </tr>
            <tr>
                <td colspan="4"><textarea id="introduction" style="width: 680px; height: auto;"></textarea></td>
            </tr>
            <tr>
                <td colspan="4">
                    <!-- 预览区域 -->
                    <fieldset style="width: 1000px;">
                        <legend style="font-size: 14px; color: red;">简介预览区域:</legend>
                        <div id="jianjieView" style="width: 680px;"></div>
                    </fieldset>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <textarea name="kindEditor" id="kindEditor" style="width: 1000px; height: 350px; font-size: 14px;"></textarea>
        <input type="button" name="button" id="subBtn" value="点此预览" />
        <input type="button" name="button" id="subBtnSave" value="保存" />
        <br />
        <!-- 预览区域 -->
        <fieldset style="width: 1000px;">
            <legend style="font-size: 14px; color: red;">预览区域(以此为准，此处才是最终的网页展示效果):</legend>
            <div id="view" style="width: 1000px;"></div>
        </fieldset>
    </body>
</html>