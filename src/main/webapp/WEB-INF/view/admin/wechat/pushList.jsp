<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script src="<c:url value='/resources/js/admin/wechat/pushList.js'/>"></script>
        
        <!-- 表格行背景色 -->
        <style type="text/css">
            .lock{background-color: #faebcc;}
            .del{background-color: #ebccd1;}
        </style>
    </head>
<body style="padding-top: 10px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0)">推送管理</a></li>
                    <li class="active">推送列表</li>
                </ol>
            </div>
        </div>
        <div class="row" style="margin-bottom: 20px">
            <div class="col-xs-12 text-right">
                <form class="form-inline paging-query" role="form" action="<c:url value="/user/userList"/>" method="post" id="queryForm" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">推送日期</div>
                            <input class="form-control" type="text" name="pushDate" placeholder="推送日期" value="">
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-method="refresh" id="refresh">查询</button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <table id="userTable" data-row-style="rowStyle"></table>
            </div>
        </div>
    </div>
    
    <!-- 推送列表请求地址 -->
    <input type="hidden" name="pushList" value="<c:url value="/wechat/pushList"/>"/>
</body>
</html>