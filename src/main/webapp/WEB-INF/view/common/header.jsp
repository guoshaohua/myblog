<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <%@ include file="/WEB-INF/view/common/common.jsp"%>
        <script type='text/javascript' src="<c:url value="/resources/js/user/header.js"/>"></script>
    </head>
<body>
    <!-- 导航栏	start -->
    <nav class="navbar navbar-default navbar-fixed-top"
        role="navigation">
        <div class="container" style="width: 100%;">
            <div class="navbar-header">

                <!-- 响应式   start-->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only"></span> <span class="icon-bar"></span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <!-- 响应式   start-->
            </div>

            <div id="navbar" class="navbar-collapse collapse" style="margin-left: 20%;">

                <!-- 导航菜单   start-->
                <ul class="nav navbar-nav">
                    <%-- <li><a class="navbar-brand" href="<c:url value="/index/index"/>">Welcome</a></li> --%>
                    <li><a>Welcome</a></li>
                </ul>
                <!-- 导航菜单    end-->
                
                <!-- 已经登录的   start-->
                <shiro:authenticated>
                    <div class="navbar-form navbar-right" role="form">
                        <img alt="" src="${sessionScope.logoImg}">
                        <span style="color: red;margin-left: 5px;margin-right: 20px;">欢迎你，<span>${sessionScope.nickName}</span></span>
                        <!-- 登录的，且角色是管理员 -->
                        <shiro:hasRole name="admin">
                            <button type="button" onclick="javascript:location.href='<c:url value="/system"/>'" class="btn btn-success"><i class="fa fa-user"></i>后台管理</button>
                        </shiro:hasRole>
                        
                        <!-- 登录的，且角色不是管理员，即：会员角色 -->
                        <shiro:lacksRole name="admin">
                            <button type="button" onclick="javascript:location.href='<c:url value="/center"/>'" class="btn btn-success"><i class="fa fa-user"></i>个人中心</button>
                        </shiro:lacksRole>
                        
                        <button type="button" onclick="javascript:location.href='<c:url value="/logout"/>'" class="btn btn-success"><i class="fa fa-power-off"></i>注销</button>
                    </div>
                </shiro:authenticated>
                <!-- 已经登录的   end-->
                
                <!-- 未登录的   start-->
                <shiro:notAuthenticated>
                    <div class="navbar-form navbar-right" role="form">
                        <%-- <button type="button" onclick="javascript:location.href='<c:url value="/toRegister"/>'" class="btn btn-success"><i class="fa fa-edit"></i>注册</button> --%>
                        <button type="button" onclick="javascript:location.href='<c:url value="/toLogin"/>'" class="btn btn-success"><i class="fa fa-user"></i>登录</button>
                    </div>
                </shiro:notAuthenticated>
                <!-- 未登录的   end -->
            </div>
        </div>
    </nav>
    <!-- 导航栏   end -->


    <!-- 系统消息提示   start -->
    <div class="alert alert-warning" role="alert" style="margin-top: 10px; height: 25px; vertical-align: middle; width: 100%; padding: 0px;">
        <i class="fa fa-volume-up" style="margin-left: 20%"></i> 
        <span style="margin-left: 10px;">请在Chrome、Firefox、IE等现代新版本浏览器浏览本站.</span>
    </div>
    <!-- 系统消息提示   end -->


    <!-- 首页地址 -->
    <input type="hidden" name="indexView" value="<c:url value="/index/index"/>" />
    <!-- 导航数据url -->
    <input type="hidden" name="navigationListUrl" value="<c:url value="/navigation/getNavigationList"/>" />
    <!-- 查询文章列表的url -->
    <input type="hidden" name="articleListUrl" value="<c:url value="/article/getArticleList"/>" />
    <!-- 项目根路径 -->
    <input type="hidden" name="projectUrl" value="<c:url value=""/>"/>
</body>
</html>