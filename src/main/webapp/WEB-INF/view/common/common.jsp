<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- <link rel="stylesheet" href="http://www.htmleaf.com/templets/default/css/common.css"> -->
<link href="<c:url value="/resources/css/common.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/docs.min.css"/>" rel="stylesheet">
<link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
<link type="text/css" media="all" href="<c:url value="/resources/css/index.css"/>" rel="stylesheet" />
<link type="text/css" media="all" href="<c:url value="/resources/css/backtop.css"/>" rel="stylesheet" />


<script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.min.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.cookie.js"/>"></script>

<script type="text/javascript" src='<c:url value="/resources/js/utils/bootstrap.min.js"/>'></script>
<script type="text/javascript" src='<c:url value="/resources/js/utils/jquery.bootstrap.js"/>'></script>

<script type='text/javascript' src="<c:url value="/resources/js/utils/bootstrapModal.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/utils/map.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/user/ajaxUtil.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/utils/backtop.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.lazyload.js"/>"></script>
<script type='text/javascript' src="<c:url value="/resources/js/user/common.js"/>"></script>

<link href="<c:url value='/resources/bootstrapTable/bootstrap-table.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/bootstrapTable/docs.css'/>" rel="stylesheet">
<script src="<c:url value='/resources/bootstrapTable/bootstrap-table.js'/>"></script>
<script src="<c:url value='/resources/bootstrapTable/locale/bootstrap-table-zh-CN.js'/>"></script>

<!-- 引入公共的模态框页面 -->
<jsp:include page="/WEB-INF/view/common/modal.jsp"/>

<style type="text/css">
    html {
        height: 100%;
    }
    
    body {
        height: 100%;
    }
    
    #container {
        height: 100%;
    }
    
    #wrap {
        height: 100%;
    }
    
    .tp figure figcaption p:last-child{
        text-transform : lowercase;
    }
</style>

<link rel="shortcut icon" href="<c:url value="/resources/images/favicon.ico"/>">
<input id="loadingImgUrl" type="hidden" value="<c:url value="/resources/images/loading.gif"/>" />