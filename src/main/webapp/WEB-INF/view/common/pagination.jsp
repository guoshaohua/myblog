<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <script type='text/javascript' src="<c:url value="/resources/js/user/pagination.js"/>"></script>
</head>
<body>
    <!-- 分页存放容器  -->
    <div id="pagination"></div>
</body>
</html>