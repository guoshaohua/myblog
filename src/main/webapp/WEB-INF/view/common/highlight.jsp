<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- 代码高亮 -->
<%-- <link type="text/css" media="all" href="<c:url value='/resources/css/monokai_sublime.css'/>" rel="stylesheet" /> --%>
<!-- <link type="text/css" media="all" href="http://cdn.bootcss.com/highlight.js/8.7/styles/magula.min.css" rel="stylesheet" /> -->
<link type="text/css" media="all" href="http://cdn.bootcss.com/highlight.js/8.7/styles/github.min.css" rel="stylesheet" />
<!-- <link type="text/css" media="all" href="http://cdn.bootcss.com/highlight.js/8.7/styles/default.min.css" rel="stylesheet" /> -->
<script src="<c:url value='/resources/js/utils/highlight.min.js'/>"></script>
<script type="text/javascript">
    hljs.configure({
        tabReplace: '    '//4个空格代替Tab切换
    });
    hljs.initHighlightingOnLoad();
</script>
