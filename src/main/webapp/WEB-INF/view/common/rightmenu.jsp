<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
<body>
    <!--右侧菜单-->
    <div class="col-lg-2 visible-lg" style="width:200px;margin-top: 55px;" id="rightdiv">
        <div data-am-sticky="{animation: 'slide-top',top:100}">
            <ul class="list-group" style="font-size:14px;">
              <a href="###" class="list-group-item on" href="jq1-jq1" data-para="time"><i class="fa fa-flag"></i> &nbsp;最新发布</a>
              <a href="###" class="list-group-item" href="jq1-jq4" data-para="comments"><i class="fa fa-heart"></i> &nbsp;最多收藏</a>
              <a href="###" class="list-group-item" href="jq1-jq2" data-para="comments"><i class="fa fa-comments-o"></i> &nbsp;最多评论</a>
              <a href="###" class="list-group-item" href="jq1-jq3" data-para="downloads"><i class="fa fa-arrow-circle-o-down"></i> &nbsp;最多下载</a>
              <a href="###" class="list-group-item" href="jq1-jq8" data-para="ie8"><i class="fa fa-bus"></i> &nbsp;浏览最多</a>
            </ul>
        </div>
    </div>
    <!--end 右侧菜单-->
</body>
</html>