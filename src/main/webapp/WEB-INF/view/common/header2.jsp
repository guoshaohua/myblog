<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="toTop" content="true">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            .navbar-inverse .navbar-nav>li>a{
                color: white;
            }
        </style>
        <script type='text/javascript' src="<c:url value="/resources/js/utils/toTop.js"/>"></script>
        <script type='text/javascript' src="<c:url value="/resources/js/user/header.js"/>"></script>
        <script type='text/javascript' src="<c:url value="/resources/js/utils/m.js"/>"></script>
        
        <script type='text/javascript' src="<c:url value="/resources/js/user/ajaxUtil.js"/>"></script>
        <script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.cookie.js"/>"></script>
    
        <link type="text/css" media="all" href="<c:url value="/resources/css/amazeui.min.css"/>" rel="stylesheet" />
        <script type='text/javascript' src="<c:url value="/resources/js/utils/amazeui.js"/>"></script>
    
        <script>var n = 0;</script>
        <script type='text/javascript' src="<c:url value="/resources/js/utils/canvas.js"/>"></script>
        
        <script type='text/javascript' src="http://hm.baidu.com/hm.js?8eb8e3ba0c301c99fbd4aafc9e20c1b5"></script>
        
    </head>
<body>
    <!--顶部导航-->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a title="欢迎访问我的博客网站" class="navbar-logo" href="<c:url value="/index/index"/>">
                <img src="<c:url value="/resources/images/twitter.png"/>" height="100%">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a target="_blank" href="http://git.oschina.net/zhoubang85/"><i class="fa fa-git"></i> &nbsp;我的开源中国</a></li>
                <li><a target="_blank" href="http://www.jq22.com/daima"><i class="fa fa-code"></i> &nbsp;在线代码</a></li>
                <li><a target="_blank" href="<c:url value="/online/view"/>"><i class="fa fa-cogs"></i> &nbsp;在线工具</a></li>
                <li><a target="_blank" href="http://www.jq22.com/cdn"><i class="fa fa-rocket"></i> &nbsp;CDN加速</a></li>
                <li class="dropdown"><a rel="nofollow" href="<c:url value="/favorite"/>" target="_blank"><i class="fa fa-bookmark"></i> &nbsp;网站收藏</a></li>
                <li>
                    <!-- 已经登录的   start-->
                    <shiro:authenticated>
                        <div class="myhome">
                            <c:if test="${sessionScope.logoImg == null}">
                                <a href="http://www.jq22.com/myhome"><img src="http://www.jq22.com/tx/26.png" /></a>
                            </c:if>
                            <c:if test="${sessionScope.logoImg != null}">
                                <a href="http://www.jq22.com/myhome"><img src="${sessionScope.logoImg}" /></a>
                            </c:if>
                            <div class="myhome-z bh2">
                                <!-- 登录的，且角色是管理员 -->
                                <shiro:hasRole name="admin">
                                    <a class="bh" href="<c:url value="/system"/>"><i class="fa fa-cog"></i> 后台管理</a> 
                                </shiro:hasRole>
                                
                                <!-- 登录的，且角色不是管理员，即：会员角色 -->
                                <shiro:lacksRole name="admin">
                                    <a class="bh" href="<c:url value="/center"/>"><i class="fa fa-user"></i> 个人中心</a> 
                                </shiro:lacksRole>
                                <a class="bh" href="<c:url value="/logout"/>"><i class="fa fa-sign-out"></i> 退出登录</a>
                            </div>
                        </div>
                    </shiro:authenticated>
                    <!-- 已经登录的   end-->
                </li>
                <!-- 未登录的   start-->
                <shiro:notAuthenticated>
                    <li>
                        <a class="bh" href="<c:url value="/toRegister"/>"><i class="fa fa-user"></i> 注册</a>
                    </li>
                    <li>
                        <a class="bh" href="<c:url value="/toLogin"/>"><i class="fa fa-user"></i> 登录</a>
                    </li>
                </shiro:notAuthenticated>
                <!-- 未登录的   end -->
            </ul>
        </div>
    </div>
</nav>
<!--end 顶部导航-->

<!-- 搜索 -->
<div class="container-fluid banner">
    <div class="banseo">
        <input style="font-size: 14px;" type="text" class="bantxt" id="searchtxt" placeholder="输入关键字，搜索文章"><button class="btn banbutt" type="button" id="seobut" ><i class="fa fa-search"></i></button>
    </div>
    <canvas></canvas>
</div>
<!-- end 搜索 -->

<!--导航菜单-->
<div class="jz container-fluid nav-bg m0 visible-lg visible-md visible-sm" id="menu_wrap">
    <!-- 主菜单 -->
    <div class="container m0" style="position: relative;" id="parentDiv"></div>
    <!-- end 主菜单 -->
    
    <div class="container-fluid" id="subDiv"></div>
</div>
<!--end 导航菜单-->


<!-- 导航数据url -->
<input type="hidden" name="navigationListUrl" value="<c:url value="/navigation/getNavigationList"/>" />
<!-- 项目根路径 -->
<input type="hidden" name="projectUrl" value="<c:url value=""/>"/>
<!-- 查询文章列表的url -->
<input type="hidden" name="articleListUrl" value="<c:url value="/article/getArticleList"/>" />
</body>
</html>