<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html class="ui-mobile">
<head>
    <title>会员登录</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10,IE=9,IE=8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/mobile-style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.mobile.flatui.css"/>">
    
    <script type="text/javascript" src="<c:url value="/resources/js/utils/jquery.mobile-1.4.5.js"/>"></script>
</head>
<body>
<div data-role="page">
    <div class="header linear-g navbar-fixed-top" >
        <a href="#panel-left" data-iconpos="notext" class="glyphicon glyphicon-th-large col-xs-2 text-right ui-link"></a> 
        <a class="text-center col-xs-8 ui-link">会员登录</a> 
        <a href="#panel-right" data-iconpos="notext" class="glyphicon glyphicon-user col-xs-2 text-left ui-link"></a>
    </div>
    <div data-role="panel" data-position="left" data-display="push" class="list-group shortcut_menu dn linear-g ui-panel ui-panel-position-left ui-panel-display-push ui-body-inherit ui-panel-closed" id="panel-left">
        <div class="ui-panel-inner" style="margin:10px;margin-top: 60px;">
            <a href="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/index.html#" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-home"> </span> &nbsp;首页
            </a>
            <a href="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/index.html#" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-edit"> </span> &nbsp;文章教程
            </a>
            <a href="###" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list"> </span> &nbsp;系统消息
            </a>
            <a href="###" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list-alt"> </span> &nbsp;关于网站
            </a>
        </div>
    </div>
    <div data-role="panel" data-position="right" data-display="push"
        class="user_box text-center dn linear-g ui-panel ui-panel-position-right ui-panel-display-push ui-panel-closed ui-body-inherit" id="panel-right">
        <div class="ui-panel-inner" style="margin-top: 50px;">
            <div class="u_info">
                <shiro:authenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> <span class="username"><shiro:principal/></span>
                </shiro:authenticated>
                <shiro:notAuthenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> 
                    <a href="<c:url value="/mobile/toLogin"/>"><span class="username">点此登录</span></a>
                </shiro:notAuthenticated>
            </div>
            <shiro:authenticated>
                <ul class="user_menu">
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-cog"> </span>&nbsp;个人资料
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-lock"> </span>&nbsp;修改密码
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-picture"> </span> &nbsp;上传头像
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-list"> </span> &nbsp;我的收藏
                        </a>
                    </li>
                    <li class="menu">
                        <a href="<c:url value="/logout?device=mobile"/>" class="ui-link">
                            <span class="glyphicon glyphicon-off"> </span> &nbsp;安全退出
                        </a>
                    </li>
                </ul>
            </shiro:authenticated>
        </div>
    </div>
    <div class="ui-panel-wrapper">
        <div data-role="content" class="container ui-content" role="main">
            <div class='container' style="width: 100%; margin-top: 40%;">
                <form action='<c:url value="/login"/>' class='form-signin' role='form' method="post">
                    <!-- 标识为手机端登录 -->
                    <input type="hidden" name="loginType" value="mobile" />
                    <!-- 默认不记住我 -->
                    <input type="hidden" name="rememberMe" value="off" />
        
                    <h2 class='form-signin-heading' style="text-align: center;"></h2>
                    <div class="input-group">
                        <input type='text' class='form-control' name="userName" placeholder="输入你的账户" autofocus />
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div style='height: 10px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type='password' name="password" class='form-control' placeholder="输入你的密码">
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class='checkbox'>
                        <label> <input type="checkbox" onclick="javascript:remember()" id="rememberMe">记住登录状态</label>
                    </div>
                    <span style="color: red; font-size: 14px;">${loginErrorMsg}</span>
                    <button class='btn btn-lg btn-primary btn-block' type='submit'>登录</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function remember() {
        if ($("#rememberMe").attr("checked") == "checked") {
            $("input[name='rememberMe']").val("on");
        } else {
            $("input[name='rememberMe']").val("off");
        }
    }
</script>

</body>
</html>