<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html class="ui-mobile">
<head>
    <title>会员注册</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10,IE=9,IE=8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/mobile-style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.mobile.flatui.css"/>">
    
    <script type="text/javascript" src="<c:url value="/resources/js/utils/jquery.mobile-1.4.5.js"/>"></script>
</head>
<body>
<div data-role="page">
    <div class="header linear-g navbar-fixed-top" >
        <a href="#panel-left" data-iconpos="notext" class="glyphicon glyphicon-th-large col-xs-2 text-right ui-link"></a> 
        <a class="text-center col-xs-8 ui-link">会员注册</a> 
        <a href="#panel-right" data-iconpos="notext" class="glyphicon glyphicon-user col-xs-2 text-left ui-link"></a>
    </div>
    <div data-role="panel" data-position="left" data-display="push" class="list-group shortcut_menu dn linear-g ui-panel ui-panel-position-left ui-panel-display-push ui-body-inherit ui-panel-closed" id="panel-left">
        <div class="ui-panel-inner" style="margin:10px;margin-top: 60px;">
            <a href="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/index.html#" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-home"> </span> &nbsp;首页
            </a>
            <a href="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/index.html#" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-edit"> </span> &nbsp;文章教程
            </a>
            <a href="###" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list"> </span> &nbsp;系统消息
            </a>
            <a href="###" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list-alt"> </span> &nbsp;关于网站
            </a>
        </div>
    </div>
    <div data-role="panel" data-position="right" data-display="push"
        class="user_box text-center dn linear-g ui-panel ui-panel-position-right ui-panel-display-push ui-panel-closed ui-body-inherit" id="panel-right">
        <div class="ui-panel-inner" style="margin-top: 50px;">
            <div class="u_info">
                <shiro:authenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> <span class="username"><shiro:principal/></span>
                </shiro:authenticated>
                <shiro:notAuthenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> 
                    <a href="<c:url value="/mobile/toLogin"/>"><span class="username">点此登录</span></a>
                </shiro:notAuthenticated>
            </div>
            <shiro:authenticated>
                <ul class="user_menu">
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-cog"> </span>&nbsp;个人资料
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-lock"> </span>&nbsp;修改密码
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-picture"> </span> &nbsp;上传头像
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-list"> </span> &nbsp;我的收藏
                        </a>
                    </li>
                    <li class="menu">
                        <a href="<c:url value="/logout?device=mobile"/>" class="ui-link">
                            <span class="glyphicon glyphicon-off"> </span> &nbsp;安全退出
                        </a>
                    </li>
                </ul>
            </shiro:authenticated>
        </div>
    </div>
    <div class="ui-panel-wrapper">
        <div data-role="content" class="container ui-content" role="main">
            <div class='container' style="width: 80%; margin-top: 15%;">
                <form class='form-signin' role='form' action="javascript:register()">
                    <h2 class='form-signin-heading' style="text-align: center;"></h2>
                    <div class="input-group">
                        <input type='text' class='form-control' name="userName" placeholder="输入你的用户名" autofocus />
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type='password' name="password" class='form-control' placeholder="输入你的密码">
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type='password' name="passwordOk" class='form-control' placeholder="输入你的确认密码">
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type="text" name="email" class='form-control' placeholder="输入你的邮箱">
                        <div class="input-group-addon">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                    </div>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type='text' name="phone" class='form-control' placeholder="输入你的手机号">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                    </div>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="input-group">
                        <input type='text' name="realName" class='form-control' placeholder="输入你的真实姓名">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil-square-o"></i>
                        </div>
                    </div>
                    <span style="color: red; font-size: 14px;height: 20px;">&nbsp;</span>
                    <div style='height: 30px; clear: both; display: block'></div>
                    <div class="form-group">
                    <button class='btn btn-lg btn-primary btn-block' type="submit" data-loading-text="注册中...">注册</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<input name="registerUrl" type="hidden" value="<c:url value="/register"/>"/>
<input id="toLoginUrl" type="hidden" value="<c:url value="/mobile/toLogin"/>"/>
    
<script type="text/javascript">
    function register(){
        userName = $("input[name='userName']").val();
        password = $("input[name='password']").val();
        passwordOk = $("input[name='passwordOk']").val();
        email = $("input[name='email']").val();
        phone = $("input[name='phone']").val();
        realName = $("input[name='realName']").val();
        $.ajax({
            type : "post",
            dataType : "json",
            async : false,
            url : $("input[name='registerUrl']").val(),
            data : {
                "userName" : userName,
                "password" : password,
                "passwordOk" : passwordOk,
                "email" : email,
                "phone" : phone,
                "realName" : realName
            },
            success : function(result) {
                if(result.code == 200){
                    $.model.alert("恭喜您注册成功，开始美好旅途吧^_^","",function(){
                        $(".btn-primary").button("reset");
                        window.location.href = $("#toLoginUrl").val();
                    });
                }else{
                    $.model.alert(result.msg,"",function(){
                    });
                }
            }
        });
    }
</script>    
</body>
</html>