<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html class="ui-mobile">
<head>
    <title>欢迎来到我的移动网站</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10,IE=9,IE=8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/mobile-style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery.mobile.flatui.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/mobile-index-css.css"/>">
    
    <script type="text/javascript" src="<c:url value="/resources/js/utils/jquery.mobile-1.4.5.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/utils/jquery-lazyload.js"/>"></script>
    
    <script type="text/javascript" src="<c:url value="/resources/js/mobile/index.js"/>"></script>
</head>
<body>
<div data-role="page" data-theme="">
    <div class="header linear-g navbar-fixed-top" >
        <a href="#panel-left" data-iconpos="notext" class="glyphicon glyphicon-th-large col-xs-2 text-right ui-link"></a> 
        <a class="text-center col-xs-8 ui-link">最新发布动态</a> 
        <a href="#panel-right" data-iconpos="notext" class="glyphicon glyphicon-user col-xs-2 text-left ui-link"></a>
    </div>
    <div data-role="panel" data-position="left" data-display="push" class="list-group shortcut_menu dn linear-g ui-panel ui-panel-position-left ui-panel-display-push ui-body-inherit ui-panel-closed" id="panel-left">
        <div class="ui-panel-inner" style="margin:10px;margin-top: 60px;">
            <a href="<c:url value="/mobile/index"/>" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-home"> </span> &nbsp;首页
            </a>
            <a href="<c:url value="/"/>" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-edit"> </span> &nbsp;文章教程
            </a>
            <a href="<c:url value="/mobile/index"/>" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list"> </span> &nbsp;系统消息
            </a>
            <a href="<c:url value="/mobile/index"/>" class="list-group-item ui-link">
                <span class="glyphicon glyphicon-list-alt"> </span> &nbsp;关于网站
            </a>
        </div>
    </div>
    <div data-role="panel" data-position="right" data-display="push"
        class="user_box text-center dn linear-g ui-panel ui-panel-position-right ui-panel-display-push ui-panel-closed ui-body-inherit" id="panel-right">
        <div class="ui-panel-inner" style="margin-top: 50px;">
            <div class="u_info">
                <shiro:authenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> <span class="username"><shiro:principal/></span>
                </shiro:authenticated>
                <shiro:notAuthenticated>
                    <img class="avatar" src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png" alt="头像"> 
                    <a href="<c:url value="/mobile/register"/>"><span class="username">点此登录</span></a>
                </shiro:notAuthenticated>
            </div>
            <shiro:authenticated>
                <ul class="user_menu">
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-cog"> </span>&nbsp;个人资料
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-lock"> </span>&nbsp;修改密码
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-picture"> </span> &nbsp;上传头像
                        </a>
                    </li>
                    <li class="menu">
                        <a href="###" class="ui-link">
                            <span class="glyphicon glyphicon-list"> </span> &nbsp;我的收藏
                        </a>
                    </li>
                    <li class="menu">
                        <a href="<c:url value="/logout?device=mobile"/>" class="ui-link">
                            <span class="glyphicon glyphicon-off"> </span> &nbsp;安全退出
                        </a>
                    </li>
                </ul>
            </shiro:authenticated>
        </div>
    </div>

    <div class="wrap clearfix overflow mg-auto" id="wrap">
        <!-- 左下角菜单 start -->
        <div class="info-nr">
            <div id="info-nr-phone" class="info-nr-phone">
              <section id="toMenu"></section>
              <div class="menu_01"> <a href="###"></a> </div>
              <div class="menu_02"> <a href="###"></a> </div>
              <div class="menu_03"> <a href="###"></a> </div>
              <div class="menu_04"> <a href="###"></a> </div>    
              <div class="menu_05"> <a href="###"></a> </div>    
            </div>
        </div>
        <!-- 左下角菜单 end -->
        
        
        <!-- 搜索start -->
        <!-- 
        <div class="input-group" style="width: 70%;margin-left: 2%;">
            <input type='text' class='form-control' name="userName" placeholder="根据文章名称检索" autofocus/>
            <div class="input-group-addon">
                <i class="fa fa-search"></i>
            </div>
        </div>
         -->
        <!-- 搜索end -->
        
        <div style="margin-top:60px;display: block;border-bottom: solid 0px #FFF;border-top: solid 0px #cacaca;text-indent: -9999px;height: 0px;">line</div>
        <ul id="container">
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <p>《JAVA基础教程》</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <p>《Apache+Tomcat集群环境搭建》</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        
        
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        </ul>
        <ul id="test">
            <li class="box">
                <div class="author">
                        <a href="#"><img src="http://www.17sucai.com/preview/32226/2014-03-08/bootstrap/images/avatar-1.png"></a>
                        <p class="author_name">楼主：admin</p>
                        <p class="author_time">时间：2015-05-09 16:30:25</p>
                        <a href="#" class="close"><img src="<c:url value="/resources/images/close.png"/>"></a>
                </div>
                <a href="#">
                    <div class="topic">
                        <div><img style="display: inline;" class="lazy" src="<c:url value="/resources/images/pho.jpg"/>"></div>
                        <p>三亚好好玩！三亚好好玩！</p>
                    </div>
                </a>
                <div class="click_hf">
                    <a class="zan"><img src="<c:url value="/resources/images/zan1.png"/>"></a>
                    <span>15</span>
                    <a class="huifu"><img src="<c:url value="/resources/images/huifu.png"/>"></a>
                    <a class="fenx"></a>                  
                </div>
            </li>
        </ul>
        <h1 style="text-align:center">正在加载...</h1>  
    </div>
</div>
</body>
</html>