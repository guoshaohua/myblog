<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>小周技术博客——免费在线分享平台</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <script src="<c:url value="/resources/js/utils/jqPaginator.min.js"/>"></script>
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
    <script type="text/javascript">
        //宠物加载参数
        var isindex = true;// 是否是首页，显示不同的对话内容
        var title = "";
        var visitor = "游客";
    </script>
    <link type="text/css" media="all" href="<c:url value="/resources/js/spig/spig.css"/>" rel="stylesheet" />
    <script type='text/javascript' src="<c:url value="/resources/js/spig/spig.js"/>"></script>
    
    <script type="text/javascript">
        /**
         * 根据模块id获取文章列表
         */
        function getArticleList(pageNo, pageSize) {
            location.href = $("input[name='articleListUrl']").val() + "?pageNo=" + pageNo + "&pageSize=" + pageSize + "&navigationId=";
        }
    
        /**
         * 分页请求 分页按钮 点击事件的方法实现
         * 
         * @param pageNo
         * @param pageSize
         */
        function paginationRequest(pageNo, pageSize) {
            getArticleList(pageNo, pageSize);
        }
    </script>
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod"  style="min-height: 620px;height: auto;">
        <!--内容列表-->
        <div class="col-lg-9 col-md-12 col-sm-12">
            <!-- 当前位置 -->
            <ol class="breadcrumb" style="width: 100%;font-size:14px;">
                <li class="active"><i class="fa fa-home">&nbsp;</i>当前位置：</li>
                <li><a href="javascript:window.parent.location.href='<c:url value="/index/index"/>'">首页</a></li>

                <c:forEach items="${address}" var="name" varStatus="i">
                    <li><a class="active">${name}</a></li>
                </c:forEach>
                <li>
                    <a style="color: #777; text-decoration: none;">${article.title}</a>
                </li>
            </ol>
            <!-- 当前位置   end -->
            
            <!-- 遍历文章 -->
            <c:forEach items="${pageVo.result}" var="obj" varStatus="i">
                <div class="col-lg-4 col-md-3 col-sm-4" data-am-scrollspy="{animation:'fade',delay:300,repeat:false}">
                    <c:set var="rand"><%= new java.util.Random().nextInt(20) + 1 %></c:set>
                    <c:if test="${obj.isViewOpen == 1}">
                        <a href="<c:url value="/article/articleDetail/${obj.id}"/>" target="_blank"><img src="<c:url value="/resources/images/article-bg-${rand}.png"/>"></a>
                    </c:if>
                    <c:if test="${obj.isViewOpen == 2}">
                        <a href="<c:url value="${obj.viewLink}"/>" target="_blank"><img src="<c:url value="/resources/images/article-bg-${rand}.png"/>"></a>
                    </c:if>
                    
                    <div class="cover-info">
                        <c:if test="${obj.isViewOpen == 1}">
                            <a title="${obj.title}" href="<c:url value="/article/articleDetail/${obj.id}"/>" target="_blank"><span style="font-size: 12px;" class="myTitle">${obj.title}</span></a>
                        </c:if>
                        <c:if test="${obj.isViewOpen == 2}">
                            <a title="${obj.title}" href="<c:url value="${obj.viewLink}"/>" target="_blank"><span style="font-size: 12px;" class="myTitle">${obj.title}</span></a>
                        </c:if>
                        <br/>
                        <small title="${obj.introduction}">${obj.introduction}</small>
                    </div>  
                    <div class="cover-fields">
                        <i class="fa fa-pagelines"></i> &nbsp;密码</div>
                    <div class="cover-stat">
                        <i class="fa fa-eye"></i><span class="f10"> &nbsp;27</span>
                        <i class="fa fa-heart"></i></i><span class="f10"> &nbsp;0</span>
                        <div class="cover-yh">
                            <!-- <a href="mem161473" data-container="body" data-toggle="popover" data-placement="top" data-content="永久网址：www.2b2b92b.com">
                                <i class="fa fa-user-secret"></i>
                            </a> -->
                        </div>
                    </div>
                </div>
            </c:forEach>
            <!-- end 遍历文章 -->
        </div>
        <!--end 内容列表-->
            
        <!--右侧菜单-->
        <jsp:include page="/WEB-INF/view/common/rightmenu.jsp" />
        <!--end 右侧菜单-->
        
        <!-- 分页 -->
        <ul class="pagination" id="pagination"></ul>
        <input type="hidden" id="PageCount" runat="server" value="${pageVo.totalSize}"/>
        <input type="hidden" id="PageSize" runat="server" value="${pageVo.pageSize}" />
        <input type="hidden" id="countindex" runat="server" value="${pageVo.totalPage}"/>
        <!--设置最多显示的页码数 可以手动设置 默认为10-->
        <input type="hidden" id="visiblePages" runat="server" value="10" />
        
        <script src="<c:url value="/resources/js/user/myPage.js"/>"></script>
        <input name="pageNo" id="pageNo" type="hidden" value="${pageVo.pageNo}" />
        <input name="pageSize" id="pageSize" type="hidden" value="${pageVo.pageSize}" />
        <input name="totalPage" id="totalPage" type="hidden" value="${pageVo.totalPage}" />
        <!-- end 分页 -->
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->

</body>
</html>