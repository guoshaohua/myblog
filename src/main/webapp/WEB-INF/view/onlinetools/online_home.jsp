<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${article.title}</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod">
        <!--内容列表-->
        <div class="col-lg-9 col-md-12 col-sm-12">
            <div style="width: 100%;">
                <!-- 当前位置   start -->
                <ol class="breadcrumb" style="margin-top: 10px; width: 100%;font-size: 14px;">
                    <li class="active"><i class="fa fa-home">&nbsp;</i>当前位置：</li>
                    <li><a href="javascript:location.href='<c:url value="/index/index"/>'">首页</a></li>
                    <li><a class="active" style="color: #777; text-decoration: none;">在线工具</a></li>
                </ol>
                <!-- 当前位置   end -->
                
                <section class="container panel panel-default" style="margin-top: 10px; width: 97%;border-color: #ddd;font-size:12px">
                        <!-- -------------------------------------------------- 中间内容 start --------------------------------------------------- -->
                        <div class="content-wrap">
                            <div id="mainContent">
                                <div class="column">
                                    <h2>工具分类索引</h2>
                                    <div class="tool_index">
                                        <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/apidocs" style="color: #3AB457;font-size: 16px;">常用文档</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://tool.oschina.net/apidocs/apidoc?api=jdk-zh">JDK6中文文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=jdk_7u4">JDK7英文文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=android/reference">Android文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=javaEE6">JavaEE6.0文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=Spring-3.1.1">Spring3文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=scala-docs-2.9.2">Scala文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=jquery">jQuery参考</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=php-zh">PHP中文文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=mysql-5.5-en">MySQL5.5手册</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=cpp%2Fen%2Fcpp.html">C++参考手册</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs">更多(120+)...</a></li>
                                        </ul>
                                         <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/commons" style="color: #3AB457;font-size: 16px;">常用对照表</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://tool.oschina.net/commons">HTTP Mime-type</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=2">HTML转义字符</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=3">RGB颜色参考</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=4">ASCII对照表</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=5">HTTP状态码详解</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=6">Java运算符对照</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=6#c_">C语言运算符对照</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=6#php_">PHP运算符对照</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=6#python_">Python运算符对照</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=7">TCP/UDP端口参考</a></li>
                                            <li><a href="http://tool.oschina.net/commons?type=8">网页字体参考</a></li>
                                        </ul>
                                        <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/#" style="color: #3AB457;font-size: 16px;">代码处理</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://tool.oschina.net/highlight">代码着色/高亮</a></li>
                                            <li><a href="http://tool.oschina.net/diff">代码对比/归并</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/xml">XML代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/css">CSS代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/json">JSON代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/js">JS代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/java">Java代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat/sql">SQL代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/less">LESS编译器</a></li>
                                            <li><a href="http://tool.oschina.net/markdown">MarkDown编译器</a></li>
                                            <li><a href="http://tool.oschina.net/mathml">MathML编辑</a></li>
                                        </ul>
                                        <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/#" style="color: #3AB457;font-size: 16px;">Html|Js|Css工具</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://runjs.cn/">JS在线编辑(RunJS)</a></li>
                                            <li><a href="http://tool.oschina.net/jscompress">JS代码压缩</a></li>
                                            <li><a href="http://tool.oschina.net/jscompress">CSS代码压缩</a></li>
                                            <li><a href="http://tool.oschina.net/jscompress?type=2">HTML代码压缩</a></li>
                                            <li><a href="http://tool.oschina.net/jscompress?type=3">多JS文件合并压缩</a></li>
                                            <li><a href="http://tool.oschina.net/ubb">HTML/UBB转换</a></li>
                                            <li><a href="http://tool.oschina.net/csv2tb">CSV转HTML表格</a></li>
                                            <li><a href="http://tool.oschina.net/jquery">jQuery分页插件</a></li>
                                            <li><a href="http://tool.oschina.net/jquery?type=2">jQuery语法着色</a></li>
                                            <li><a href="http://tool.oschina.net/jquery?type=3">jQuery对话框插件</a></li>
                                            <li><a href="http://tool.oschina.net/jquery?type=4">jQuery提示插件</a></li>
                                        </ul>
                                         <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/#" style="color: #3AB457;font-size: 16px;">加密/转码工具</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://tool.oschina.net/htpasswd" style="color:#A00;font-weight:bold;">[新]生成htpasswd</a></li>
                                            <li><a href="http://tool.oschina.net/encrypt?type=3">BASE64编码解码</a></li>
                                            <li><a href="http://tool.oschina.net/encrypt">加密/解密</a></li>
                                            <li><a href="http://tool.oschina.net/encrypt?type=2">散列/哈希</a></li>
                                            <li><a href="http://tool.oschina.net/encrypt?type=4">图片转BASE64编码</a></li>
                                            <li><a href="http://tool.oschina.net/hexconvert">进制转换</a></li>
                                            <li><a href="http://tool.oschina.net/encode?type=2">Native转UTF-8</a></li>
                                            <li><a href="http://tool.oschina.net/encode?type=3">Native转ASCII</a></li>
                                            <li><a href="http://tool.oschina.net/encode?type=4">URL转码</a></li>
                                            <li><a href="http://tool.oschina.net/qr">生成QrCode</a></li>
                                            <li><a href="http://tool.oschina.net/qr?type=2">QrCode解码</a></li>
                                        </ul>
                                        <ul class="nav nav-stacked">
                                            <strong><a href="http://tool.oschina.net/#" style="color: #3AB457;font-size: 16px;">其他实用工具</a></strong>
                                            <li class="divider"></li><li>
                                            </li><li><a href="http://tool.oschina.net/regex">正则表达式测试</a></li>
                                            <li><a href="http://tool.oschina.net/diff">代码对比/归并</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=postgresql9.1">Postgresql9.1文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=rails">Rails文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=hibernate-4.1.4">Hibernate文档</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs/apidoc?api=struts-2.3.4">Struts文档</a></li>
                                            <li><a href="http://tool.oschina.net/jquery">jQuery插件演示</a></li>
                                            <li><a href="http://runjs.cn/">JS/CSS编辑测试</a></li>
                                            <li><a href="http://tool.oschina.net/codeformat">代码格式化</a></li>
                                            <li><a href="http://tool.oschina.net/apidocs">文档大全</a></li>
                                            <li><a href="http://tool.oschina.net/jscompress">JS/CSS压缩</a></li>
                                        </ul>
                                    </div>
                                    <h2>常用工具</h2>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/apidocs.gif"/>" alt="在线API文档" target="#apidocs_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/apidocs">API 文档</a></div>
                                            <div class="intro">在线API文档工具提供各种语言的几十种文档，供开发者在线查阅。</div>
                                            <div class="tags">标签：<em>JDK</em>，<em>JavaEE</em>，<em>JQuery</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="apidocs_detail">
                                            <div><a href="http://tool.oschina.net/apidocs"><img src="<c:url value="/resources/online_home/apidocs.gif"/>" alt="在线API文档" width="96" class="ToolLogo"></a></div>                    
                                            <div class="t_title"><a href="http://tool.oschina.net/apidocs">API 文档</a></div>
                                            <div class="detailIntro">在线API文档工具提供各种语言的几十种在线文档，包括常见的JAVA、JQuery、Ruby等，供开发者在线查阅。</div>
                                            <div class="detailTags">标签：<em>JDK</em>，<em>JavaEE</em>，<em>JQuery</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/runjs.gif"/>" alt="RunJS" target="#runjs_detail"></div>
                                            <div class="t_title"><a href="http://runjs.cn/">RunJS</a></div>
                                            <div class="intro">RunJS，在线编辑运行HTML、CSS、JS，让JS飞一会儿！</div>
                                            <div class="tags">标签：<em>RunJS</em>，<em>JS</em>，<em>CSS</em>，<em>HTML</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="runjs_detail">
                                            <div><a href="http://runjs.cn/"><img src="<c:url value="/resources/online_home/runjs.gif"/>" alt="RunJS" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://runjs.cn/">RunJS</a></div>
                                            <div class="detailIntro">RunJS，在线编辑运行HTML、CSS、JS，让JS飞一会儿！提供代码编辑，预览，分享，Fork，主题设置等多种功能。</div>
                                            <div class="detailTags">标签：<em>RunJS</em>，<em>JS</em>，<em>CSS</em>，<em>HTML</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/alloyphoto.gif"/>" alt="alloyphoto" target="#alloyphoto_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/alloyphoto">AlloyPhoto</a></div>
                                            <div class="intro">AlloyPhoto是基于HTML5技术的开发的在线图片处理平台</div>
                                            <div class="tags">标签：<em>JavaScript</em>，<em>图片处理</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="alloyphoto_detail">
                                            <div><a href="http://tool.oschina.net/alloyphoto"><img src="<c:url value="/resources/online_home/alloyphoto.gif"/>" alt="alloyphoto" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/alloyphoto">AlloyPhoto</a></div>
                                            <div class="detailIntro">AlloyPhoto是基于HTML5技术的专业级图像处理引擎—— <a href="http://www.oschina.net/p/alloyimage">AlloyImage</a> 开发的在线图片处理平台</div>
                                            <div class="detailTags">标签：<em>JavaScript</em>，<em>图片处理</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/commons.gif"/>" alt="常用对照表" target="#commons_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/commons">常用对照表</a></div>
                                            <div class="intro">这里提供IT开发人员常用的对照表，比如ASCII，HTML转义字符，RGB颜色等。</div>
                                            <div class="tags">标签：<em>ASCII</em>，<em>HTML转义</em>，<em>RGB</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="commons_detail">
                                            <div><a href="http://tool.oschina.net/commons"><img src="<c:url value="/resources/online_home/commons.gif"/>" alt="常用对照表" width="96" class="ToolLogo"></a></div>                  
                                            <div class="t_title"><a href="http://tool.oschina.net/commons">常用对照表</a></div>
                                            <div class="detailIntro">提供IT开发人员常用的对照表，比如ASCII，HTML转义字符，RGB颜色，Http Content-type等。</div>
                                            <div class="detailTags">标签：<em>ASCII</em>，<em>HTML转义</em>，<em>RGB颜色</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/regex.gif"/>" alt="在线正则表达式测试" target="#regex_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/regex">正则表达式测试</a></div>
                                            <div class="intro">在线正则表达式匹配、替换</div>
                                            <div class="tags">标签：<em>正则表达式</em>，<em>regex</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="regex_detail">
                                            <div><a href="http://tool.oschina.net/regex"><img src="<c:url value="/resources/online_home/regex.gif"/>" alt="在线正则表达式测试" width="96" class="ToolLogo"></a></div>                  
                                            <div class="t_title"><a href="http://tool.oschina.net/regex">正则表达式测试</a></div>
                                            <div class="detailIntro">在线正则表达式匹配、替换，另有多种常用正则表达式提供方便使用</div>
                                            <div class="detailTags">标签：<em>正则表达式</em>，<em>regex</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/encrypt.gif"/>" alt="在线加密/解密，散列/哈希" target="#encrypt_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/encrypt">加密/解密，哈希</a></div>
                                            <div class="intro">这个小工具提供在线文本加密/解密，散列，提供BASE64、SHA1、MD5、AES等多种算法</div>
                                            <div class="tags">标签：<em>SHA1</em>，<em>MD5</em>，<em>AES</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="encrypt_detail">
                                            <div><a href="http://tool.oschina.net/encrypt"><img src="<c:url value="/resources/online_home/encrypt.gif"/>" alt="在线加密/解密，散列/哈希" width="96" class="ToolLogo"></a></div>                  
                                            <div class="t_title"><a href="http://tool.oschina.net/encrypt">加密/解密，散列/哈希</a></div>
                                            <div class="detailIntro">这个小工具提供在线文本加密/解密，散列，提供SHA1、MD5、HMAC、AES、RC4、Rabbit、Base64等多种算法，采用开源的Crypto-JS库实现。</div>
                                            <div class="detailTags">标签：<em>SHA1</em>，<em>MD5</em>，<em>AES</em>，<em>DES</em>，<em>Base64</em></div>
                                        </div>
                                    </div>  
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/qr.gif"/>" alt="在线生成 QR Code" target="#qr_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/qr">生成二维条形码</a></div>
                                            <div class="intro">生成生成QR码，能够提供多参数设置生成QR码，可以对输出图像格式（GIF/JPEG/PNG），纠错级别，类型，边缘留白（Margin），原胞大小进行设置</div>
                                            <div class="tags">标签：<em>QR</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="qr_detail">
                                            <div><a href="http://tool.oschina.net/qr"><img src="<c:url value="/resources/online_home/qr.gif"/>" alt="在线生成 QR Code" width="96" class="ToolLogo"></a></div>                 
                                            <div class="t_title"><a href="http://tool.oschina.net/qr">生成二维条形码</a></div>
                                            <div class="detailIntro">生成生成QR码，能够提供多参数设置生成QR码，可以对输出图像格式（GIF/JPEG/PNG），纠错级别，类型，边缘留白（Margin），原胞大小进行设置</div>
                                            <div class="detailTags">标签：<em>QR</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/codeformat.gif"/>" alt="在线代码格式化" target="#codeformat_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/codeformat">代码格式化</a></div>
                                            <div class="intro">该工具提供对Java、JavaScript、CSS、HTML、JSON、SQL的代码格式化功能。</div>
                                            <div class="tags">标签：<em>Java</em>，<em>HTML</em>，<em>SQL</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="codeformat_detail">
                                            <div><a href="http://tool.oschina.net/codeformat"><img src="<c:url value="/resources/online_home/codeformat.gif"/>" alt="在线代码格式化" width="96" class="ToolLogo"></a></div>                  
                                            <div class="t_title"><a href="http://tool.oschina.net/codeformat">代码格式化</a></div>
                                            <div class="detailIntro">该工具提供对Java、JavaScript、CSS、HTML、JSON、SQL的代码格式化功能。</div>
                                            <div class="detailTags">标签：<em>Java</em>，<em>HTML</em>，<em>JavaScript</em>，<em>格式化</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/highlight.gif"/>" alt="在线代码着色/高亮" target="#highlight_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/highlight">代码着色/高亮</a></div>
                                            <div class="intro">此工具可以提供在线将代码关键字着色、加行号等功能，并提供输出HTML.</div>
                                            <div class="tags">标签：<em>代码着色</em>，<em>代码高亮</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="hexconvert_detail">
                                            <div><a href="http://tool.oschina.net/highlight"><img src="<c:url value="/resources/online_home/highlight.gif"/>" alt="在线代码着色/高亮" width="96" class="ToolLogo"></a></div>                  
                                            <div class="t_title"><a href="http://tool.oschina.net/highlight">代码着色/高亮</a></div>
                                            <div class="detailIntro">此工具可以提供在线将代码关键字着色、加行号等功能，并提供输出HTML，支持常见的20多种编程语言，方便在做代码演示的时候使用。</div>
                                            <div class="detailTags">标签：<em>代码着色</em>，<em>代码高亮</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/jscompress.gif"/>" alt="在线JS/CSS压缩" target="#jscompress_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/jscompress">JS/CSS压缩</a></div>
                                            <div class="intro">用户可以通过该工具在线压缩Javascript和CSS代码。</div>
                                            <div class="tags">标签：<em>Javascript</em>，<em>CSS</em>，<em>压缩</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="jscompress_detail">
                                            <div><a href="http://tool.oschina.net/jscompress"><img src="<c:url value="/resources/online_home/jscompress.gif"/>" alt="在线JS/CSS压缩" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/jscompress">JS/CSS压缩</a></div>
                                            <div class="detailIntro">用户可以通过该工具在线压缩Javascript和CSS代码，此工具使用的是YUICompressor引擎。</div>
                                            <div class="detailTags">标签：<em>Javascript</em>，<em>CSS</em>，<em>压缩</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/encode.gif"/>" alt="在线编码转换" target="#encode_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/encode">编码转换</a></div>
                                            <div class="intro">该工具为开发者提供Native到Unicode、ASCII的编码转换</div>
                                            <div class="tags">标签：<em>ASCII</em>，<em>Unicode</em>，<em>编码</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="encode_detail">
                                            <div><a href="http://tool.oschina.net/encode"><img src="<c:url value="/resources/online_home/encode.gif"/>" alt="在线编码转换" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/encode">编码转换</a></div>
                                            <div class="detailIntro">该工具为开发者提供Native到Unicode、ASCII的编码转换</div>
                                            <div class="detailTags">标签：<em>ASCII</em>，<em>Unicode</em>，<em>编码</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/hexconvert.gif"/>" alt="在线进制转换" target="#hexconvert_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/hexconvert">不同进制转换</a></div>
                                            <div class="intro">此工具可以实现2~36进制之间任意进制转换，支持浮点型。</div>
                                            <div class="tags">标签：<em>二进制</em>，<em>十六进制</em>，<em>进制转换</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="hexconvert_detail">
                                            <div><a href="http://tool.oschina.net/hexconvert"><img src="<c:url value="/resources/online_home/hexconvert.gif"/>" alt="在线进制转换" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/hexconvert">不同进制转换</a></div>
                                            <div class="detailIntro">此工具可以实现2~36进制之间任意进制转换，支持浮点型。</div>
                                            <div class="detailTags">标签：<em>二进制</em>，<em>十六进制</em>，<em>进制转换</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/mathml.gif"/>" alt="在线MathML编辑测试" target="#mathml_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/mathml">MathML编辑测试</a></div>
                                            <div class="intro">该工具提供对MathML、LaTeX、数学公式的在线编辑，生成图片功能。</div>
                                            <div class="tags">标签：<em>MathML</em>，<em>LaTeX</em>，<em>数学公式</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="mathml_detail">
                                            <div><a href="http://tool.oschina.net/mathml"><img src="<c:url value="/resources/online_home/mathml.gif"/>" alt="在线MathML编辑测试" width="96" class="ToolLogo"></a></div>                 
                                            <div class="t_title"><a href="http://tool.oschina.net/mathml">MathML编辑测试</a></div>
                                            <div class="detailIntro">该工具（MathML Edit）提供对MathML、LaTeX、数学公式等的在线编辑，生成图片功能。</div>
                                            <div class="detailTags">标签：<em>MathML</em>，<em>LaTeX</em>，<em>数学公式</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/less.gif"/>" alt="在线LESS编译器" target="#less_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/less">LESS编译器</a></div>
                                            <div class="intro">这个工具可以在线将您的LESS代码编译成CSS代码，方便前端人员使用</div>
                                            <div class="tags">标签：<em>Less</em>，<em>CSS</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="less_detail">
                                            <div><a href="http://tool.oschina.net/less"><img src="<c:url value="/resources/online_home/less.gif"/>" alt="在线Less编译器" width="96" class="ToolLogo"></a></div>                    
                                            <div class="t_title"><a href="http://tool.oschina.net/less">LESS编译器</a></div>
                                            <div class="detailIntro">这个工具可以在线将您的LESS代码编译成CSS代码，方便前端人员使用</div>
                                            <div class="detailTags">标签：<em>Less</em>，<em>CSS</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/markdown.gif"/>" alt="在线MarkDown编译器" target="#markdown_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/markdown">MarkDown编译器</a></div>
                                            <div class="intro">这个工具可以帮助开发者将web上的文本转换成HTML文档。</div>
                                            <div class="tags">标签：<em>HTML</em>，<em>Markdown</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="markdown_detail">
                                            <div><a href="http://tool.oschina.net/markdown"><img src="<c:url value="/resources/online_home/markdown.gif"/>" alt="在线MarkDown编译器" width="96" class="ToolLogo"></a></div>                    
                                            <div class="t_title"><a href="http://tool.oschina.net/markdown">MarkDown编译器</a></div>
                                            <div class="detailIntro">Markdown 是一个 Web 上使用的文本到HTML的转换工具，可以通过简单、易读易写的文本格式生成结构化的HTML文档。</div>
                                            <div class="detailTags">标签：<em>HTML</em>，<em>Markdown</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/ubb.gif"/>" alt="在线UBB/HTML转换" target="#ubb_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/ubb">UBB/HTML转换</a></div>
                                            <div class="intro">此工具可以方便开发者在UBB和HTML之间互转。</div>
                                            <div class="tags">标签：<em>UBB</em>，<em>HTML</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="ubb_detail">
                                            <div><a href="http://tool.oschina.net/ubb"><img src="<c:url value="/resources/online_home/ubb.gif"/>" alt="在线UBB/HTML转换" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/ubb">UBB/HTML转换</a></div>
                                            <div class="detailIntro">UBB是一种网页中的替代HTML代码的安全代码，他具有少量的文本格式，例如加粗、颜色等，此工具方便UBB/HTML互转。</div>
                                            <div class="detailTags">标签：<em>UBB</em>，<em>HTML</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool mr20">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/csv2tb.gif"/>" alt="在线CSV转HTML表格" target="#csv2tb_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/csv2tb">CSV转HTML表格</a></div>
                                            <div class="intro">该工具提供在线CSV转换为HTML的表格，能够快速的将CSV数据转换页面展示数据。</div>
                                            <div class="tags">标签：<em>CSV</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="csv2tb_detail">
                                            <div><a href="http://tool.oschina.net/csv2tb"><img src="<c:url value="/resources/online_home/csv2tb.gif"/>" alt="在线CSV转HTML表格" width="96" class="ToolLogo"></a></div>                 
                                            <div class="t_title"><a href="http://tool.oschina.net/csv2tb">CSV转HTML表格</a></div>
                                            <div class="detailIntro">该工具提供在线CSV转换为HTML的表格，能够快速的将CSV数据转换页面展示数据。</div>
                                            <div class="detailTags">标签：<em>CSV</em></div>
                                        </div>
                                    </div>
                                    <div class="latestTool" style="margin-bottom: 50px;">
                                        <div class="toolDescription">
                                            <div><img src="<c:url value="/resources/online_home/jquery.gif"/>" alt="jQuery在线插件演示大全" target="#jquery_detail"></div>
                                            <div class="t_title"><a href="http://tool.oschina.net/jquery">jQuery插件演示大全</a></div>
                                            <div class="intro">该在线工具提供大量常用jQuery插件的演示，为您节省插件选择的时间，并提供在线对示例进行编辑。</div>
                                            <div class="tags">标签：<em>jquery</em>，<em>插件</em></div>
                                        </div>
                                        <div class="detail boxShadow" style="display:none;" id="jquery_detail">
                                            <div><a href="http://tool.oschina.net/jquery"><img src="<c:url value="/resources/online_home/jquery.gif"/>" alt="jQuery在线插件演示大全" width="96" class="ToolLogo"></a></div>                   
                                            <div class="t_title"><a href="http://tool.oschina.net/jquery">jQuery插件演示大全</a></div>
                                            <div class="detailIntro">该在线工具提供大量常用jQuery插件的演示，为您节省插件选择的时间，并提供在线对示例进行编辑。</div>
                                            <div class="detailTags">标签：<em>jquery</em>，<em>插件</em></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------- 中间内容 end --------------------------------------------------- -->
                    </section>
            </div>
        </div>
        <!--end 内容列表-->
            
        <!--右侧菜单-->
        <jsp:include page="/WEB-INF/view/common/rightmenu.jsp" />
        <!--end 右侧菜单-->
        
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->
<script type="text/javascript">
    $(document).ready(function(){
        $(".latestTool").mouseover(function(){
            $(".detail").hide();
            var detail = $(this).find(".detail");
            $(this).addClass("toolActive");
            detail.show();
            $(this).mouseout(function(){
                detail.hide();
                $(".toolActive").removeClass("toolActive");
            });
        });
    });
</script>
<style>
    .tool_index ul{
        display:inline-block;
        margin:10px 9px 10px 9px;
        padding:0 0 0 0;
        width:164px;
    }
    .tool_index ul strong{margin-left:0px;}
    .nav-stacked > li > a{
        padding: 10px 0px;
    }
</style>
<link rel="stylesheet" href="<c:url value="/resources/online_home/basic.css"/>" type="text/css">
</body>
</html>