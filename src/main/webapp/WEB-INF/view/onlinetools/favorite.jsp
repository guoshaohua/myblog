<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${article.title}</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod">
        <!--内容列表-->
        <div class="col-lg-9 col-md-12 col-sm-12">
            <div style="width: 100%;">
                <!-- 当前位置   start -->
                <ol class="breadcrumb" style="width: 100%;font-size: 14px;">
                    <li class="active"><i class="fa fa-home">&nbsp;</i>当前位置：</li>
                    <li><a href="javascript:location.href='<c:url value="/index/index"/>'">首页</a></li>
                    <li><a href="javascript:location.href='<c:url value="/article/getArticleList?navigationId=55ffa1ba9ff1e8de3e6cc348"/>'">网站收藏</a></li>
                </ol>
                <!-- 当前位置   end -->
                
                <section class="container panel panel-default" style="margin-top: 10px; width: 97%;border-color: #ddd;">
                        <!-- -------------------------------------------------- 中间内容 start --------------------------------------------------- -->
                        <div class="content-wrap">
                            <div class="content" style="margin-top: 40px;">
                                <div class="hot-posts">
                                    <h3 style="text-align: center; max-width: 100%;">
                                        <i class="fa fa-quote-left"></i>
                                        <span style="padding-left: 15px; padding-right: 10px;font-size: 20px;">不错的网站博客</span>
                                        <i class="fa fa-quote-right"></i>
                                    </h3>
                                    <br>
                                </div>

                                <div style="width: 100%; margin-left: 0px; height: auto; font-size: 12px;">
                                    <div style="width: 100%; color: green; font-size: 14px; font-weight: bold;">
                                        <table width="100%" border="0px">
                                            <tr>
                                                <td width="50%" style="font-weight: bold;font-size: 18px;text-align: center;">网站描述</td>
                                                <td width="50%" style="font-weight: bold;font-size: 18px;text-align: center;">网址</td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">Bootstrap中文网开源项目免费 CDN 服务</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.bootcdn.cn/" target="_blank">http://www.bootcdn.cn/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">Bootstrap Table 中文实例、文档</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://wenzhixin.net.cn/p/bootstrap-table/docs/examples.html" target="_blank">http://wenzhixin.net.cn/p/bootstrap-table/docs/examples.html</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">Bootstrap日期和时间表单组件</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.bootcss.com/p/bootstrap-datetimepicker/index.htm" target="_blank">http://www.bootcss.com/p/bootstrap-datetimepicker/index.htm</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">在线工具 —— 开源中国社区</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://tool.oschina.net/" target="_blank">http://tool.oschina.net/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">H-ui 前端框架</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://test.h-ui.net/index.shtml" target="_blank">http://test.h-ui.net/index.shtml</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">jQuery UI Datepicker-日期选择控件(可对时分单独选择)</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://trentrichardson.com/examples/timepicker/#tp-examples" target="_blank">http://trentrichardson.com/examples/timepicker/#tp-examples</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">赞生博客</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.zan3.com/" target="_blank">http://www.zan3.com/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">jQuery之家-自由分享jQuery、html5、css3的插件库</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.htmleaf.com/" target="_blank">http://www.htmleaf.com/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">Amaze UI-轻量级前端框架</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://amazeui.org/getting-started" target="_blank">http://amazeui.org/getting-started</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">SOYEP - 关注前端设计，关注互联网</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.soyep.net/" target="_blank">http://www.soyep.net/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">百度编辑器-UEditor</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://fex.baidu.com/ueditor/" target="_blank">http://fex.baidu.com/ueditor/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">欲思博客- 关注前端和WordPress,分享福利和心得</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://yusi123.com/" target="_blank">http://yusi123.com/</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">好库网-免费svn</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.okbase.net" target="_blank">http://www.okbase.net</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;">51linux.net - 免费linux服务器</td>
                                                <td style="font-weight: bold;text-align: center;"><a href="http://www.51linux.net" target="_blank">http://www.51linux.net</a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;"></td>
                                                <td style="font-weight: bold;text-align: center;"><a href="" target="_blank"></a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;"></td>
                                                <td style="font-weight: bold;text-align: center;"><a href="" target="_blank"></a></td>
                                            </tr>
                                            <tr style="height: 30px;">
                                                <td style="color: green;font-weight: bold;text-align: center;"></td>
                                                <td style="font-weight: bold;text-align: center;"><a href="" target="_blank"> </a></td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------- 中间内容 end --------------------------------------------------- -->
                    </section>
            </div>
        </div>
        <!--end 内容列表-->
            
        <!--右侧菜单-->
        <jsp:include page="/WEB-INF/view/common/rightmenu.jsp" />
        <!--end 右侧菜单-->
        
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->

</body>
</html>