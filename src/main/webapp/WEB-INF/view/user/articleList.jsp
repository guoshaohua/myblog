<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>文章列表</title>
        <%@ include file="/WEB-INF/view/common/pace.jsp"%>
        <link type="text/css" media="all" href="<c:url value="/resources/js/spig/spig.css"/>" rel="stylesheet" />
        <script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.min.js"/>"></script>
        <script type='text/javascript' src="<c:url value="/resources/js/user/articleList.js"/>"></script>
        <script type='text/javascript' src="<c:url value="/resources/js/spig/spig.js"/>"></script>
    </head>
<body>

    <!-- 导航栏	start -->
    <jsp:include page="/WEB-INF/view/common/header.jsp" />
    <!-- 导航栏	end -->
    
    <!-- 动画 start -->
    <div role="main" class="site clearfix">
        <div class="context-loader-container" id="site-container">
            <section class="aws-section" id="aws">
                <div class="leaf-header-container">
                    <div class="bg-animation">
                        <img width="300" src="<c:url value="/resources/images/cloud-1.png"/>" class="cloud cloud-1" alt="Cloud-1"/> 
                        <img width="347" src="<c:url value="/resources/images/cloud-2.png"/>" class="cloud cloud-2" alt="Cloud-2"/> 
                        <img width="50" src="<c:url value="/resources/images/corel.png"/>" class="cloud corel" alt="corel"/> 
                        <img width="470" src="<c:url value="/resources/images/cloud-3.png"/>" class="cloud cloud-3" alt="Cloud-3"/> 
                        <img width="762" src="<c:url value="/resources/images/cloud-4.png"/>" class="cloud cloud-4" alt="Cloud-4"/> 
                        <img width="587" src="<c:url value="/resources/images/cloud-5.png"/>" class="cloud cloud-5" alt="Cloud-5"/> 
                        <img width="857" src="<c:url value="/resources/images/cloud-6.png"/>" class="cloud cloud-6" alt="Cloud-6"/>
                    </div>
                    <div class="htmleaf-header-content">
                        <h2>J2EE技术分享<br> 自由分享 源码、Demo下载、高级技术实例教程</h2>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- 动画 end -->
    
    
    <div id="wrap">
        <div id="container">
            <div id="left_side">左侧内容</div>
            <div id="content">

                <!-- 中间内容   start -->
                <div style="width: 100%;">
                    <!-- 所在位置   start -->
                    <ol class="breadcrumb" style="margin-top: 10px; width: 100%;">
                        <li class="active"><i class="fa fa-home">&nbsp;</i>当前位置：</li>
                        <li><a href="javascript:location.href='<c:url value="/index/index"/>'">首页</a></li>
                        <c:forEach items="${articleQo.parentNames}" var="obj" varStatus="i">
                            <c:if test="${i.count != fn:length(articleQo.parentNames)}">
                                <li><a class="active">${obj}</a></li>
                            </c:if>
                            <c:if test="${i.count == fn:length(articleQo.parentNames)}">
                                <li>
                                    <a class="active" style="color: #777; text-decoration: none;">${obj}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                    </ol>
                    <!-- 所在位置   end -->

                    <div class="container-fluid" style="width: 100%; height: auto;">
                        <div class="row">
                            <div class="col-xs-12">
                                <c:forEach items="${pageVo.result}" var="obj" varStatus="i">
                                    <!-- 
                                    <div <c:if test="${i.index % 6 == 0}">class="panel panel-success"</c:if>
                                         <c:if test="${i.index % 6 == 1}">class="panel panel-info"</c:if>
                                         <c:if test="${i.index % 6 == 2}">class="panel panel-primary"</c:if>
                                         <c:if test="${i.index % 6 == 3}">class="panel panel-warning"</c:if>
                                         <c:if test="${i.index % 6 == 4}">class="panel panel-danger"</c:if>
                                         <c:if test="${i.index % 6 == 5}">class="panel panel-default"</c:if>> -->
                                    <div class="plugthumb">
                                        <div class="tp">
                                            <figure class="effect-bubba">
                                                <c:if test="${i.index%7 == 0}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411291537.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 1}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411141553.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 2}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411151553.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 3}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411171424.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 4}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411261511.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 5}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201411171555.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <c:if test="${i.index%7 == 6}">
                                                    <img width="250" height="158" data-original="<c:url value="/resources/images/201412081645.jpg"/>" src="<c:url value="/resources/images/loading.gif"/>" class="lazy">
                                                </c:if>
                                                <figcaption>
                                                    <p>
                                                        <i class="fa fa-heart-o"></i>1314
                                                    </p>
                                                    <p>
                                                        <i class="fa fa-leaf"></i>JAVA技术分享
                                                    </p>
                                                    <p>www.2b2b92b.com</p>
                                                </figcaption>
                                            </figure>
                                        </div>
                                        <div class="plug-info">
                                            <div class="plug-title">
                                                <c:if test="${obj.isViewOpen == 1}">
                                                    <a target="_blank" href="<c:url value="/article/articleDetail/${obj.id}"/>"><b>${obj.title}</b></a>
                                                </c:if>
                                                <c:if test="${obj.isViewOpen == 2}">
                                                    <a target="_blank" href="<c:url value="${obj.viewLink}"/>"><b>${obj.title}</b></a>
                                                </c:if>
                                            </div>
                                            <div class="plug-description">
                                                ${obj.introduction}
                                            </div>
                                        </div>
                                        <div class="plug-mesg">
                                            <ul>
                                                <li>
                                                    <a href="###">
                                                    <img width="24" height="24" src="<c:url value="/resources/images/css3-32.png"/>"></a></li>
                                            </ul>
                                            <span class="pull-right" style="margin-top: -10px;"><i class="fa fa-clock-o">&nbsp;</i><fmt:formatDate value="${obj.createTime}" pattern="yyyy-MM-dd hh:mm:ss"/></span>
                                        </div>
                                    </div>
                                    
                                    <!-- 
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h2 class="panel-title" id="panel-title" style="padding-top: 50px;">
                                                <span class="label label-success" style="vertical-align: middle;">原创</span>
                                                <a target="_blank" style="margin-left: 10px;" href="<c:url value="/article/articleDetail/${obj.id}"/>">${obj.title}</a>
                                            </h2>
                                        </div>
                                        <div class="panel-body" style="height: 100px;">
                                            <span style="vertical-align: middle;">${obj.introduction}</span>
                                        </div>
                                    </div>
                                     -->
                                </c:forEach>
                            </div>
                        </div>
                        
                        <!-- 分页按钮    start -->
                        <jsp:include page="/WEB-INF/view/common/pagination.jsp"></jsp:include>
                        <!-- 分页按钮    end -->
                        
                    </div>
                </div>
                <!-- 中间内容   end -->

            </div>
            <div id="right_side">右侧内容</div>
        </div>
        <!-- <div id="footer">footer</div> -->
    </div>


    <input id="navigationId" type="hidden" value="${articleQo.navigationId}" />
    <input name="pageNo" type="hidden" value="${pageVo.pageNo}" />
    <input name="pageSize" type="hidden" value="${pageVo.pageSize}" />
    <input name="totalPage" type="hidden" value="${pageVo.totalPage}" />
</body>
</html>