<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=10,IE=9,IE=8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="小周博客，技术分享">
    <title>个人中心</title>
    <%@ include file="/WEB-INF/view/common/common.jsp"%>
    <script src="<c:url value='/resources/js/member/center.js'/>"></script>
    
    <style type="text/css">
        .nav-pills li a{
            padding: 15px 15px;
        }
    </style>
    <style type="text/css">
        /* Custom Styles */
        ul.nav-tabs{
            width: 140px;
            margin-top: 20px;
            border-radius: 4px;
            border: 1px solid #ddd;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067);
        }
        ul.nav-tabs li{
            margin: 0;
            border-top: 1px solid #ddd;
        }
        ul.nav-tabs li:first-child{
            border-top: none;
        }
        ul.nav-tabs li a{
            margin: 0;
            padding: 8px 16px;
            border-radius: 0;
        }
        ul.nav-tabs li.active a, ul.nav-tabs li.active a:hover{
            color: #fff;
            background: #0088cc;
            border: 1px solid #0088cc;
        }
        ul.nav-tabs li:first-child a{
            border-radius: 4px 4px 0 0;
        }
        ul.nav-tabs li:last-child a{
            border-radius: 0 0 4px 4px;
        }
        ul.nav-tabs.affix{
            top: 30px;
        }
    </style>
</head>

<!--  data-spy="scroll" data-target="#myScrollspy" -->
<body>
    
    <!-- 导航栏    start -->
    <nav class="navbar navbar-default navbar-fixed-top"
        role="navigation">
        <div class="container" style="width: 100%;">
            <div class="navbar-header">

                <!-- 响应式   start-->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                    <span class="sr-only"></span> <span class="icon-bar"></span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <!-- 响应式   start-->
            </div>

            <div id="navbar" class="navbar-collapse collapse" style="margin-left: 10%;">
                <!-- 导航菜单   start-->
                <ul class="nav navbar-nav">
                    <li><a>Welcome</a></li>
                </ul>
                <!-- 导航菜单    end-->
                <div class="navbar-form navbar-right" role="form">
                    <button type="button" onclick="javascript:location.href='<c:url value="/index/index"/>'" class="btn btn-success"><i class="fa fa-home"></i>首页</button>
                </div>
                <!-- 导航菜单    end-->
                <div class="navbar-form navbar-right" role="form" style="margin-right: -20px;">
                    <button type="button" onclick="javascript:location.href='<c:url value="/logout"/>'" class="btn btn-success"><i class="fa fa-power-off"></i>注销</button>
                </div>
            </div>
        </div>
    </nav>
    <!-- 导航栏   end -->
    
    <div id="wrap">
        <div id="container">
            <div id="left_side" style="width: 15%;left: 20px;">
                <div class="row">
                    <div class="col-xs-10" id="myScrollspy">
                        <ul class="nav nav-tabs nav-stacked" data-spy="affix" data-offset-top="100">
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content" style="width: 80%;height: auto;">

                <!-- 中间内容   start -->
                <div style="width: 100%;">
                    <iframe name="memberFrame" src="<c:url value="/notice"/>" height="1000" width="100%" frameborder="0"></iframe>
                </div>
                <!-- 中间内容   end -->

            </div>
            <!-- <div id="right_side" style="width: 8%;">右侧部分</div> -->
        </div>
        <!-- <div id="footer">footer</div> -->
    </div>
    
    <!-- 获取菜单列表url -->
    <input type="hidden" name="getMenuListUrl" value="<c:url value="/menu/getMenuList"/>"/>
    
    <!-- 项目根路径 -->
    <input type="hidden" name="projectUrl" value="<c:url value=""/>"/>
</body>
</html>