<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${article.title}</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod" style="min-height: 620px;height: auto;">
        <!--内容列表-->
        <div class="col-lg-9 col-md-12 col-sm-12">
            <div style="width: 100%;">
                <!-- 当前位置   start -->
                <ol class="breadcrumb" style="width: 100%;font-size: 14px;">
                    <li class="active"><i class="fa fa-home">&nbsp;</i>当前位置：</li>
                    <li><a href="javascript:window.parent.location.href='<c:url value="/index/index"/>'">首页</a></li>
    
                    <c:forEach items="${address}" var="name" varStatus="i">
                        <li><a class="active">${name}</a></li>
                    </c:forEach>
                    <li>
                        <a style="color: #777; text-decoration: none;">${article.title}</a>
                    </li>
                </ol>
                <!-- 当前位置   end -->
                
                <section class="container panel panel-default" style="margin-top: 10px; width: 97%;border-color: #ddd;">
                    <div class="content-wrap">
                        <div class="content" style="margin-top: 40px;">
                            <div class="hot-posts">
                                <h3 style="text-align: center; max-width: 100%;">
                                    <i class="fa fa-quote-left"></i>
                                    <span style="padding-left: 15px; padding-right: 10px;font-size: 20px;">${article.title}</span>
                                    <i class="fa fa-quote-right"></i>
                                </h3>
                                <br>
                            </div>
    
                            <div style="width: 100%; margin-left: 0px; height: auto;font-size: 12px;">${article.content}</div>
    
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!--end 内容列表-->
            
        <!--右侧菜单-->
        <jsp:include page="/WEB-INF/view/common/rightmenu.jsp" />
        <!--end 右侧菜单-->
        
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->

</body>
</html>