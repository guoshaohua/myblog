<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <script type='text/javascript' src="<c:url value="/resources/js/utils/jquery.min.js"/>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            window.parent.location.href = "<c:url value="/toLogin"/>";
        });
    </script>
</head>
<body>

</body>
</html>