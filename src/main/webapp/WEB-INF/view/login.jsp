<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>会员登录</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
    <script type="text/javascript">
        function remember() {
            if ($("#rememberMe").attr("checked") == "checked") {
                $("input[name='rememberMe']").val("on");
            } else {
                $("input[name='rememberMe']").val("off");
            }
        }
    </script>
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod" style="min-height: 610px;height: auto;">
        <div class='container' style="width: 30%; margin-top: 10px;">
            <form action='<c:url value="/LoginServlet"/>' class='form-signin' role='form' method="get">
                <!-- 默认不记住我 -->
                <input type="hidden" name="rememberMe" value="off" />
    
                <h2 class='form-signin-heading' style="text-align: center;"><i class="fa fa-smile-o"></i>&nbsp;网站登录</h2>
                <div class="input-group">
                    <input type='text' class='form-control' name="userName" placeholder="输入你的账户" required autofocus />
                    <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type='password' name="password" class='form-control' placeholder="输入你的密码" required>
                    <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </div>
                </div>
                <div class='checkbox'>
                    <label style="font-size: 12px;"> <input type="checkbox" onclick="javascript:remember()" id="rememberMe">记住登录状态</label>
                </div>
                <span style="color: red; font-size: 14px;height: 30px;">${loginErrorMsg}</span>
                <div style='height: 15px; clear: both; display: block'></div>
                <button class='btn btn-lg btn-primary btn-block' type='submit'>登录</button>
                <div style="text-align: right; margin-top: 25px">
                    <a onclick="javascript:location.href='<c:url value="/qqLogin"/>'" href="javascript:;">
                        <img border="0" alt="QQ登录" src="<c:url value="/resources/images/qqlogin.png"/>">
                    </a>
                </div>
            </form>
        </div>
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->

</body>
</html>