<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>会员注册</title>

    <link href="http://www.jq22.com/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" media="screen">
    
    <link type="text/css" media="all" href="<c:url value="/resources/css/my.css"/>" rel="stylesheet" />
    <script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src='http://www.jq22.com/js/bootstrap.min.js'></script>
    
    <link href="<c:url value="/resources/css/myPage.css"/>" rel="stylesheet">
    
    <%@ include file="/WEB-INF/view/common/pace.jsp"%>
    
    <script type="text/javascript">
        function remember() {
            if ($("#rememberMe").attr("checked") == "checked") {
                $("input[name='rememberMe']").val("on");
            } else {
                $("input[name='rememberMe']").val("off");
            }
        }
    </script>
</head>
<body data-spy="scroll" data-target=".navbar-example">
<!-- 导航栏    start -->
<jsp:include page="/WEB-INF/view/common/header2.jsp" />
<!-- 导航栏    end -->

<!--主体-->
<div class="container-fluid m" id="zt">  
    <div class="container m0 bod" style="min-height: 610px;height: auto;">
        <div class='container' style="width: 30%; margin-top: 10px;">
            <form class='form-signin' role='form' action="javascript:register()">
                <h2 class='form-signin-heading' style="text-align: center;">会员注册</h2>
                <div class="input-group" style="height: 40px;">
                    <input type='text' class='form-control' name="userName" placeholder="输入你的用户名" required autofocus />
                    <div class="input-group-addon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type='password' name="password" class='form-control' placeholder="输入你的密码" required>
                    <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type='password' name="passwordOk" class='form-control' placeholder="输入你的确认密码" required>
                    <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type="text" name="email" class='form-control' placeholder="输入你的邮箱" required>
                    <div class="input-group-addon">
                        <i class="fa fa-envelope-o"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type='text' name="phone" class='form-control' placeholder="输入你的手机号">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                </div>
                <div style='height: 30px; clear: both; display: block'></div>
                <div class="input-group">
                    <input type='text' name="realName" class='form-control' placeholder="输入你的真实姓名">
                    <div class="input-group-addon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                </div>
                <span style="color: red; font-size: 14px;height: 10px;">&nbsp;</span>
                <div style='height: 10px; clear: both; display: block'></div>
                <button class='btn btn-lg btn-primary btn-block' type="submit" data-loading-text="注册中...">注册</button>
            </form>
        </div>
    </div>  
</div>
<!--end主体-->

<!-- 底部   start -->
<jsp:include page="/WEB-INF/view/common/footer.jsp" />
<!-- 底部    end -->

<input name="registerUrl" type="hidden" value="<c:url value="/register"/>"/>
<input id="toLoginUrl" type="hidden" value="<c:url value="/toLogin"/>"/>
<script type="text/javascript">
    function register(){
        $(".btn-primary").button("loading");
        
        userName = $("input[name='userName']").val();
        password = $("input[name='password']").val();
        passwordOk = $("input[name='passwordOk']").val();
        email = $("input[name='email']").val();
        phone = $("input[name='phone']").val();
        realName = $("input[name='realName']").val();
        $.ajax({
            type : "post",
            dataType : "json",
            async : false,
            url : $("input[name='registerUrl']").val(),
            data : {
                "userName" : userName,
                "password" : password,
                "passwordOk" : passwordOk,
                "email" : email,
                "phone" : phone,
                "realName" : realName
            },
            success : function(result) {
                if(result.code == 200){
                    $.model.alert("恭喜您注册成功，开始美好旅途吧^_^","",function(){
                        $(".btn-primary").button("reset");
                        window.location.href = $("#toLoginUrl").val();
                    });
                }else{
                    $.model.alert(result.msg,"",function(){
                        $(".btn-primary").button("reset");
                    });
                }
            }
        });
    }
</script>
</body>
</html>