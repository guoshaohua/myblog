<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html>
    <head>
        <title>技术教程</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=10,IE=9,IE=8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="keywords" content="小周博客，技术分享">
    </head>
<body class="home blog">

    <!-- 导航栏	start -->
    <jsp:include page="/WEB-INF/view/common/header.jsp" />
    <!-- 导航栏	end -->


    <div id="wrap">
        <div id="container">
            <div id="left_side" style="width: 8%;">left_side</div>
            <div id="content" style="width: 80%; margin-left: 10%; height: auto;">

                <!-- 中间内容   start -->
                <div style="width: 100%;"></div>
                <!-- 中间内容   end -->

            </div>
            <div id="right_side" style="width: 8%;">right-side</div>
        </div>
        <!-- <div id="footer">footer</div> -->
    </div>

</body>
</html>