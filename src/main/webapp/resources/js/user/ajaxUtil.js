
/**
 * 通用的ajax请求函数
     * @param type  请求类型 get 或者 post
     * @param url   请求接口url
     * @param parmasMap  Map类型的参数,key与value方式，其中，value统一为字符串
     * @param async   是否异步请求
     * @param successFun   请求成功回调函数的名称
 */
function ajaxRequest(type, url, parmasMap, async, successFun) {
    //拼接参数
    var parmas = "{";
    
    //遍历map，获取参数
    if(parmasMap != null){
        parmasMap.each(function(key,value,index){
            parmas += "\"" + key + "\"" + ":" + "\"" + value + "\"";
            if(index < parmasMap.size() - 1){
                parmas += ",";
            }
        });
        parmas += "}";
    }
    
    $.ajax({
        type : type,
        url : url,
        data : parmasMap == null ? null : eval("(" + parmas + ")"),
        async : async,
        cache : false,
        dataType : "text",
        contentType : "application/json",
        success : function(data) {
            var funObj = new runFun(successFun, data);
            try {
                funObj.func();
            } catch (e) {
                console.log(e);
                console.log(successFun + "()方法不存在或方法内部代码执行有错误");
            }
        }
    });
}

/**
 * 根据方法名称，调用执行方法 
 *      successFun :请求成功回调的方法名称 
 *      data ： 后台返回的数据
 * 
 * 参考文章：http://www.jb51.net/article/48733.htm
 */
function runFun(successFun, data) {
    this.func = new Function(successFun + "('" + data + "');");
}