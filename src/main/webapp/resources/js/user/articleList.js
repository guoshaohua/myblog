//宠物加载参数
var isindex = true;// 是否是首页，显示不同的对话内容
var title = "";
var visitor = "游客";

$(document).ready(function() {
    // 初始化分页
    initPagination(null, false);
});

/**
 * 根据模块id获取文章列表
 */
function getArticleList(pageNo, pageSize) {
    location.href = $("input[name='articleListUrl']").val() + "?pageNo=" + pageNo + "&pageSize=" + pageSize + "&navigationId=" + $("#navigationId").val();
}

/**
 * 分页请求 分页按钮 点击事件的方法实现
 * 
 * @param pageNo
 * @param pageSize
 */
function paginationRequest(pageNo, pageSize) {
    getArticleList(pageNo, pageSize);
}
