
/**
 * 获取导航栏数据
 *      从cookie中读取导航数据
 */
function getNavigationList(){
    var navigationData = $.cookie("navigationData");
    if(navigationData == null){//cookie中没有导航数据，则从后台调用
        ajaxRequest("get", $("input[name='navigationListUrl']").val(), null, true, "navigationCallback");
    }else{//从cookie中读取导航数据进行处理
        navigationCallback(navigationData);
    }
}

/**
 * 导航数据回调 class=\"current-menu-item\"
 */
function navigationCallback(data){
    var result;//最终的导航数据
    
    var navigationData = $.cookie("navigationData");
    if(navigationData == null){
        result = JSON.parse(data);
        
        //设置cookie有效期为1天
        var date = new Date();
        date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
        //将导航数据放在客户端cookie中
        $.cookie("navigationData", data, {path: '/', expires: date});
    }else{
        result = JSON.parse($.cookie("navigationData"));
    }
    //需要将返回的字符串结果，先转换成json格式对象
    var dataArr = result;
    for (var i = 0; i < dataArr[0].length; i++) {
        var str = "";
        // 子菜单数量
        var subLength = dataArr[0][i].sub.length;
        if (subLength <= 0) {// 没有子菜单选项
            if (dataArr[0][i].parent.linkType == 2) {// 跳转到view
                str += '<a href="' + $("input[name='projectUrl']").val() + dataArr[0][i].parent.link +'"><span class="sort"><i class="fa fa-'+dataArr[0][i].parent.icon+'"></i> &nbsp;'+dataArr[0][i].parent.name+'</span></a>';
            } else {// 查询该模块下的文章列表
                str += '<a href="' + $("input[name='articleListUrl']").val() + '?navigationId=' + dataArr[0][i].parent.id + '"><span class="sort"><i class="fa fa-'+dataArr[0][i].parent.icon+'"></i> &nbsp;'+dataArr[0][i].parent.name+'</span></a>';
            }
        } else {
            var str2 = '';
            str2 += '<div id="n' + (i+1) + '" class="nav-zi ty" style="display: none;" _t_nav="n' + (i+1) + '"><ul class="list-inline container m0">';
            for (var k = 0; k < subLength; k++) {
                if(dataArr[0][i].sub[k].linkType == 1){//非视图view
                    str2 += '<li><a href="'+ $("input[name='articleListUrl']").val() +'?navigationId='+ dataArr[0][i].sub[k].id +'"><i class="fa fa-' + dataArr[0][i].sub[k].icon + ' ls"></i> '+ dataArr[0][i].sub[k].name +'</a></li>';
                }else{
                    str2 += '<li><a href="'+ $("input[name='projectUrl']").val() + dataArr[0][i].sub[k].link + '"><i class="fa fa-' + dataArr[0][i].sub[k].icon + ' ls"></i> '+ dataArr[0][i].sub[k].name +'</a></li>';
                }
            }
            str2 += '</ul></div>';
            $("#subDiv").append(str2);
            str += '<a href="###" _t_nav="n'+(i+1)+'" id="an'+(i+1)+'"><span class="sort"><i class="fa fa-'+dataArr[0][i].parent.icon+'"></i> &nbsp;'+dataArr[0][i].parent.name+'<i class="fa fa-angle-down"></i></span></a>';
        }
        //后面加上竖线
        if(i < dataArr[0].length - 1){
            str += '|';
        }
        $("#parentDiv").append(str);
    }
    
    loadNavigationHover();
}

/**
 * 初始化
 */
$(document).ready(function() {
    getNavigationList();
});