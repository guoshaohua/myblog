﻿function exeData(num, type) {
    loadpage();
}
function loadpage() {
    //当前页
    var pageNo = $("#pageNo").val();
    //总页数
    var totalPage = $("#totalPage").val();
    
    var myPageCount = parseInt($("#PageCount").val());
    var myPageSize = parseInt($("#PageSize").val());
    var countindex = myPageCount % myPageSize > 0 ? (myPageCount / myPageSize) + 1 : (myPageCount / myPageSize);
    $("#countindex").val(countindex);
    
    //上一页
    var prePage = (Number(pageNo) - 1) > 0 ? (Number(pageNo) - 1) : 1;
    //下一页
    var nextPage = (Number(pageNo) + 1) > totalPage ? totalPage : (Number(pageNo) + 1);
    $.jqPaginator('#pagination', {
        totalPages: parseInt($("#countindex").val()),
        visiblePages: parseInt($("#visiblePages").val()),
        currentPage: Number(pageNo),
        first: '<li class="first"><a href="javascript:paginationRequest(1,' + myPageSize + ');">首页</a></li>',
        prev: '<li class="prev"><a href="javascript:paginationRequest(' + prePage + ',' + myPageSize + ');"><i class="arrow arrow2"></i>上一页</a></li>',
        next: '<li class="next"><a href="javascript:paginationRequest(' + nextPage + ',' + myPageSize + ');">下一页<i class="arrow arrow3"></i></a></li>',
        last: '<li class="last"><a href="javascript:paginationRequest(' + totalPage +',' + myPageSize + ');">末页</a></li>',
        page: '<li class="page"><a href="javascript:paginationRequest({{page}},' + myPageSize + ');">{{page}}</a></li>',
        onPageChange: function (num, type) {
            if (type == "change") {
                exeData(num, type);
            }
        }
    });
}
$(function () {
    loadpage();
});