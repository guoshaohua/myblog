/*
 * 该分页在项目中处于独立部分。
 * 按照指定的参数名称进行调用initPagination()方法，即可生成分页结果。
 * 每一个分页链接的请求地址，都调用的是paginationRequest()的委托回调函数，
 * 		该函数由调用者进行实现。解决了分页与ajax或者同步view显示的分离
 * 
 * */

var pageNo, // 当前页
pageSize, // 每页数量
totalPage, // 总页数
pagination, // 分页数据
active, // 当前页选中样式
maxPageNum = 10, // 默认最大页码数
threshold = 5;// 分页左右两侧，默认最小页码数阀值

/**
 * 初始化分页数据。该分页功能独立。
 */
function initPagination() {
    pageNo = $("input[name='pageNo']").val();
    pageSize = $("input[name='pageSize']").val();
    totalPage = $("input[name='totalPage']").val();
    /* 显示分页 */
    if (totalPage > 0) {
        $("#pagination").html(getPagination());
    }
}

/* 获取分页结果 */
function getPagination() {
    str = "<ul class=\"pagination\">";
    if (pageNo == 1) {
        str += "    <li class=\"disabled\"><a href=\"###\">&laquo;</a></li>";
    } else {
        str += "    <li><a href=\"javascript:paginationRequest(" + (pageNo - 1) + "," + pageSize + ")\">&laquo;</a></li>";
    }

    /* 10页之内，直接全部显示页码 */
    if (totalPage <= maxPageNum) {
        loadCommon(1, totalPage);
    }
    /* 大于10页，显示省略号 */
    if (totalPage > maxPageNum) {
        /* 当前页位于总页数的中间段,两边都显示省略号 */
        if (pageNo > threshold && pageNo <= totalPage - threshold) {
            str += "<li><a href=\"javascript:paginationRequest(" + 1 + "," + pageSize + ")\">1</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + 2 + "," + pageSize + ")\">2</a></li>";
            str += "<li><a href=\"###\">...</a></li>";
            loadCommon(pageNo - 2, pageNo + 1);
            str += "<li><a href=\"###\">...</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + (totalPage - 1) + "," + pageSize + ")\">" + (totalPage - 1) + "</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + totalPage + "," + pageSize + ")\">" + totalPage + "</a></li>";
        }
        /* 当前页处于后半段(接近末尾)，左边显示省略号 */
        if (pageNo > totalPage - threshold) {
            str += "<li><a href=\"javascript:paginationRequest(" + 1 + "," + pageSize + ")\">1</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + 2 + "," + pageSize + ")\">2</a></li>";
            str += "<li><a href=\"###\">...</a></li>";
            loadCommon(totalPage - threshold - 1, totalPage);
        }
        /* 当前页处于前5页，右边显示省略号 */
        if (pageNo <= threshold) {
            loadCommon(1, threshold + 2);
            str += "<li><a href=\"###\">...</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + (totalPage - 1) + "," + pageSize + ")\">" + (totalPage - 1) + "</a></li>";
            str += "<li><a href=\"javascript:paginationRequest(" + totalPage + "," + pageSize + ")\">" + totalPage + "</a></li>";
        }
    }
    if (pageNo == totalPage) {
        str += "<li class=\"disabled\"><a href=\"###\">&raquo;</a></li>";
    } else {
        str += "<li><a href=\"javascript:paginationRequest(" + (Number(pageNo) + 1) + "," + pageSize + ")\">&raquo;</a></li>";
    }
    str += "</ul>";
    return str;
}

function loadCommon(startIndex, endIndex) {
    for (var i = startIndex; i <= endIndex; i++) {
        if (i == pageNo) {
            active = "active";
        } else {
            active = "";
        }
        str += "<li class=\"" + active + "\"><a href=\"javascript:paginationRequest(" + i + "," + pageSize + ")\">" + i + "</a></li>";
    }
}