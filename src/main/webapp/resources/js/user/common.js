/**
 * 图片懒加载
 */
$(document).ready(function($) {
    $("img.lazy").lazyload({
        effect : "fadeIn"
    });
});
