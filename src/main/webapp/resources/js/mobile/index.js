$(function(){
    
    $('.list-group-item,.menu a').click(function(){
        ////隐藏加载器  
        //$.mobile.loading('hide'); 
        
        //显示ajax-loader.gif
        $.mobile.loading('show', {  
            text: '加载中...', //加载器中显示的文字  
            textVisible: true, //是否显示文字  
            theme: 'a',        //加载器主题样式a-e  
            textonly: false,   //是否只显示文字  
            html: ""//要显示的html内容，如图片等  
        });
    });
    
    
    $("img.lazy").lazyload({
        effect: "fadeIn",
        threshold :200
    });
    
    $(window).scroll(function(){
        // 当滚动到最底部以上50像素时， 加载新内容
        if ($(document).height() - $(this).scrollTop() - $(this).height() < 100){
            $('#container').append($("#test").html());  
               $("img.lazy").lazyload({
                     effect: "fadeIn",
                      threshold :200
             });    
        }
    });
});