
/**
 * 文章是否视图访问，还是访问数据库
 */
function selViewOpen(){
    var v = $("#isViewOpen").val();
    if(v == 1){//否
        $("#viewLinkDiv").css("display","none");
        $("#container").show();
        $("#subBtn").css("display","block");
        $("#contentTitle").css("display","block");
        $("#filedSetDiv").css("display","block");
    }else if(v ==2){//是
        $("#viewLinkDiv").css("display","block");
        $("#container").hide();
        $("#subBtn").css("display","none");
        $("#contentTitle").css("display","none");
        $("#filedSetDiv").css("display","none");
    }
}


/**
 * 编辑器
 */
var release = {
    // 全局参数声明
    parmas : {
        subBtn : "subBtn",// 预览按钮id
        view : "view",// 预览区域id
        moduleSubId : "moduleSubId",// 子模块下拉id
        subBtnSave : "subBtnSave",// 保存按钮id
        title : "title",// 标题文本框id
        introduction : "introduction"// 简介id
    },
    // 预览视图
    view : function() {
        $("#" + this.parmas.subBtn).click(function() {
            var ue = UE.getEditor('container');
            // 存入数据库
            $("#" + release.parmas.view).html(ue.getContent());
        });
    },
    // 保存
    save : function() {
        $("#" + this.parmas.subBtnSave).click(function() {
            // 标题
            var title = $("#" + release.parmas.title).val();
            // 所属模块
            var moduleSubId = $("#" + release.parmas.moduleSubId).val();
            // 简介
            var introduction = $("#" + release.parmas.introduction).val();
            
            var isViewOpen = $("#isViewOpen").val();//是否视图访问
            var viewLink = $("#viewLink").val();
            
            
            if(title == ""){
                $.model.alert("文章标题不能为空", "", function(){
                    
                });
                return;
            }
            if(introduction == ""){
                $.model.alert("文章简介不能为空", "", function(){
                    
                });
                return;
            }
            
            if(isViewOpen == 1){//否，编辑器发布
                var ue = UE.getEditor('container');
                // 内容
                var content = ue.getContent();
                if(content == ""){
                    $.model.alert("文章内容不能为空", "", function(){
                        
                    });
                    return;
                }
                var reg1=new RegExp("<pre class=\"brush:java;toolbar:false\">","g"); //创建正则RegExp对象
                var content1 = content.replace(reg1,"<pre class=\"brush:java;toolbar:false\"><code>");
                
                var reg2=new RegExp("</pre>","g"); //创建正则RegExp对象
                var content2 = content1.replace(reg2,"</code></pre>");
                //console.log(content2);
                $.ajax({
                    type : "post",
                    async : false,
                    url : $("input[name='saveArticleUrl']").val(),
                    data : {
                        "title" : title,
                        "navigationId" : moduleSubId,
                        "introduction" : introduction,
                        "content" : content2,
                        "userId" : 1,
                        "viewLink" : "",
                        "isViewOpen" : 1,
                    },
                    dataType : 'json',
                    success : function(result) {
                        if (result.code == 200) {
                            $.model.alert("文章发表成功");
                        } else {
                            alert(result.msg);
                        }
                    }
                });
            }else{//视图访问
                $.ajax({
                    type : "post",
                    async : false,
                    url : $("input[name='saveArticleUrl']").val(),
                    data : {
                        "title" : title,
                        "navigationId" : moduleSubId,
                        "introduction" : introduction,
                        "content" : "",
                        "viewLink" : viewLink,
                        "isViewOpen" : 2,
                        "userId" : 1
                    },
                    dataType : 'json',
                    success : function(result) {
                        if (result.code == 200) {
                            $.model.alert("文章发表成功");
                        } else {
                            alert(result.msg);
                        }
                    }
                });
            }
        });
    },
    // 获取子模块下拉列表数据
    getModuleSubData : function() {
        $.ajax({
            type : "get",
            url : $("input[name='getModuleSubUrl']").val(),
            dataType : 'json',
            success : function(result) {
                release.moduleDataCallback(result);
            }
        });
    },
    // 下拉数据回调
    moduleDataCallback : function(result) {
        if (result.length > 0) {
            var str = "";
            for (var i = 0; i < result.length; i++) {
                str += "<option value='" + result[i].id + "'>" + result[i].name + "</option>";
            }
            $("#" + this.parmas.moduleSubId).append(str);
        }
    },
    // 初始化操作
    init : function() {
        this.view();
        this.save();
        //this.getModuleSubData();
    }
}


$(document).ready(function() {
    release.init();
});
