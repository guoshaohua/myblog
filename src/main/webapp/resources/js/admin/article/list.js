
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#articleTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#articleTable').bootstrapTable({
        method : 'get',
        url : $("input[name='articleList']").val(),
        cache : false,
        height : 400,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 5,//每页条数
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'title',
            title : '文章标题',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'createTime',
            title : '创建时间',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'updateTime',
            title : '更新时间',
            align : 'left',
            valign : 'bottom'
        },  {
            field : "id",
            title : '操作',
            align : 'left',
            valign : 'middle',
            formatter : operateFormatter
        } ]
    });
});


/**
 * 单元格内容填充
 * @param value
 * @param row
 * @param index
 * @returns
 */
function operateFormatter(value, row, index) {
    return [
        "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editArticle('" + value + "')\">"
            + '<span class="glyphicon glyphicon-edit"></span>修改'
        +"</a>",
        "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"删除\" onclick=\"javascript:delArticle('" + value + "')\">"
        + '<span class="glyphicon glyphicon-edit"></span>删除'
        +"</a>",
        "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"预览\" onclick=\"javascript:showArticle('" + value + "')\">"
        + '<span class="glyphicon glyphicon-edit"></span>预览'
        +"</a>"
    ].join('');
}

function showArticle(id){
    window.open($("input[name='showArticleUrl']").val() + "/" + id);
}
function delArticle(id){
    var map = new Map();
    map.put("articleId", id);
    
    ajaxRequest("get", $("input[name='delArticleUrl']").val(), map, false, "delArticleCallback");
}

function delArticleCallback(result){
    var data = JSON.parse(result);
    if(data.code != 200){
        $.model.alert("删除失败","",function(){
            location.reload();
        });
    }else{
        $.model.alert("删除成功","",function(){
            location.reload();
        });
    }
}
/**
 * 编辑文章
 * @param roleId
 */
function editArticle(id){
    //window.open ($("input[name='editArticleUrl']").val() + "?articleId=" + id,'newwindow','');
    location.href = $("input[name='editArticleUrl']").val() + "?articleId=" + id;
}