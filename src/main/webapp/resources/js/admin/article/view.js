
$(document).ready(function() {
    $("#subBtn").click(function() {
        var ue = UE.getEditor('container');
        console.log(ue.getContent());
        $("#view").html(ue.getContent());
    });
});



/**
 * 更新文章
 */
function updateArticle(){
    $(".btn-primary").button("loading");
    var title = $("#title").val();
    var navigationId = $("select[name='navigationId']").val();
    var ue = UE.getEditor('container');
    var content = ue.getContent();
    
    
    var reg1=new RegExp("<pre class=\"brush:java;toolbar:false\">","g"); //创建正则RegExp对象
    var content1 = content.replace(reg1,"<pre class=\"brush:java;toolbar:false\"><code>");
    
    var reg2=new RegExp("</pre>","g"); //创建正则RegExp对象
    var content2 = content1.replace(reg2,"</code></pre>");
    
    $.ajax({
        type : "post",
        async : false,
        url : $("input[name='updateArticleUrl']").val(),
        data : {
            "title" : encodeURI(title),
            "id" : $("input[name='articleId']").val(),
            "navigationId" : navigationId,
            "available" : "1",
            "content" : content2
        },
        dataType : 'json',
        success : function(result) {
            if (result.code == 200) {
                $.model.alert("更新成功", "", function(){
                    $(".btn-primary").button("reset");
                    $(".btn-default").click();
                });
            } else {
                $.model.alert("更新失败", "", function(){
                    $(".btn-primary").button("reset");
                    $(".btn-default").click();
                });
            }
        }
    });
}


function updateArticleCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $.model.alert("更新成功", "", function(){
            $(".btn-primary").button("reset");
            $(".btn-success").click();
        });
    }else{
        $.model.alert("更新失败", "", function(){
            $(".btn-primary").button("reset");
            $(".btn-success").click();
        });
    }
}