
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#permissionTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#permissionTable').bootstrapTable({
        method : 'get',
        url : $("input[name='permissionList']").val(),
        cache : false,
        height : 500,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 5,//每页条数
        pageList: [5,10,20],//分页size数组
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'name',
            title : '权限名称',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'code',
            title : '权限code',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'description',
            title : '权限说明',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'parentId',
            title : '父级ID',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'parentName',
            title : '父级名称',
            align : 'left',
            valign : 'bottom'
        }]
    });
});
