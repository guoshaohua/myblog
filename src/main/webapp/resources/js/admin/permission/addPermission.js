/**
 * 点击某个节点，为parentId赋值当前点击的节点id
 * @param event
 * @param treeId
 * @param treeNode
 * @param clickFlag
 */
function onClick(event, treeId, treeNode, clickFlag) {
    $("#parentId").val(treeNode.id);
}

/**
 * tree基本设置
 */
var setting = {
    view : {
        showIcon : false
    },
    data : {
        simpleData : {
            enable : true
        }
    },
    callback: {
        onClick: onClick
    }
};
var zNodes;
$(document).ready(function(){
    /**
     * 获取权限列表json字符串，并使用eval函数转换为一个对象.不转的话，则会报错。
     */
    zNodes = eval("(" + $("input[name='permissions']").val() + ")");
    
    /**
     * 初始化tree
     */
    $.fn.zTree.init($("#permissionTree"), setting, zNodes);
    
    /**
     * 获取tree对象
     */
    var treeObj = $.fn.zTree.getZTreeObj("permissionTree");
    /**
     * 获取id为-1的节点，这里为最顶级节点
     */
    var node = treeObj.getNodeByParam("id", -1, null);
    /**
     * 默认选中最顶级节点.(这里指的是：无上级权限)
     */
    treeObj.selectNode(node);
});
$(document).ready(function(){
    $("#permissionForm").validate({
        rules : {
            name : { 
                required:true,
                maxlength:20
            },
            code : { 
                required:true,
                maxlength:50
            }
        },
        messages : {
            name : { 
                required : "权限名称不能为空",
                    maxlength : "权限长度不能超过20个字符"
                },
            code : { 
                required : "权限代码不能为空",
                maxlength : "权限代码长度不能超过50个字符"
            }
        }
    });
});

/**
 * 新增权限
 */
function addPermission(){
    $(".btn-primary").button("loading");
    var parmasMap = new Map();
    parmasMap.put("name", encodeURI($("#name").val()));
    parmasMap.put("code", encodeURI($("#code").val()));
    parmasMap.put("parentId",$("#parentId").val());
    ajaxRequest("get", $("input[name='addPermissionUrl']").val(), parmasMap, false, "addPermissionCallback")
}
function addPermissionCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $(".btn-primary").button('reset');
        $.model.alert("权限添加成功","",function(){
            window.location.reload();
        });
    }else if(result.code == 201){
        $(".btn-primary").button('reset');
        $.model.alert("权限code重复，重新输入");
    }
}

/**
 * 删除一个节点
 */
function delPermission(){
    $(".btn-danger").button("loading");
    if ($("#parentId").val() == 0 || $("#parentId").val() == -1) {
        $.model.alert("对不起，根节点不能删除","",function(){
            $(".btn-danger").button('reset');
        });
        return false;
    }
    $.messager.confirm("系统提示", "你确定要删除所选权限吗?", function() { 
        var map = new Map();
        map.put("id", $("#parentId").val());
        ajaxRequest("get", $("input[name='delPermissionUrl']").val(), map, false, "delPermissionCallback");
    },function(){
        $(".btn-danger").button('reset');
    });
}

function delPermissionCallback(data){
    $(".btn-danger").button('reset');
    var result = JSON.parse(data);
    if (result.code == 200) {
        $.model.alert("权限删除成功","",function(){
            window.location.reload();
        });
    } else if(result.code == 201) {
        $.model.alert("该节点下有子节点，不能删除");
    } else if(result.code == 202) {
        $.model.alert("该权限已分配给角色，不能删除");
    } else {
        $.model.alert("未知异常，删除失败，请稍后重试");
    }
}