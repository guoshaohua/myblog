$(document).ready(function() {
    
});


function editMenu(){
    $(".btn-primary").button("loading");
    var map = new Map();
    map.put("name", encodeURI($("#name").val()));
    if($("#menuPid").val() != "-1"){
        map.put("link", encodeURI($("#link").val()));
    }
    map.put("pid", $("#menuPid").val());
    map.put("id", $("#menuId").val());
    map.put("model", $("#model").val());
    
    ajaxRequest("get", $("input[name='editMenuUrl']").val() , map, false, "editMenuCallback");
}

function editMenuCallback(result){
    var data = JSON.parse(result);
    if(data.code == 200){
        $.model.alert("菜单更新成功","",function(){
            $(".btn-primary").button("reset");
            $(".btn-success").click();
        });
    }
}