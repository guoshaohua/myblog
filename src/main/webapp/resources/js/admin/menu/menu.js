
function checkSysMenu() {
    var zTree = $.fn.zTree.getZTreeObj("menuTree"), 
    type = {"Y" : "ps","N" : "ps"};
    zTree.setting.check.chkboxType = type;
}

function onClick(event, treeId, treeNode, clickFlag) {
    $("input[name='menuId']").val(treeNode.id);
}

$(document).ready(function() {
    var setting = {
        check : {
            enable : true
        },
        data : {
            simpleData : {
                enable : true
            }
        },
        callback: {
            onClick: onClick
        }
    };
    var menus = $("input[name='menus']").val();
    $.fn.zTree.init($("#menuTree"), setting, eval("(" + menus + ")"));
    checkSysMenu();
});

function changeParent(){
    var selMenuType = $("select[name='menuType']").val();
    if(selMenuType == "1"){//是父级菜单
        $("#parentMenuDiv").css("display","none");
        $("#linkDiv").css("display","none");
    }else{
        $("#parentMenuDiv").css("display","block");
        $("#linkDiv").css("display","block");
    }
}

// 验证必填项是否输入合法
function check_param() {
    var menuType = $("select[name='menuType']").val();
    var name = $("input[name='name']").val();
    if(name == "" || name.trim().length <= 0){
        return false;
    }
    if(menuType == "2"){
        var link = $("input[name='link']").val();
        if(link == "" || link.trim().length <= 0){
            return false;
        }
    }
    return true;
}

/**
 * 添加菜单
 */
function addMenu(){
    var $btn = $(".btn-primary").button('loading');
    if(!check_param()){
        $.model.alert("请检查各项必填项是否输入正确","",function(){
            $btn.button('reset');
        });
        return;
    }
    var menuId = $("input[name='menuId']").val();
    var map = new Map();
    map.put("name", encodeURI($("#name").val()));
    map.put("model", $("input[name='model']").val());
    
    var menuType = $("select[name='menuType']").val();
    if(menuType == "1"){//父级
        map.put("pid", "-1");
    }else{
        map.put("id", menuId);
        map.put("pid", $("#pid").val());
        map.put("link", $("#link").val());
    }
    ajaxRequest("get", $("input[name='addMenuUrl']").val(), map, false, "addMenuCallback")
}

function addMenuCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $(".btn-primary").button('reset');
        $.model.alert("添加成功","",function(){
            window.location.reload();
        });
    }
}
