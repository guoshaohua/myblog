
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#menuTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#menuTable').bootstrapTable({
        method : 'get',
        url : $("input[name='menuList']").val(),
        cache : false,
        height : 350,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 5,//每页条数
        pageList: [5,10,20],
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'name',
            title : '菜单名称',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'link',
            title : '菜单链接',
            align : 'left',
            valign : 'bottom'
        }, {
            field : "id",
            title : '操作',
            align : 'left',
            valign : 'middle',
            formatter : operateFormatter
        } ]
    });
});



/**
 * 单元格内容填充
 * @param value
 * @param row
 * @param index
 * @returns
 */
function operateFormatter(value, row, index) {
    return [
            "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editMenu('" + value + "')\">"
            + "   <span class=\"glyphicon glyphicon-edit\"></span>修改" 
            + "</a>"
    ].join('');
}

/**
 * 编辑菜单
 * @param roleId
 */
function editMenu(menuId){
    location.href = $("input[name='editMenuUrl']").val() + "?id=" + menuId;
}
