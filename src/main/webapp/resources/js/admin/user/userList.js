
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#userTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#userTable').bootstrapTable({
        method : 'get',
        url : $("input[name='userList']").val(),
        cache : false,
        height : 500,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 5,//每页条数
        pageList: [5,10,20],
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'userName',
            title : '用户名称',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'realName',
            title : '真实姓名',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'phone',
            title : '手机号',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'email',
            title : '邮箱',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'createTime',
            title : '注册时间',
            align : 'left',
            valign : 'middle'
        }, {
            field : 'updateTime',
            title : '更新时间',
            align : 'left',
            valign : 'middle'
        }, {
            field : 'statusName',
            title : '状态',
            align : 'left',
            valign : 'bottom'
        }, {
            field : "id",
            title : '操作',
            align : 'left',
            valign : 'middle',
            formatter : operateFormatter
        } ]
    });
});

/**
 * 设置行背景色
 *      pink是页面中head内的css样式
 * @param row
 * @param index
 * @returns
 */
function rowStyle(row, index) {
    var classes = ["lock","del"];
    if (row["status"] == "2") {//锁定状态
        return {
            classes : classes[0]
        };
    }else if(row["status"] == "3"){//删除状态
        return {
            classes : classes[1]
        }; 
    }
    return {};
}


/**
 * 根据用户状态获取操作链接按钮组
 * @param status
 */
function getBtnLinkByStatus(id,status){
    var str = "";
    switch (status) {
        case 1://账户正常
            str += "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editUser('" + id + "')\">"
                + "   <span class=\"glyphicon glyphicon-edit\"></span>修改" 
                + "</a>";
            str += "<a style=\"margin-right:10px\" class=\"btn btn-warning btn-xs\" title=\"锁定\" onclick=\"javascript:updateUserStatus('" + id + "',2)\">"
                + "   <span class=\"glyphicon glyphicon-lock\"></span>锁定" 
                + "</a>";
            str += "<a style=\"margin-right:10px\" class=\"btn btn-danger btn-xs del_object\" title=\"删除\" onclick=\"javascript:updateUserStatus('" + id + "',3)\">"
                + "   <span class=\"glyphicon glyphicon-remove\"></span>删除" 
                + "</a>";
            break;
        case 2://账户锁定
            str += "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editUser('" + id + "')\">"
                + "   <span class=\"glyphicon glyphicon-edit\"></span>修改" 
                + "</a>";
            str += "<a style=\"margin-right:10px\" class=\"btn btn-danger btn-xs del_object\" title=\"解锁\" onclick=\"javascript:updateUserStatus('" + id + "',1)\">"
                + "   <span class=\"glyphicon glyphicon-lock\"></span>解锁" 
                + "</a>";
            str += "<a style=\"margin-right:10px\" class=\"btn btn-danger btn-xs del_object\" title=\"删除\" onclick=\"javascript:updateUserStatus('" + id + "',3)\">"
                + "   <span class=\"glyphicon glyphicon-remove\"></span>删除" 
                + "</a>";
            break;
        default://3  已删除
            str += "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editUser('" + id + "')\">"
                + "   <span class=\"glyphicon glyphicon-edit\"></span>修改" 
                + "</a>";
            str += "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"恢复账户\" onclick=\"javascript:updateUserStatus('" + id + "',1)\">"
                + "   <span class=\"glyphicon glyphicon-remove\"></span>恢复账户" 
                + "</a>";
            break;
    }
    return str;
}

/**
 * 单元格内容填充
 * @param value
 * @param row
 * @param index
 * @returns
 */
function operateFormatter(value, row, index) {
    return [getBtnLinkByStatus(value,row["status"])].join('');
}

/**
 * 编辑会员
 * @param roleId
 */
function editUser(userId){
    location.href = $("input[name='editUserUrl']").val() + "?id=" + userId;
}

function updateUserStatus(userId,status){
    var map = new Map();
    map.put("id", userId);
    map.put("status", status);
    ajaxRequest("get", $("input[name='updateUserStatusUrl']").val() , map, false, "updateUserStatusCallback");
}
function updateUserStatusCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $.model.alert("更新会员状态成功","",function(){
            location.reload();
        });
    }else{
        $.model.alert("更新会员状态失败");
    }
}