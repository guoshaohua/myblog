
function checkRole() {
    var zTree = $.fn.zTree.getZTreeObj("roleTree"), 
    type = {"Y" : "ps","N" : "ps"};
    zTree.setting.check.chkboxType = type;
}

$(document).ready(function() {
    var setting = {
        check : {
            enable : true
        },
        data : {
            simpleData : {
                enable : true
            }
        }
    };
    var roles = $("input[name='roles']").val();
    $.fn.zTree.init($("#roleTree"), setting, eval("(" + roles + ")"));
    checkRole();
});

// 验证是否选择了权限
function check_param() {
    var numberval = 0;
    var treeRole = $.fn.zTree.getZTreeObj("roleTree"), 
        roles = treeRole.getCheckedNodes(true), 
        pmss = "";
    if (roles.length > 0) {
        for (var i = 0; i < roles.length; i++) {
            pmss += roles[i].id + ",";
        }
        if (pmss.length > 0) {
            pmss = pmss.substring(0, pmss.length - 1);
        }
        $("#roleIds").val(pmss);
    } else {
        numberval++;
    }
    if (numberval > 0) {
        return false;
    }
    return true;
}

/**
 * 更新会员信息
 */
function updateUser(){
    var $btn = $(".btn-primary").button("loading");
    if(!check_param()){
        $.model.alert("请为该会员赋予角色");
        $btn.button('reset');
        return;
    }
    var map = new Map();
    map.put("id", $("input[name='userId']").val());
    map.put("roleIds", $("#roleIds").val());
    map.put("userName", $("#userName").val());
    map.put("phone", $("#phone").val());
    map.put("email", $("#email").val());
    
    ajaxRequest("get", $("input[name='updateUserUrl']").val(), map, false, "updateUserCallback")
}

function updateUserCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $(".btn-primary").button('reset');
        $.model.alert("信息保存成功","",function(){
            $(".btn-success").click();
        });
    }
}
