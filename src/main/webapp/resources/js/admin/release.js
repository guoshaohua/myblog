/**
 * 编辑器
 */
var release = {
    // 全局参数声明
    parmas : {
        subBtn : "subBtn",// 预览按钮id
        kindEditor : "kindEditor",// 内容区域id
        view : "view",// 预览区域id
        moduleSubId : "moduleSubId",// 子模块下拉id
        subBtnSave : "subBtnSave",// 保存按钮id
        title : "title",// 标题文本框id
        introduction : "introduction"// 简介id
    },
    // 编辑器工具初始化
    editInit : function() {
        KindEditor.ready(function(K) {
            var editor1 = K.create("textarea[name='kindEditor']", {
                cssPath : $("input[name='prettifyUrl']").val(),
                uploadJson : $("input[name='fileUploadUrl']").val(),
                fileManagerJson : $("input[name='fileManagerUrl']").val(),
                allowFileManager : true,
                afterCreate : function() {
                    var self = this;
                    K.ctrl(document, 13, function() {
                        self.sync();
                        document.forms['example'].submit();
                    });
                    K.ctrl(self.edit.doc, 13, function() {
                        self.sync();
                        document.forms['example'].submit();
                    });
                }
            });
            prettyPrint();
        });
    },
    // 预览视图
    view : function() {
        $("#" + this.parmas.subBtn).click(function() {
            $("#" + release.parmas.kindEditor).prev().children("div:eq(1)").children(".ke-edit-iframe").attr("id", "ke-edit-iframe");
            // 获取编辑区域内的内容
            var contents = $(document.getElementById('ke-edit-iframe').contentWindow.document.body).html();
            // 存入数据库
            $("#" + release.parmas.view).html(contents);
        });
    },
    // 保存
    save : function() {
        $("#" + this.parmas.subBtnSave).click(function() {
            // 标题
            var title = $("#" + release.parmas.title).val();
            // 所属模块
            var moduleSubId = $(
                    "#" + release.parmas.moduleSubId).val();
            // 简介
            var introduction = $(
                    "#" + release.parmas.introduction).val();
            // 内容
            var content = $(document.getElementById('ke-edit-iframe').contentWindow.document.body).html();
            $.ajax({
                type : "post",
                async : false,
                url : "article/saveArticle",
                data : {
                    "title" : title,
                    "navigationId" : moduleSubId,
                    "introduction" : introduction,
                    "content" : content,
                    "userId" : 1
                },
                dataType : 'json',
                success : function(result) {
                    if (result.code == 200) {
                        alert("文章发表成功");
                    } else {
                        alert(result.msg);
                    }
                }
            });
        });
    },
    // 获取子模块下拉列表数据
    getModuleSubData : function() {
        $.ajax({
            type : "get",
            url : "navigation/getModuleSub",
            dataType : 'json',
            success : function(result) {
                release.moduleDataCallback(result);
            }
        });
    },
    // 下拉数据回调
    moduleDataCallback : function(result) {
        console.log("分类结果：" + result);
        if (result.length > 0) {
            var str = "";
            for (var i = 0; i < result.length; i++) {
                str += "<option value='" + result[i].id + "'>" + result[i].name + "</option>";
            }
            $("#" + this.parmas.moduleSubId).append(str);
        }
    },
    // 初始化操作
    init : function() {
        this.editInit();
        this.view();
        this.save();
        this.getModuleSubData();
    }
}

$(document).ready(function() {
    release.init();
    $("#jianjieBtn").click(function() {
        var introduction = $("#introduction").val();
        $("#jianjieView").html(introduction);
    });
});
