

$(document).ready(function(){
    /**
     * 加载菜单Menu
     */
    var menuData = $.cookie("menuDataSystem");
    if(menuData == null){
        var map = new Map();
        map.put("model", 1);
        map.put("type", "1");
        ajaxRequest("get", $("input[name='getMenuListUrl']").val(), map, true, "menuCallback");
    }else{
        menuCallback(menuData);
    }
});


//该变量为map，key为父级菜单id，value为该父级菜单下的子菜单数组
var m;
/**
 * 菜单回调
 *      本地存储、cookie缓存，减少系统请求压力
 * @param result
 */
function menuCallback(data){
    var result;
    var menuData = $.cookie("menuDataSystem");
    if(menuData == null){
        result = JSON.parse(data);
        //设置cookie有效期为1天
        var date = new Date();
        date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000));
        $.cookie("menuDataSystem", data, {path: '/', expires: date});
    }else{
        result = JSON.parse($.cookie("menuDataSystem"));
    }
    //存放父级别菜单
    var parentMenuArr = new Array();
    m = new Map();
    if(result.rows != null && result.rows.length > 0){
        var data = result.rows;
        for (var i = 0; i < data.length; i++) {
            var menuObj = data[i];
            if(menuObj.pid == -1){//父级别
                parentMenuArr.push(menuObj);
                m.put(menuObj.id,new Array());
            }
        }
        for (var i = 0; i < data.length; i++) {
            var menuObj = data[i];
            if(menuObj.pid != -1){//父级别
                m.get(menuObj.pid).push(menuObj);
            }
        }
        
        //遍历显示父级菜单
        for (var i = 0; i < parentMenuArr.length; i++) {
            var str = "";
            var color = "";
            if(i == 0){
                color = "active";
            }
            str += "<li name=\"li_" + parentMenuArr[i].id + "\" class=\"" + color + "\"><a onfocus=\"this.blur()\" href=\"javascript:showChildMenu('" + parentMenuArr[i].id + "');\">" + parentMenuArr[i].name + "</a></li>";
            $(".navbar-nav").append(str);
        }
    }
    if(parentMenuArr != null && parentMenuArr.length > 0){
        //默认显示第一个父菜单的子菜单列表
        showChildMenu(parentMenuArr[0].id);
    }
}


/**
 * 设置菜单背景样式
 * @param pid
 */
function initParentMenuBg(pid){
    var lis = $(".navbar-nav").children("li");
    for (var i = 0; i < lis.length; i++) {
        $(lis[i]).removeClass("active");
    }
    $("li[name='li_" + pid + "']").addClass("active");
}

function initSubMenuBg(id){
    var lis = $(".nav-stacked").children("li");
    for (var i = 0; i < lis.length; i++) {
        $(lis[i]).removeClass("active");
    }
    $("li[name='sub_" + id + "']").addClass("active");
}

/**
 * 根据父id获取子菜单数据
 *      显示在左侧页面
 * @param pid
 */
function showChildMenu(pid){
    //为父级菜单设置选中样式背景
    initParentMenuBg(pid);
    
    $(".nav-stacked").html("");
    for (var i = 0; i < m.get(pid).length; i++) {
        var color = "";
        if(i == 0){
            color = "active";
        }
        var str = "<li name=\"sub_" + m.get(pid)[i].id + "\" class=\"" + color + "\"><a style=\"cursor:pointer;\" onclick='javascript:window.frames[\"systemFrame\"].location.href=\"" + $("input[name='projectUrl']").val() + m.get(pid)[i].link + "\";initSubMenuBg(\"" + m.get(pid)[i].id + "\")'>" + m.get(pid)[i].name + "</a></li>";
        $(".nav-stacked").append(str);
    }
    if(m.get(pid)[0] != null && m.get(pid)[0].link != null){
        window.frames["systemFrame"].location.href = $("input[name='projectUrl']").val() + m.get(pid)[0].link;
    }
}
