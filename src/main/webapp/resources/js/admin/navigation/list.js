
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#navigationTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#navigationTable').bootstrapTable({
        method : 'get',
        url : $("input[name='navigationList']").val(),
        cache : false,
        height : 500,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 10,//每页条数
        pageList: [5,10,20],
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'name',
            title : '导航名称',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'available',
            title : '是否可用',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'link',
            title : '链接',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'pid',
            title : '父级',
            align : 'left',
            valign : 'bottom'
        }, {
            field : "id",
            title : '操作',
            align : 'left',
            valign : 'middle',
            formatter : operateFormatter
        } ]
    });
});



/**
 * 单元格内容填充
 * @param value
 * @param row
 * @param index
 * @returns
 */
function operateFormatter(value, row, index) {
    return [
        "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editNavigation('" + value + "')\">"
        + "   <span class=\"glyphicon glyphicon-edit\"></span>修改" 
        + "</a>"
    ].join('');
}

/**
 * 编辑导航
 */
function editNavigation(id){
    location.href = $("input[name='editNavigationUrl']").val() + "?id=" + id;
}
