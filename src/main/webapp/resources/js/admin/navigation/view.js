
$(document).ready(function(){
    $("#linkType").change(function(){
        var linkType = $(this).val();
        if(linkType == 2){//视图
            $("#linkDiv").css("display","block");
        }else{
            $("#linkDiv").css("display","none");
        }
    });
});

/**
 * 更新导航数据
 */
function editNavigation(){
    $(".btn-primary").button("loading");
    var map = new Map();
    map.put("name", encodeURI($("#name").val()));
    map.put("link", $("#link").val() == undefined ? "" : $("#link").val());
    map.put("id", $("#navigationId").val());
    map.put("pid", $("#pid").val() == undefined ? 0 : $("#pid").val());
    map.put("available", $("#available").val());
    map.put("linkType", $("#linkType").val());
    ajaxRequest("get", $("input[name='editNavigationUrl']").val() , map, false, "editNavigationCallback");
}

function editNavigationCallback(result){
    var data = JSON.parse(result);
    if(data.code == 200){
        $.model.alert("导航更新成功","",function(){
            $(".btn-primary").button("reset");
            $(".btn-success").click();
        });
    }
}