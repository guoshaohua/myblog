
$(document).ready(function(){
    
    
    var linkType = $("#linkType").val();
    var isParent = $("#isParentSelect").val();
    if(linkType == 2 && isParent != 1){//视图
        $("#linkDiv").css("display","block");
        $("#link").attr("required","required");
    }else{
        $("#link").removeAttr("required");
        $("#linkDiv").css("display","none");
    }
    
    /**
     * 是否父级
     */
    $("#isParentSelect").change(function(){
        var isParent = $(this).val();
        if(isParent == 1){//是父级
            $("#pidDiv").css("display","none");
            $("#linkDiv").css("display","none");
            $("#link").removeAttr("required");
        }else{
            $("#pidDiv").css("display","block");
            $("#linkDiv").css("display","block");
            $("#link").attr("required","required");
        }
    });
    
    /**
     * 链接类型
     */
    $("#linkType").change(function(){
        var linkType = $(this).val();
        if(linkType == 1){//非视图
            $("#linkDiv").css("display","none");
            $("#link").removeAttr("required");
        }else if($("#isParentSelect").val() != 1){
            $("#linkDiv").css("display","block");
            $("#link").attr("required","required");
        }
    });
});

/**
 * 更新导航数据
 */
function addNavigation(){
    $(".btn-primary").button("loading");
    
    var isParent = $("#isParentSelect").val();//是否父级
    var map = new Map();
    map.put("name", encodeURI($("#name").val()));
    map.put("linkType", $("#linkType").val());
    map.put("available", 1);
    if(isParent == 1){//是父级
        map.put("pid", 0);
        map.put("indexNo", $("#pid option").length + 1);
    }else{
        map.put("pid", $("#pid").val());
        var linkType = $("#linkType").val();
        if(linkType == 2){//视图
            map.put("link", encodeURI($("#link").val()));
        }
    }
    
    ajaxRequest("get", $("input[name='addNavigationUrl']").val() , map, false, "addNavigationCallback");
}

function addNavigationCallback(result){
    var data = JSON.parse(result);
    if(data.code == 200){
        $.model.alert("导航添加成功","",function(){
            $(".btn-primary").button("reset");
            $(".btn-success").click();
        });
    }
}