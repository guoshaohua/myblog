
$(document).ready(function(){
    
    /**
     * 刷新
     */
    $('#refresh').click(function() {
        $('#roleTable').bootstrapTable('refresh', {});
    });
    
    /**
     * 表格数据初始化
     */
    $('#roleTable').bootstrapTable({
        method : 'get',
        url : $("input[name='roleList']").val(),
        cache : false,
        height : 400,
        striped : false,//行背景色
        pagination : true,//分页
        pageSize : 5,//每页条数
        sidePagination : "server",
        searchFormName : "queryForm",
        search : false,
        columns : [ {
            field : 'name',
            title : '角色名称',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'status',
            title : '角色状态',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'description',
            title : '角色说明',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'createTime',
            title : '创建时间',
            align : 'left',
            valign : 'bottom'
        }, {
            field : 'updateTime',
            title : '更新时间',
            align : 'left',
            valign : 'middle'
        }, {
            field : "id",
            title : '操作',
            align : 'left',
            valign : 'middle',
            formatter : operateFormatter
        } ]
    });
});

/**
 * 设置行背景色
 *      pink是页面中head内的css样式
 * @param row
 * @param index
 * @returns
 */
function rowStyle(row, index) {
    var classes = ["lock","del","ok"];
    if (row["status"] == "2") {//锁定状态
        return {
            classes : classes[0]
        };
    }else if(row["status"] == "3"){//删除状态
        return {
            classes : classes[1]
        }; 
    }
    return {};
}

/**
 * 单元格内容填充
 * @param value
 * @param row
 * @param index
 * @returns
 */
function operateFormatter(value, row, index) {
    return [
        "<a style=\"margin-right:10px\" class=\"btn btn-info btn-xs\" title=\"修改\" onclick=\"javascript:editRole('" + value + "')\">",
            '<span class="glyphicon glyphicon-edit"></span>修改',
        "</a>"
    ].join('');
}

/**
 * 编辑角色
 * @param roleId
 */
function editRole(roleId){
    location.href = $("input[name='editRoleUrl']").val() + "?id=" + roleId;
}