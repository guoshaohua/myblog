
function checkPermission() {
    var zTree = $.fn.zTree.getZTreeObj("permissionTree"), 
    type = {"Y" : "ps","N" : "ps"};
    zTree.setting.check.chkboxType = type;
}

$(document).ready(function() {
    var setting = {
        check : {
            enable : true
        },
        data : {
            simpleData : {
                enable : true
            }
        }
    };
    var permissions = $("input[name='permissions']").val();
    $.fn.zTree.init($("#permissionTree"), setting, eval("(" + permissions + ")"));
    checkPermission();
});

// 验证是否选择了权限
function check_param() {
    var numberval = 0;
    var treePermission = $.fn.zTree.getZTreeObj("permissionTree"), 
        permissions = treePermission.getCheckedNodes(true), 
        pmss = "";
    if (permissions.length > 0) {
        for (var i = 0; i < permissions.length; i++) {
            pmss += permissions[i].id + ",";
        }
        if (pmss.length > 0) {
            pmss = pmss.substring(0, pmss.length - 1);
        }
        $("#permissionIds").val(pmss);
    } else {
        numberval++;
    }
    if (numberval > 0) {
        return false;
    }
    return true;
}

/**
 * 更新角色
 */
function updateRole(){
    var $btn = $(".btn-primary").button('loading');
    if(!check_param()){
        $btn.button('reset');
        $.model.alert("请为该角色赋予权限");
        return;
    }
    var map = new Map();
    map.put("description", encodeURI($("#description").val()));
    map.put("id", $("input[name='roleId']").val());
    map.put("permissionIds", $("#permissionIds").val());
    ajaxRequest("get", $("input[name='updateRoleUrl']").val(), map, false, "updateRoleCallback")
}

function updateRoleCallback(data){
    var result = JSON.parse(data);
    if(result.code == 200){
        $(".btn-primary").button('reset');
        $.model.alert("更新成功","",function(){
            $(".btn-success").click();
        });
    }
}
