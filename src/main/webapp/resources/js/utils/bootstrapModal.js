/**
 * @author zhoubang
 * 
 * 简单封装了通用的模态提示框
 *      方便js代码直接调用。 
 *      html代码使用的是 /WEB-INF/view/common/modal.jsp
 */

/**
 * 消息提示，带有回调处理
 */
$.model = (function() {
    var alert = function(message,title,callback){
        if(title == "" || title == null){
            title = "系统提示";
        }
        $("#gridSystemModalLabel").html(title);
        $("#modelAlert .container-fluid").html(message);
        $("#modelAlert").on('hidden.bs.modal', function (e) {
            if(undefined != callback){
                callback();
            }
        });
        $("#modelAlert").modal("show");
    };
    return {
        alert : alert
    };
})();

