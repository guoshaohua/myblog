﻿$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    // $('[data-toggle="popover"]').popover()
    $('[data-toggle="popover"]').popover({
        trigger : 'hover'
    })
    $('[data-spy="scroll"]').each(function() {
        var $spy = $(this).scrollspy('refresh')
    })
})
$(window).scroll(function() {
    var menu_top = $('#menu_wrap').offset().top;
    if ($(window).scrollTop() >= 255) {
        $(".nav-bg").css("top", 50);
        $(".nav-bg").css("position", "fixed");
        $('#menu_wrap').addClass('menuFixed')
    }
    if ($(window).scrollTop() < 255) {
        $(".nav-bg").css("position", "relative");
        $(".nav-bg").css("top", 255);
        $('#menu_wrap').removeClass('menuFixed')
    }
    if (n == 1) {
        if ($(window).scrollTop() >= 30) {
            $(".nav-bg").css("top", 50);
            $(".nav-bg").css("position", "fixed");
            $('#menu_wrap').addClass('menuFixed')
        }
        if ($(window).scrollTop() < 30) {
            $(".nav-bg").css("position", "relative");

            $('#menu_wrap').removeClass('menuFixed')
        }
    }
});

/*jQuery(document).ready(function() {
    var qcloud = {};
    $('[_t_nav]').hover(function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            $('[_t_nav]').each(function() {
                // $(this)[_nav == $(this).attr('_t_nav') ? 'addClass':
                // 'removeClass']('nav-zibg');
            });
            $('#a' + _nav).addClass("nav-zibg");
            $('#' + _nav).stop(true, true).fadeIn(100);
            $('#zt').addClass('mh');
        }, 300);
    }, function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            $('[_t_nav]').removeClass('nav-zibg');
            $('#' + _nav).stop(true, true).fadeOut(0);
            $('#zt').removeClass('mh')
        }, 300);
    });
});*/

function loadNavigationHover(){
    var qcloud = {};
    $('[_t_nav]').hover(function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            $('[_t_nav]').each(function() {
                // $(this)[_nav == $(this).attr('_t_nav') ? 'addClass':
                // 'removeClass']('nav-zibg');
            });
            $('#a' + _nav).addClass("nav-zibg");
            $('#' + _nav).stop(true, true).fadeIn(100);
            $('#zt').addClass('mh');
        }, 300);
    }, function() {
        var _nav = $(this).attr('_t_nav');
        clearTimeout(qcloud[_nav + '_timer']);
        qcloud[_nav + '_timer'] = setTimeout(function() {
            $('[_t_nav]').removeClass('nav-zibg');
            $('#' + _nav).stop(true, true).fadeOut(0);
            $('#zt').removeClass('mh')
        }, 300);
    });
    
    //鼠标事件，菜单背景样式
    $(".nav-zi a").mouseover(function() {
        $(this).children().removeClass("ls");
    });
    $(".nav-zi a").mouseout(function() {
        $(this).children().addClass("ls");
    });
}

/*$(document).ready(function() {
    $(".nav-zi a").mouseover(function() {
        console.log($(this).children());
        $(this).children().removeClass("ls");
    });
    $(".nav-zi a").mouseout(function() {
        $(this).children().addClass("ls");
    });
});*/

