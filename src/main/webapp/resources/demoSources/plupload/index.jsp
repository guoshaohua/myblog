<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglibs.jsp"%>
<!DOCTYPE>
<html>
<head>
<title>GodSon Easyui 结合Pluplaod插件的上传演示</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="<c:url value="/resources/demoSources/plupload/bootstrap/easyui.css"/>" type="text/css"></link>
<script type="text/javascript" src="<c:url value="/resources/demoSources/plupload/jquery-1.8.0.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/demoSources/plupload/easyui/jquery.easyui.min.js"/>"></script>
<script type="text/javascript">
/**
 * 创建上传窗口 公共方法
 * @param chunk 是否分割大文件
 * @param callBack 上传成功之后的回调
 */
function Uploader(chunk,callBack){
	var addWin = $('<div style="overflow: hidden;"/>');
	var upladoer = $('<iframe/>');
	upladoer.attr({'src':'<c:url value="/resources/demoSources/plupload/uploader.jsp?chunk=' + chunk + '"/>',width:'100%',height:'100%',frameborder:'0',scrolling:'no'});
	addWin.window({
		title:"上传文件",
		height:350,
		width:550,
		minimizable:false,
		modal:true,
		collapsible:false,
		maximizable:false,
		resizable:false,
		content:upladoer,
		onClose:function(){
			var fw = GetFrameWindow(upladoer[0]);
			var files = fw.files;
			$(this).window('destroy');
			callBack.call(this,files);
		},
		onOpen:function(){
			var target = $(this);
			setTimeout(function(){
				var fw = GetFrameWindow(upladoer[0]);
				fw.target = target;
			},100);
		}
	});
}

/**
 * 根据iframe对象获取iframe的window对象
 * @param frame
 * @returns {Boolean}
 */
function GetFrameWindow(frame){
	return frame && typeof(frame)=='object' && frame.tagName == 'IFRAME' && frame.contentWindow;
}
 
function makerUpload(chunk){
 Uploader(chunk,function(files){
	 if(files && files.length>0){
		 $("#res").text("成功上传："+files.join(","));
	 }
 });
}
</script>
</head>
<body style="width: 100%;height: 100%;overflow:hidden;margin-left: 50px;margin-top: 30px;">
	<h1>GodSon Easyui 结合Pluplaod插件的上传演示</h1>
    <h3>(上传的文件将于第二天清除,请不要恶意上传特大文件消耗流量)</h3>
	<hr/>
	<a class="easyui-linkbutton" href="javascript:makerUpload(false)">不分割文件上传</a> <a class="easyui-linkbutton" href="javascript:makerUpload(true)">分割文件上传</a>
	<hr/>
	<div id="res"></div>
</body>
</html>