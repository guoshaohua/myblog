package junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.zb.bean.Article;
import com.zb.qo.ArticleQo;
import com.zb.service.ArticleService;
import com.zb.vo.PageVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ArticleTest {

    @Autowired
    private ArticleService articleService;

    /**
     * 根据模块id，获取分页文章列表
     * 
     * @throws Exception
     */
    @Test
    public void testGetArticleList() throws Exception {
        ArticleQo a = new ArticleQo();
        a.setNavigationId("1");
        a.setPageSize(2);
        a.setPageNo(1);

        PageVo<Article> page = articleService.getArticleList(a);
        for (Article article : page.getResult()) {
            System.out.println(article.toString());
        }
        System.out.println();
        System.out.println("每页条数:" + page.getPageSize());
        System.out.println("总页数:" + page.getTotalPage());
        System.out.println("总条数:" + page.getTotalSize());
    }
}
