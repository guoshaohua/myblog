package junit;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zb.bean.Navigation;
import com.zb.service.NavigationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class NavigationTest {

    @Autowired
    private NavigationService navigationService;

    /**
     * 获取所有导航数据 输出结果为pid=0的数量.
     * 
     * 作者: zhoubang 日期：2015年4月2日 下午2:35:52
     * 
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testGetNavigationList() throws Exception {
        List<Object> ls = navigationService.getNavigationList();
        for (int i = 0; i < ls.size(); i++) {
            Collection<Map<String, Object>> map = (Collection<Map<String, Object>>) ls.get(i);
            for (Map<String, Object> m : map) {
                System.out.println("父导航数据：" + ((Navigation) m.get("parent")).toString());
                System.out.println("子导航数量：" + ((List<Object>) m.get("sub")).size());
                System.out.println();
            }
        }
    }
}
