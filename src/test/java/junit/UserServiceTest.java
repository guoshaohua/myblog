package junit;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zb.bean.Permission;
import com.zb.bean.Role;
import com.zb.bean.User;
import com.zb.qo.UserQo;
import com.zb.service.PermissionService;
import com.zb.service.RoleService;
import com.zb.service.UserService;
import com.zb.vo.PageVo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class UserServiceTest {

    @Resource(name = "userServiceImpl")
    private UserService userService;

    @Resource(name = "roleServiceImpl")
    private RoleService roleService;

    @Resource(name = "permissionServiceImpl")
    private PermissionService permissionService;;

    /**
     * 根据用户名获取用户信息
     * 
     * 作者：zhoubang 日期：2015年3月14日 上午2:13:59
     * 
     * @throws Exception
     */
    @Test
    public void testGetUserByName() throws Exception {
        User user = userService.getUserByName("zhoubang");
        System.out.println(user.toString());
    }

    /**
     * 获取用户角色列表
     * 
     * 作者：zhoubang 日期：2015年3月14日 上午2:15:09
     * 
     * @throws Exception
     */
    @Test
    public void testGetUserRoles() throws Exception {
        List<Role> roles = roleService.getUserRoles("zhoubang");
        System.out.println(roles.size());
    }

    /**
     * 获取用户的权限列表
     * 
     * 作者：zhoubang 日期：2015年3月14日 上午2:16:46
     * 
     * @throws Exception
     */
    @Test
    public void testGetUserPermissions() throws Exception {
        List<Permission> permissions = permissionService.getUserPermissions("zhoubang");
        System.out.println(permissions.size());
    }
    
    /**
     * 获取所有用户列表
     * 
     * 作者: zhoubang 
     * 日期：2015年4月24日 上午10:32:04
     * @throws Exception
     */
    @Test
    public void testGetUserList() throws Exception {
        UserQo userQo = new UserQo();
        userQo.setPageNo(2);
        userQo.setPageSize(2);
        PageVo<User> userList = userService.getUserList(userQo);
        System.out.println(userList.getResult().size());
    }
    
}
