该项目的初衷：<br/>
分享技术文章、主流技术、资源、教程等。
将比较好的技术文章分享给大家学习，共同进步。<br/>
欢迎大家访问，网址 http://www.2b2b92b.com


项目发布到外网说明：<br/>
    1、将/resources/css/font-awesome.css中的24-33行注释，同时41-50注释去掉<br/>
    2、修改/src/main/resources/jdbc/jdbc.properties中的数据库连接配置<br/>
    3、记得将本地sql进行导出,在服务器运行<br/>
    4、修改ueditor.config.js中的URL配置.
    5、项目中使用了mysql函数，请复制在mysql客户端运行创建函数
    
    SET GLOBAL log_bin_trust_function_creators=TRUE;
    
    DELIMITER $$
    USE `myblog` $$           
    DROP FUNCTION IF EXISTS `getParentNameList`$$
    CREATE FUNCTION `getParentNameList`(navigationId VARCHAR(24)) RETURNS VARCHAR(100) CHARSET utf8
    BEGIN
    DECLARE sTemp VARCHAR(100);
    DECLARE pidv VARCHAR(24);
    SET sTemp = (SELECT n.name FROM blog_navigation n WHERE n.id = navigationId);
    SET pidv = (SELECT pid FROM blog_navigation WHERE id = navigationId);
    WHILE pidv > 0 DO
    SET sTemp = CONCAT((SELECT n.name FROM blog_navigation n WHERE n.id = pidv),'-',sTemp);
    SELECT pid INTO pidv FROM blog_navigation WHERE id = pidv;
    END WHILE;
    RETURN sTemp;
    END $$
    DELIMITER $$;